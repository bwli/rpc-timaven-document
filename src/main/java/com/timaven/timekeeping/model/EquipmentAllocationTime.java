package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.timekeeping.model.enums.EquipmentHourlyType;
import com.timaven.timekeeping.model.enums.EquipmentOwnershipType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EquipmentAllocationTime.class)
@Getter
@Setter
@NoArgsConstructor
public class EquipmentAllocationTime {
    private Long id;

    private String equipmentId;

    private String description;

    private String alias;

    private String serialNumber;

    private String department;

    private String equipmentClass;

    private EquipmentHourlyType hourlyType;

    private EquipmentOwnershipType ownershipType;

    private String empId;

    private BigDecimal totalHour;

    private BigDecimal totalCharge;

    private EquipmentAllocationSubmission equipmentAllocationSubmission;

    private Set<EquipmentCostCodePerc> equipmentCostCodePercs;

    private Long weeklyProcessId;

    private EquipmentWeeklyProcess equipmentWeeklyProcess;

    private LocalDate payrollDate;

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentAllocationTime)) return false;
        if (null != id) {
            EquipmentAllocationTime that = (EquipmentAllocationTime) o;
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }
}

package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class StageCostCode {

    private Long id;

    private Long submissionId;

    private String costCode;

    private String description;
}

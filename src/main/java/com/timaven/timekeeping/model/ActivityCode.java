package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ActivityCode.class)
@Getter
@Setter
@NoArgsConstructor
public class ActivityCode {

    private Long id;

    private String activityCode;

    public ActivityCode(Long id, String activityCode){
        this.id = id;
        this.activityCode = activityCode;
    }
}
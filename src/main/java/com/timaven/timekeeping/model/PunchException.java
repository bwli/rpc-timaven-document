package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Comparator;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = PunchException.class)
@Getter
@Setter
@NoArgsConstructor
public class PunchException implements Comparable<PunchException> {
    private Long id;

    @JsonBackReference
    private AllocationTime allocationTime;

    private String exception;

    private int order;

    private int exceptionType;

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PunchException)) return false;
        PunchException that = (PunchException) o;
        if (null != id) {
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }

    @Override
    public int compareTo(PunchException e) {
        return Comparator.comparing(PunchException::getOrder).compare(this, e);
    }
}

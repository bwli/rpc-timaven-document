package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class PurchaseVendorRoster {
    private String id;
    private Long vendorId;
    private String vendorName;
    private LocalDateTime createdAt;
}

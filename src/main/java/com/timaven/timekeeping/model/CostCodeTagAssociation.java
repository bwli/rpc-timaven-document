package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.timekeeping.model.enums.CostCodeType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = CostCodeTagAssociation.class)
@Getter
@Setter
@NoArgsConstructor
public class CostCodeTagAssociation {
    private Long id;
    private String tagType;
    private String typeDescription;
    private String codeName;
    private String codeDescription;
    private CostCodeType costCodeType;
    private Long key;

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeTagAssociation)) return false;
        if (null != id) {
            CostCodeTagAssociation that = (CostCodeTagAssociation) o;
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }
}

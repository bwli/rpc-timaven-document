package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = AllocationSubmission.class)
@Getter
@Setter
@NoArgsConstructor
public class AllocationSubmission implements Comparable<AllocationSubmission> {

    private Long id;
    private Long submitUserId;
    private Long approveUserId;
    private Long finalizeUserId;
    private LocalDateTime approvedAt;
    private LocalDateTime finalizedAt;
    private Long reportSubmissionId;
    private String teamName;
    private AllocationSubmissionStatus status;
    private LocalDate dateOfService;
    private LocalDate payrollDate;
    private Long projectId;
    private LocalDateTime createdAt = LocalDateTime.now();
    private Set<AllocationTime> allocationTimes;
    private Boolean isBySystem = false;
    private Boolean billingPurposeOnly = false;

    private User submitUser;
    private User approveUser;
    private Boolean exported = false;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        if (id != null) {
            hcb.append(id);
        } else {
            hcb.append(projectId);
            hcb.append(teamName);
            hcb.append(dateOfService);
        }
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AllocationSubmission)) return false;
        AllocationSubmission that = (AllocationSubmission) o;
        EqualsBuilder eb = new EqualsBuilder();
        if (id != null && that.id != null) {
            eb.append(id, that.id);
        } else {
            eb.append(projectId, that.projectId);
            eb.append(teamName, that.teamName);
            eb.append(dateOfService, that.dateOfService);
            eb.append(payrollDate, that.payrollDate);
        }
        return eb.isEquals();
    }

    @Override
    public int compareTo(AllocationSubmission o) {
        return Comparator.comparing(AllocationSubmission::getProjectId, Comparator.nullsFirst(Comparator.naturalOrder()))
                .thenComparing(AllocationSubmission::getDateOfService).reversed()
                .thenComparing(AllocationSubmission::getTeamName)
                .thenComparing(AllocationSubmission::getPayrollDate).reversed()
                .compare(this, o);
    }
}

package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
public class StageCraft {

    private Long id;

    private Long submissionId;

    private String craftCode;

    private String description;

    private BigDecimal billableST;

    private BigDecimal billableOT;

    private BigDecimal billableDT;

    private BigDecimal perDiem;

    private BigDecimal rigPay;

    private String company;

    private LocalDate startDate;

    private LocalDate endDate;

    private String perDiemRule;
}

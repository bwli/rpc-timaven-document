package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.timekeeping.model.enums.AllocationTimeHourType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ActivityCode.class)
@Getter
@Setter
@NoArgsConstructor
public class ActivityCodePerc {

    private Long id;

    private BigDecimal stHour;

    private BigDecimal otHour;

    private BigDecimal dtHour;

    private BigDecimal totalHour;

    private String employeeId;

    private LocalDateTime createdAt;

    private String activityCodeText;

    private Long activityId;

    private AllocationTime allocationTime;

    private ActivityCode activityCode;

    private AllocationTimeHourType extraTimeType;
}

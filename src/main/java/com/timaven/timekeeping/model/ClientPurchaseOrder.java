package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"orderCode", "projectId"})
public class ClientPurchaseOrder {
    private Long id;

    @NotNull
    private String orderCode;

    @NotNull
    private Long projectId;

    public void merge(ClientPurchaseOrder cpo) {
        if (cpo == null) return;
        //Nothing to merge
    }
}

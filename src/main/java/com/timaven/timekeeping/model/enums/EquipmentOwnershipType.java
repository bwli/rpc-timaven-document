package com.timaven.timekeeping.model.enums;

public enum EquipmentOwnershipType {
    COMPANY_OWNED("Co. Owned"),
    RENTAL("Rental");

    private final String displayValue;

    EquipmentOwnershipType(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public static EquipmentOwnershipType fromDisplayValue(String displayValue) {
        if (displayValue.equalsIgnoreCase("Co. Owned")) return COMPANY_OWNED;
        if (displayValue.equalsIgnoreCase("Rental")) return RENTAL;
        return null;
    }
}

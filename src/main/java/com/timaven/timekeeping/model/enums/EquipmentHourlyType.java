package com.timaven.timekeeping.model.enums;

import org.springframework.util.StringUtils;

public enum EquipmentHourlyType {
    DAILY("Non-hourly"),
    HOURLY("Hourly");

    private final String displayValue;

    EquipmentHourlyType(String displayValue) {
        this.displayValue = displayValue;
    }

    public static EquipmentHourlyType fromString(String s) {
        if (StringUtils.isEmpty(s) || "non-hourly".equalsIgnoreCase(s) || "daily".equalsIgnoreCase(s)) {
            return DAILY;
        } else if ("hourly".equalsIgnoreCase(s)) {
            return HOURLY;
        }
        return DAILY;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}

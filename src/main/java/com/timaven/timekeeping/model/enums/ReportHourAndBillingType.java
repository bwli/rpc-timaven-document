package com.timaven.timekeeping.model.enums;

public enum ReportHourAndBillingType {
    EMPLOYEE_HOURS_BILLING,
    EMPLOYEE_HOURS,
    COST_BY_TOTALS,
    CLASSIFICATION_CODE,
    PER_DIEM_EXPENSE,
    TRAVEL_PAY_SUMMARY
}

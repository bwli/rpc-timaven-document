package com.timaven.timekeeping.model.enums;

public enum EquipmentWeeklyProcessType {
    NORMAL,
    TLO;

    public int getValue() {
        return switch (this) {
            case NORMAL -> 0;
            case TLO -> 1;
        };
    }

    public static EquipmentWeeklyProcessType fromValue(int type) {
        if (type == 1) return TLO;
        return NORMAL;
    }

}

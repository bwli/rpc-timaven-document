package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.timekeeping.model.enums.AllocationTimeHourType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = AllocationTime.class)
@Getter
@Setter
@NoArgsConstructor
public class AllocationTime implements Comparable<AllocationTime> {
    private Long id;

    private String empId;

    private String clientEmpId;

    private String badge;

    private String teamName;

    private BigDecimal stHour;

    private BigDecimal otHour;

    private BigDecimal dtHour;

    private AllocationTimeHourType extraTimeType;

    private BigDecimal totalHour;

    private BigDecimal allocatedHour;

    private BigDecimal netHour;

    private boolean hasPerDiem;

    private boolean hasMob;

    private String mobCostCode;

    private BigDecimal mobAmount = BigDecimal.ZERO;

    private BigDecimal mobMileageRate = BigDecimal.ZERO;

    private BigDecimal mobMileage = BigDecimal.ZERO;

    private BigDecimal perDiemAmount = BigDecimal.ZERO;

    private BigDecimal rigPay;

    private AllocationSubmission allocationSubmission;

    private Set<CostCodePerc> costCodePercs;

    private Set<PunchException> punchExceptions;

    private Set<AllocationRecord> allocationRecords;

    private Set<AllocationException> allocationExceptions;

    private AllocationException allocationException;

    private String firstName;

    private String lastName;

    private String jobNumber;

    private String maxTimeCostCode;

    private String perDiemCostCode;

    private Long weeklyProcessId;

    private WeeklyProcess weeklyProcess;

    private Employee employee;

    private PerDiem perDiem;

    private List<CostCodeTagAssociation> mobCostCodeTagAssociations;

    private List<CostCodeTagAssociation> perDiemCostCodeTagAssociations;

    private String fullName;

    private Set<ActivityCodePerc> activityCodePercs = new HashSet<>();

    public BigDecimal getStHour() {
        return stHour == null ? BigDecimal.ZERO : stHour;
    }

    public BigDecimal getOtHour() {
        return otHour == null ? BigDecimal.ZERO : otHour;
    }

    public BigDecimal getDtHour() {
        return dtHour == null ? BigDecimal.ZERO : dtHour;
    }

    public BigDecimal getTotalHour() {
        return totalHour == null ? BigDecimal.ZERO : totalHour;
    }

    public BigDecimal getAllocatedHour() {
        return allocatedHour == null ? BigDecimal.ZERO : allocatedHour;
    }

    public BigDecimal getMobAmount() {
        return mobAmount == null ? BigDecimal.ZERO : mobAmount;
    }

    public BigDecimal getPerDiemAmount() {
        return perDiemAmount == null ? BigDecimal.ZERO : perDiemAmount;
    }

    public AllocationException getAllocationException() {
        if (allocationException == null && allocationExceptions != null) return allocationExceptions.stream()
                .findFirst().orElse(null);
        return allocationException;
    }

    public Set<AllocationException> getAllocationExceptions() {
        if (null == allocationExceptions) {
            allocationExceptions = new HashSet<>();
        }
        return allocationExceptions;
    }

    public String getFullName() {
        if (StringUtils.hasText(firstName) && StringUtils.hasText(lastName)) {
            return String.format("%s, %s", lastName, firstName);
        } else if (StringUtils.hasText(firstName)) {
            return firstName;
        } else if (StringUtils.hasText(lastName)) {
            return lastName;
        }
        return "Unknown";
    }

    public static Comparator<AllocationTime> getAllocationTimeComparatorByName() {
        return (t0, t1) -> Comparator
                .comparing(AllocationTime::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(AllocationTime::getFirstName, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(t0, t1);
    }

    @Override
    public int hashCode() {
        if (null != id) {
            HashCodeBuilder hcb = new HashCodeBuilder();
            hcb.append(id);
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AllocationTime)) return false;
        if (null != id) {
            AllocationTime that = (AllocationTime) o;
            EqualsBuilder eb = new EqualsBuilder();
            eb.append(id, that.id);
            return eb.isEquals();
        }
        return super.equals(o);
    }

    @Override
    public int compareTo(AllocationTime o) {
        return Comparator.comparing(AllocationTime::getFullName)
                .compare(this, o);
    }
}

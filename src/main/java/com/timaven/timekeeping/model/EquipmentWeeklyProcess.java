package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.timaven.timekeeping.model.enums.EquipmentWeeklyProcessType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = EquipmentWeeklyProcess.class)
@Getter
@Setter
@NoArgsConstructor
public class EquipmentWeeklyProcess {

    private Long id;
    private Long approveUserId;
    private Long reviewUserId;
    private Long finalizeUserId;
    private Long reportUserId;
    private LocalDateTime approvedAt;
    private LocalDateTime reviewedAt;
    private LocalDateTime finalizedAt;
    private LocalDateTime reportGeneratedAt;

    private EquipmentWeeklyProcessType type = EquipmentWeeklyProcessType.NORMAL;

    private LocalDate weekEndDate;

    private Long projectId;

    private BigDecimal billableAmount;

    private BigDecimal baseAmount;

    private BigDecimal totalHours;

    private LocalDateTime createdAt;

    private List<EquipmentAllocationTime> equipmentAllocationTimes;

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = null;
        if (null != id) {
            hcb = new HashCodeBuilder();
            hcb.append(id);
        } else if (null != projectId && null != weekEndDate) {
            hcb = new HashCodeBuilder();
            hcb.append(projectId);
            hcb.append(weekEndDate);
            hcb.append(type);
        }
        if (null != hcb) {
            return hcb.toHashCode();
        }
        return super.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EquipmentWeeklyProcess)) return false;
        EquipmentWeeklyProcess that = (EquipmentWeeklyProcess) o;
        EqualsBuilder eb = null;
        if (null != id) {
            eb = new EqualsBuilder();
            eb.append(id, that.id);
        } else if (null != projectId && null != weekEndDate) {
            eb = new EqualsBuilder();
            eb.append(projectId, that.projectId);
            eb.append(weekEndDate, that.weekEndDate);
            eb.append(type, that.type);
        }
        if (null != eb) {
            return eb.isEquals();
        }
        return super.equals(o);
    }
}

package com.timaven.timekeeping.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Comparator;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"projectId", "teamName", "costCode", "startDate", "endDate"})
public class TeamCostCode implements Comparable<TeamCostCode> {
    private Long id;

    private Long projectId;

    private String teamName;

    private String costCode;

    private LocalDate startDate;

    private LocalDate endDate;

    private LocalDateTime createdAt;

    public int customHashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(projectId);
        hcb.append(teamName);
        hcb.append(costCode);
        return hcb.toHashCode();
    }

    @Override
    public int compareTo(TeamCostCode that) {
        return Comparator.comparing(TeamCostCode::getProjectId, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(TeamCostCode::getTeamName, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(TeamCostCode::getCostCode, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, that);
    }
}

package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class EmpPunchRecords {

    private String empId;
    private String badge;
    private String clientEmpId;
    private String lastName;
    private String firstName;
    private String cardHolder;
    private String credentialCode;
    private LocalDate date;
    private String teamName;
    private String company;
    //Used to record all records
    private List<PunchRecord> allPunchRecordList = new ArrayList<>();
    //Used to calculate adjusted time
    private List<PunchRecord> validPunchRecordList = new ArrayList<>();
    private Boolean lastSide = null;
    private LocalDateTime lastEventTime = null;
    private Duration netDuration = Duration.ZERO;
    private Duration payDuration = null;
    private List<EmpPunchException> exceptionList = new ArrayList<>();

    public PunchRecord getLastPunchRecord() {
        return CollectionUtils.isEmpty(validPunchRecordList) ? null :
                validPunchRecordList.get(validPunchRecordList.size() - 1);
    }

    public PunchRecord getFirstPunchRecord() {
        return CollectionUtils.isEmpty(validPunchRecordList) ? null : validPunchRecordList.get(0);
    }

    public int getPunchRecordListSize() {
        return CollectionUtils.isEmpty(validPunchRecordList) ? 0 : validPunchRecordList.size();
    }

    public void removePunchRecords(List<PunchRecord> needToBeRemovedList) {
        if (!CollectionUtils.isEmpty(validPunchRecordList)) {
            validPunchRecordList.removeAll(needToBeRemovedList);
        }
    }

    public void finalizeValidPunchRecords() {
        if (!CollectionUtils.isEmpty(validPunchRecordList)) {
            validPunchRecordList.forEach(p -> p.setValid(true));
        }
    }

    public void addException(String exception, int order, int exceptionType) {
        EmpPunchException empPunchException = new EmpPunchException(exception, order, exceptionType);
        exceptionList.add(empPunchException);
    }

    public List<EmpPunchException> getExceptionList() {
        return exceptionList;
    }

    public void addPunchRecord(PunchRecord punchRecord) {
        if (punchRecord.getEvent().isAccessible()) {
            // Access granted
            if (null == lastSide) {
                // First valid punch record
                this.validPunchRecordList.add(punchRecord);
                this.lastSide = punchRecord.isSide();
            } else {
                if (this.lastSide == punchRecord.isSide()) {
                    LocalDateTime curEventTime = punchRecord.getEventTime();
                    if (lastEventTime.plusMinutes(5).isBefore(curEventTime)) {
                        // If time difference is more than 5 minutes, we consider it as new pair
                        this.validPunchRecordList.add(punchRecord);
                    } else {
                        if (this.lastSide) {
                            //keep last
                            this.validPunchRecordList.remove(this.validPunchRecordList.size() - 1);
                            this.validPunchRecordList.add(punchRecord);
                        }
                        // for side out we keep first
                    }
                } else {
                    this.validPunchRecordList.add(punchRecord);
                    this.lastSide = !lastSide;
                }
            }
            this.lastEventTime = punchRecord.getNetEventTime();
        }
        this.allPunchRecordList.add(punchRecord);
    }

    public BigDecimal getTotalHours() {
        Duration payDuration = this.payDuration == null ? Duration.ZERO : this.payDuration;
        return BigDecimal.valueOf(payDuration.toMinutes() / 60d)
                .setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros();
    }

    public BigDecimal getNetHours() {
        return BigDecimal.valueOf(netDuration.toMinutes() / 60d)
                .setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros();
    }

    public String getTeamName() {
        if (teamName != null) {
            teamName = teamName.trim();
        }
        return teamName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmpPunchRecords that = (EmpPunchRecords) o;
        return empId.equals(that.empId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(empId);
    }

    public class EmpPunchException {
        private String exception;
        private int order;
        private int exceptionType;

        public EmpPunchException() {
        }

        EmpPunchException(String exception, int order, int exceptionType) {
            this.exception = exception;
            this.order = order;
            this.exceptionType = exceptionType;
        }

        public String getException() {
            return exception;
        }

        public void setException(String exception) {
            this.exception = exception;
        }

        public int getOrder() {
            return order;
        }

        public void setOrder(int order) {
            this.order = order;
        }

        public int getExceptionType() {
            return exceptionType;
        }

        public void setExceptionType(int exceptionType) {
            this.exceptionType = exceptionType;
        }
    }
}

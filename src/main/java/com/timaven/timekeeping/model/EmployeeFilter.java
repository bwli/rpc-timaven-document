package com.timaven.timekeeping.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeFilter {
    Long projectId;
    String teamName;
    Collection<String> empIds;
    Collection<String> badges;
    Collection<String> clientEmpIds;
    @NotNull
    LocalDate startDate;
    LocalDate endDate;
}

package com.timaven.timekeeping.model.dto;

import com.timaven.timekeeping.model.enums.EquipmentAllocationSubmissionStatus;
import lombok.Getter;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Getter
public class EquipmentAllocationTimeFilter {
    private final Set<LocalDate> dates;
    private final LocalDate startDate;
    private final LocalDate endDate;
    private final LocalDate payrollStartDate;
    private final LocalDate payrollEndDate;
    private final Long projectId;
    private final EquipmentAllocationSubmissionStatus status;
    private final EquipmentAllocationSubmissionStatus notStatus;
    private final List<String> includes;

    public EquipmentAllocationTimeFilter(EquipmentAllocationTimeFilterBuilder builder) {
        this.dates = builder.dates;
        this.startDate = builder.startDate;
        this.endDate = builder.endDate;
        this.payrollStartDate = builder.payrollStartDate;
        this.payrollEndDate = builder.payrollEndDate;
        this.projectId = builder.projectId;
        this.status = builder.status;
        this.notStatus = builder.notStatus;
        this.includes = builder.includes;
    }

    public static class EquipmentAllocationTimeFilterBuilder {
        private Set<LocalDate> dates;
        private LocalDate startDate;
        private LocalDate endDate;
        private LocalDate payrollStartDate;
        private LocalDate payrollEndDate;
        private Long projectId;
        private EquipmentAllocationSubmissionStatus status;
        private EquipmentAllocationSubmissionStatus notStatus;
        private List<String> includes;

        public EquipmentAllocationTimeFilterBuilder dates(Set<LocalDate> dates) {
            this.dates = dates;
            return this;
        }

        public EquipmentAllocationTimeFilterBuilder startDate(LocalDate startDate) {
            this.startDate = startDate;
            return this;
        }

        public EquipmentAllocationTimeFilterBuilder endDate(LocalDate endDate) {
            this.endDate = endDate;
            return this;
        }

        public EquipmentAllocationTimeFilterBuilder payrollStartDate(LocalDate payrollStartDate) {
            this.payrollStartDate = payrollStartDate;
            return this;
        }

        public EquipmentAllocationTimeFilterBuilder payrollEndDate(LocalDate payrollEndDate) {
            this.payrollEndDate = payrollEndDate;
            return this;
        }

        public EquipmentAllocationTimeFilterBuilder projectId(Long projectId) {
            this.projectId = projectId;
            return this;
        }

        public EquipmentAllocationTimeFilterBuilder status(EquipmentAllocationSubmissionStatus status) {
            this.status = status;
            return this;
        }

        public EquipmentAllocationTimeFilterBuilder notStatus(EquipmentAllocationSubmissionStatus notStatus) {
            this.notStatus = notStatus;
            return this;
        }

        public EquipmentAllocationTimeFilterBuilder includes(List<String> includes) {
            this.includes = includes;
            return this;
        }

        public EquipmentAllocationTimeFilter build() {
            return new EquipmentAllocationTimeFilter(this);
        }

    }
}

package com.timaven.timekeeping.model.dto;

import be.quodlibet.boxable.HorizontalAlignment;
import be.quodlibet.boxable.VerticalAlignment;
import be.quodlibet.boxable.image.Image;
import be.quodlibet.boxable.line.LineStyle;
import lombok.Getter;
import org.apache.pdfbox.pdmodel.font.PDFont;

import java.awt.*;

@Getter
public class CellAttributes {
    private final String value;
    private final HorizontalAlignment horizontalAlignment;
    private final VerticalAlignment verticalAlignment;
    private final Color fillColor;
    private final Color textColor;
    private final PDFont font;
    private final Float fontSize;
    private final Image image;
    private final Float leftPadding;
    private final Float rightPadding;
    private final Float topPadding;
    private final Float bottomPadding;
    private final LineStyle borderStyle;
    private final LineStyle leftBorderStyle;
    private final LineStyle rightBorderStyle;
    private final LineStyle topBorderStyle;
    private final LineStyle bottomBorderStyle;

    public CellAttributes(CellBuilder builder) {
        this.value = builder.value;
        this.horizontalAlignment = builder.horizontalAlignment;
        this.verticalAlignment = builder.verticalAlignment;
        this.fillColor = builder.fillColor;
        this.textColor = builder.textColor;
        this.font = builder.font;
        this.fontSize = builder.fontSize;
        this.image = builder.image;
        this.leftPadding = builder.leftPadding;
        this.rightPadding = builder.rightPadding;
        this.topPadding = builder.topPadding;
        this.bottomPadding = builder.bottomPadding;
        this.borderStyle = builder.borderStyle;
        this.leftBorderStyle = builder.leftBorderStyle;
        this.rightBorderStyle = builder.rightBorderStyle;
        this.topBorderStyle = builder.topBorderStyle;
        this.bottomBorderStyle = builder.bottomBorderStyle;
    }

    public String getValue() {
        return value == null ? "" : value;
    }

    public static class CellBuilder {
        private String value;
        private final HorizontalAlignment horizontalAlignment;
        private final VerticalAlignment verticalAlignment;
        private Color fillColor;
        private Color textColor;
        private PDFont font;
        private Float fontSize;
        private Image image;
        private Float leftPadding;
        private Float rightPadding;
        private Float topPadding;
        private Float bottomPadding;
        private LineStyle borderStyle;
        private LineStyle leftBorderStyle;
        private LineStyle rightBorderStyle;
        private LineStyle topBorderStyle;
        private LineStyle bottomBorderStyle;

        public CellBuilder(HorizontalAlignment horizontalAlignment, VerticalAlignment verticalAlignment) {
            this.horizontalAlignment = horizontalAlignment;
            this.verticalAlignment = verticalAlignment;
        }

        public CellBuilder value(String value) {
            this.value = value;
            return this;
        }

        public CellBuilder fillColor(Color fillColor) {
            this.fillColor = fillColor;
            return this;
        }

        public CellBuilder textColor(Color textColor) {
            this.textColor = textColor;
            return this;
        }

        public CellBuilder font(PDFont font) {
            this.font = font;
            return this;
        }

        public CellBuilder fontSize(float fontSize) {
            this.fontSize = fontSize;
            return this;
        }

        public CellBuilder image(Image image) {
            this.image = image;
            return this;
        }

        public CellBuilder leftPadding(float leftPadding) {
            this.leftPadding = leftPadding;
            return this;
        }

        public CellBuilder rightPadding(float rightPadding) {
            this.rightPadding = rightPadding;
            return this;
        }

        public CellBuilder topPadding(float topPadding) {
            this.topPadding = topPadding;
            return this;
        }

        public CellBuilder bottomPadding(float bottomPadding) {
            this.bottomPadding = bottomPadding;
            return this;
        }

        public CellBuilder borderStyle(LineStyle borderStyle) {
            this.borderStyle = borderStyle;
            return this;
        }

        public CellBuilder leftBorderStyle(LineStyle leftBorderStyle) {
            this.leftBorderStyle = leftBorderStyle;
            return this;
        }

        public CellBuilder rightBorderStyle(LineStyle rightBorderStyle) {
            this.rightBorderStyle = rightBorderStyle;
            return this;
        }

        public CellBuilder topBorderStyle(LineStyle topBorderStyle) {
            this.topBorderStyle = topBorderStyle;
            return this;
        }

        public CellBuilder bottomBorderStyle(LineStyle bottomBorderStyle) {
            this.bottomBorderStyle = bottomBorderStyle;
            return this;
        }

        public CellAttributes build() {
            return new CellAttributes(this);
        }
    }
}

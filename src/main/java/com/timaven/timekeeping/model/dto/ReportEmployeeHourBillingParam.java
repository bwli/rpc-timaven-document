package com.timaven.timekeeping.model.dto;

import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class ReportEmployeeHourBillingParam {
    private Set<Long> projectIds;
    private List<String> groupingBy;
    private String startDate;
    private String endDate;
    Boolean approvedOnly;
}

package com.timaven.timekeeping.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Getter
@Setter
public class ReceivedPurchaseOrderDto {
    private Long id;
    private String controlCompany;
    private Integer vendorNumber;
    private String vendorName;
    private String purchaseOrderNumber;
    private Integer poItem;
    private Integer sequenceNo;
    private String partNumber;
    private String description;
    private String unitOfMeasure;
    private BigDecimal quantityOrdered;
    private BigDecimal unitCost;
    private BigDecimal quantityReceived;
    private BigDecimal quantityRejected;
    private BigDecimal amountReceived;
    private BigDecimal amountAccepted;
    private String jobNumber;
    private String subJobNumber;
    private String formattedCostDistribution;
    private String costType;
    private String formattedGlAccount;
    private LocalDate dateReceived;
    private LocalDateTime createdAt;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReceivedPurchaseOrderDto that = (ReceivedPurchaseOrderDto) o;
        if (id != null && that.id != null) return id.equals(that.id);
        return false;
    }

    @Override
    public int hashCode() {
        if (id != null) return Objects.hash(id);
        return super.hashCode();
    }
}

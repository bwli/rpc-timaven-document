package com.timaven.timekeeping.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class IziToastOptions {

    private String title;
    private String message;
    /**
     * Where it will be shown. It can be bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter or center.
     */
    private String position;
    /**
     * It can be #hexadecimal, pre-defined themes like blue, red, green and yellow or set another class. Create and use like this ".iziToast-color-name"
     */
    private String color;
    /**
     * Amount in milliseconds to close the toast or false to disable.
     */
    private Object timeout;

    public IziToastOptions(IziToastOptionBuilder builder) {
        this.title = builder.title;
        this.message = builder.message;
        this.position = builder.position;
        this.color = builder.color;
        this.timeout = builder.timeout;
    }

    public static class IziToastOptionBuilder {
        private String title = "";
        private String message = "";
        private String position = "bottomRight";
        private String color = "";
        private Object timeout = 5000;

        public IziToastOptionBuilder title(String title) {
            this.title = title;
            return this;
        }

        public IziToastOptionBuilder message(String message) {
            this.message = message;
            return this;
        }

        public IziToastOptionBuilder position(String position) {
            if (!StringUtils.hasText(position)) return this;
            if (position.equalsIgnoreCase("bottomRight")
                    || position.equalsIgnoreCase("bottomLeft")
                    || position.equalsIgnoreCase("topRight")
                    || position.equalsIgnoreCase("topLeft")
                    || position.equalsIgnoreCase("topCenter")
                    || position.equalsIgnoreCase("bottomCenter")
                    || position.equalsIgnoreCase("center"))
                this.position = position;
            return this;
        }

        public IziToastOptionBuilder color(String color) {
            this.color = color;
            return this;
        }

        public IziToastOptionBuilder timeout(Object timeout) {
            this.timeout = timeout;
            return this;
        }

        public IziToastOptions build() {
            return new IziToastOptions(this);
        }

    }
}

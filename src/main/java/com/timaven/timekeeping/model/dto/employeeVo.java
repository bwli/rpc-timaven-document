package com.timaven.timekeeping.model.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class employeeVo {
    private BigDecimal billed;
    private BigDecimal cost;
    private String employeeID;
    private String firstName;
    private String lastName;
    private BigDecimal profit;
    private BigDecimal totalHour;
}

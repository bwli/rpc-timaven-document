package com.timaven.timekeeping.model.dto;

import lombok.Data;

import java.util.Map;

@Data
public class ECMSMapping {
    Map<String, String> craftCodeEmplClassMap;
    Map<String, String> craftCodeEmplTypeMap;
}

package com.timaven.timekeeping.model.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
public class ReportHourAndBillingParam {
    private Set<Long> projectIds;
    private List<String> groupingBy;
    private String startDate;
    private String endDate;
    private String shift;
    private Boolean approvedOnly;
    private Boolean hideDetail;

    public List<String> getGroupingBy() {
        return groupingBy == null ? new ArrayList<>() : groupingBy;
    }
}

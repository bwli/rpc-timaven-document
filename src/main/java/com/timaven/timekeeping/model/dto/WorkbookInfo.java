package com.timaven.timekeeping.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.poi.ss.usermodel.Workbook;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkbookInfo {
    private Workbook workbook;
    private String fileName;
    private int firstRowNum = 0;
}

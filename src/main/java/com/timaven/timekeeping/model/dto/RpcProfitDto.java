package com.timaven.timekeeping.model.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class RpcProfitDto {
    private String costCode;
    private BigDecimal costCodeBilled;
    private BigDecimal costCodeCost;
    private BigDecimal costCodeProfit;
    private List<employeeVo> employeeVOS;

}

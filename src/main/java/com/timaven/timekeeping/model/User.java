package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity bean with JPA annotations Hibernate provides JPA implementation
 */
@Getter
@Setter
@EqualsAndHashCode(of = "username")
@NoArgsConstructor
public class User {


    private Long id;

    @NotNull
    private String username;

    @NotNull
    private String password;

    @NotNull
    private String displayName;

    private String ipAddress;

    private LocalDateTime loggedInAt;

    @NotNull
    private Boolean active = true;

    private String passwordConfirm;

    private Set<Long> roleIds;

    @JsonManagedReference("userProjects")
    private Set<UserProject> userProjects;

    private Set<String> teams;

    public Set<String> getTeams() {
        if (teams == null) {
            teams = new HashSet<>();
        }
        return teams;
    }
}

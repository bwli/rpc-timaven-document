package com.timaven.timekeeping.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

@Getter
@Setter
@NoArgsConstructor
public class PunchRecord {
    private String empId;
    private String badge;
    private String clientEmpId;
    private String teamName;
    private String lastName;
    private String firstName;
    private String cardHolder;
    private String credentialCode;
    private LocalTime scheduleStart;
    private LocalTime scheduleEnd;
    private boolean valid = false;
    private Event event;
    private Door door;
    private boolean side;
    private LocalDateTime eventTime;
    private LocalDateTime netEventTime;
    private String company;
    private Duration netDuration;
    private Duration payDuration;

    public Event getEvent() {
        if (null == event) {
            event = new Event();
        }
        return event;
    }

    public Door getDoor() {
        if (null == door) {
            door = new Door();
        }
        return door;
    }

    public boolean isInGraceInPeriod(int graceIn) {
        return eventTime.minusMinutes(graceIn).toLocalTime().isBefore(scheduleStart);
    }

    public void adjustGraceIn() {
        eventTime = LocalDateTime.of(eventTime.toLocalDate(), scheduleStart);
    }

    public boolean isInGraceOutPeriod(int graceOut) {
        return eventTime.plusMinutes(graceOut).toLocalTime().isAfter(scheduleEnd);
    }

    public void adjustGraceOut() {
        eventTime = LocalDateTime.of(eventTime.toLocalDate(), scheduleEnd);
    }

    public void roundTime(int roundingMinutes, int minutesToBePlus) {
        if (roundingMinutes == 0) return;
        eventTime = eventTime.truncatedTo(ChronoUnit.HOURS)
                .plusMinutes((long) roundingMinutes * ((eventTime.getMinute() +
                        (minutesToBePlus)) / roundingMinutes));
    }

    public class Event {
        private boolean accessible;
        private String detail;

        public boolean isAccessible() {
            return accessible;
        }

        public void setAccessible(boolean accessible) {
            this.accessible = accessible;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }
    }

    public class Door {
        private String name;
        private boolean entry;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isEntry() {
            return entry;
        }

        public void setEntry(boolean entry) {
            this.entry = entry;
        }
    }
}

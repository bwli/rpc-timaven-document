package com.timaven.timekeeping.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class CostCodeAssociation {

    private Long id;

    private String costCode;

    private Long projectId;

    LocalDate startDate;

    LocalDate endDate;

    private String costCodeFull;

    private String level1;

    private String level2;

    private String level3;

    private String level4;

    private String level5;

    private String level6;

    private String level7;

    private String level8;

    private String level9;

    private String level10;

    private String level11;

    private String level12;

    private String level13;

    private String level14;

    private String level15;

    private String level16;

    private String level17;

    private String level18;

    private String level19;

    private String level20;

    private LocalDateTime createdAt;

    public String getLevel(int level) {
        String levelX = switch (level) {
            case 1 -> level1;
            case 2 -> level2;
            case 3 -> level3;
            case 4 -> level4;
            case 5 -> level5;
            case 6 -> level6;
            case 7 -> level7;
            case 8 -> level8;
            case 9 -> level9;
            case 10 -> level10;
            case 11 -> level11;
            case 12 -> level12;
            case 13 -> level13;
            case 14 -> level14;
            case 15 -> level15;
            case 16 -> level16;
            case 17 -> level17;
            case 18 -> level18;
            case 19 -> level19;
            case 20 -> level20;
            default -> "";
        };
        return levelX == null ? "" : levelX;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        hcb.append(id);
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CostCodeAssociation)) return false;
        CostCodeAssociation that = (CostCodeAssociation) o;
        EqualsBuilder eb = new EqualsBuilder();
        eb.append(id, that.id);
        return eb.isEquals();
    }
}

package com.timaven.timekeeping.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Comparator;

@Getter
@Setter
@NoArgsConstructor
public class Employee implements Comparable<Employee>, Cloneable {

    private Long id;

    @NotNull
    private String empId;

    private String clientEmpId;

    private String craft;

    private Long perDiemId;

    private BigDecimal baseST;

    private BigDecimal baseOT;

    private BigDecimal baseDT;

    private BigDecimal holidayRate;

    private BigDecimal sickLeaveRate;

    private BigDecimal travelRate;

    private BigDecimal vacationRate;

    private String clientCraft;

    private String teamName;

    private String crew;

    private Long projectId;

    private String firstName;

    private String lastName;

    private String middleName;

    // Either F or M
    private String gender;

    private String company;

    private String badge;

    private String signInSheet;

    private String jobNumber;

    @JsonFormat(pattern = "MM/dd/yyyy")
    private LocalDate effectedOn;

    private LocalDate terminatedAt;

    private LocalDate rehiredAt;

    private LocalDate hiredAt;

    @NotNull
    private Boolean active = true;

    private Boolean hasPerDiem = false;

    private Boolean hasRigPay = false;

    private String shift;

    private LocalTime scheduleStart;

    private LocalTime scheduleEnd;

    private LocalTime lunchStart;

    private LocalTime lunchEnd;

    private LocalDateTime createdAt;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Craft craftEntity;

    private String invalidReason;

    private String fullName;

    private String activityCode;

    public String getFullName() {
        if (StringUtils.hasText(firstName) && StringUtils.hasText(lastName)) {
            return String.format("%s, %s", lastName, firstName);
        } else if (StringUtils.hasText(firstName)) {
            return firstName;
        } else if (StringUtils.hasText(lastName)) {
            return lastName;
        }
        return "Unknown";
    }

    @Override
    public int hashCode() {
        HashCodeBuilder hcb = new HashCodeBuilder();
        if (id != null) {
            hcb.append(id);
        } else {
            hcb.append(empId);
            hcb.append(hiredAt);
            hcb.append(terminatedAt);
            hcb.append(projectId);
        }
        return hcb.toHashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee that = (Employee) o;
        EqualsBuilder eb = new EqualsBuilder();
        if (this.id != null && that.id != null) {
            eb.append(id, that.id);
        } else {
            eb.append(empId, that.empId);
            eb.append(projectId, that.projectId);
            eb.append(hiredAt, that.hiredAt);
            eb.append(terminatedAt, that.terminatedAt);
        }
        return eb.isEquals();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public int compareTo(Employee o) {
        return Comparator.comparing(Employee::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                .thenComparing(Employee::getFirstName, Comparator.nullsLast(Comparator.naturalOrder()))
                .compare(this, o);
    }
}

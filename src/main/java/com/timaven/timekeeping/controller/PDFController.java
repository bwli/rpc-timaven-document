package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.dto.ReportHourAndBillingParam;
import com.timaven.timekeeping.model.enums.ReportHourAndBillingType;
import com.timaven.timekeeping.service.ExportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping(path = "/v1")
@PreAuthorize("#oauth2.hasAnyScope('read')")
public class PDFController extends BaseController {

    private final ExportService exportService;

    @Autowired
    public PDFController(ExportService exportService) {
        this.exportService = exportService;
    }

    @GetMapping(value = "/sign-in-sheet", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> signInSheet(@RequestParam Long projectId,
                                                           @RequestParam(required = false) String signInSheet,
                                                           @RequestParam LocalDate dateOfService) {
        return exportService.exportSignInSheet(projectId, signInSheet, dateOfService);
    }

    @GetMapping(value = "/timesheet", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> timesheet(@RequestParam Long projectId,
                                                         @RequestParam(required = false) String team,
                                                         @RequestParam LocalDate dateOfService) {
        return exportService.exportTimesheet(projectId, team, dateOfService);
    }

    @GetMapping(value = "/report-ot", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> otReport(@RequestParam Long projectId,
                                                        @RequestParam LocalDate weekEndDate,
                                                        @RequestParam(required = false) LocalDate payrollDate) {
        return exportService.generateOtReport(projectId, weekEndDate, payrollDate);
    }

    @PostMapping(value = "/report-hours-billing")
    public ResponseEntity<InputStreamResource> reportHoursAndBilling(@RequestBody ReportHourAndBillingParam param,
                                                                     @RequestParam ReportHourAndBillingType type) {
        return exportService.reportHourAndBilling(param, type);
    }
}

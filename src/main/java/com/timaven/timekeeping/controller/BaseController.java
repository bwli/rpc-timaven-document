package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.dto.Principal;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

public class BaseController {
    public static Long getUserId() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        return ((Principal) principal).getId();
    }

    public static String getUsername() {
        if (SecurityContextHolder.getContext().getAuthentication() == null) return "Unknown";
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        return ((Principal) principal).getUsername();
    }

    public static String getDisplayName() {
        if (SecurityContextHolder.getContext().getAuthentication() == null) return "Unknown";
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        return Optional.ofNullable(((Principal) principal).getDisplayName()).orElse(getUsername());
    }
}

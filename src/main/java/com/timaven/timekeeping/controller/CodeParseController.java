package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.BillingCode;
import com.timaven.timekeeping.model.CommonResult;
import com.timaven.timekeeping.model.CostCodeLog;
import com.timaven.timekeeping.model.dto.TagCode;
import com.timaven.timekeeping.service.CodeService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static com.timaven.timekeeping.util.ExcelUtil.*;

@SuppressWarnings("Duplicates")
@RestController
@PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
public class CodeParseController extends BaseController {

    private final static String SHEET_NAME_BILLING_CODE = "data";
    private final static String KEY_BILLING_CODE_TYPE_ID = "typeId";
    private final static String KEY_PARSE_TYPE = "parseType";
    private final static String TYPE_DOT = "dot";
    private final static String TYPE_COLUMN = "column";
    private final static String KEY_PROJECT_Id = "projectId";
    private final static String KEY_DATE = "date";

    private final CodeService codeService;

    @Autowired
    public CodeParseController(CodeService codeService) {
        this.codeService = codeService;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @PostMapping(value = "/billing-codes")
    public CommonResult uploadBillingCodes(MultipartHttpServletRequest request) {
        // Getting uploaded files from the request object
        Map<String, MultipartFile> fileMap = request.getFileMap();
        CommonResult commonResult = new CommonResult();

        if (fileMap.size() == 1) {
            // Iterate through the map
            try {
                Workbook workbook = GlobalController.getWorkbook(fileMap);
                for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                    Sheet sheet = workbook.getSheetAt(i);
                    String sheetName = sheet.getSheetName().toLowerCase();
                    switch (sheetName) {
                        case SHEET_NAME_BILLING_CODE:
                            Long typeId = Long.valueOf(request.getParameterMap().get(KEY_BILLING_CODE_TYPE_ID)[0]);
                            Long projectId = null;
                            if (request.getParameterMap().get(KEY_PROJECT_Id) != null
                                    && request.getParameterMap().get(KEY_PROJECT_Id)[0] != null) {
                                projectId = Long.valueOf(request.getParameterMap().get(KEY_PROJECT_Id)[0]);
                            }
                            parseBillingCode(sheet, typeId, projectId, commonResult);
                            break;
                        default:
                            break;
                    }
                }
                commonResult.setResult("success");
                commonResult.addSuccess("Billing Code parse is complete.");
                return commonResult;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        commonResult.setResult("failed");
        commonResult.addError("Some exceptions happened.");
        return commonResult;
    }

    @PreAuthorize("hasAnyRole('ROLE_admin', 'ROLE_project_manager')")
    @PostMapping(value = "/cost-codes")
    public CommonResult uploadCostCodes(MultipartHttpServletRequest request) {
        // Getting uploaded files from the request object
        Map<String, MultipartFile> fileMap = request.getFileMap();
        CommonResult commonResult = new CommonResult();

        if (fileMap.size() == 1) {
            // Iterate through the map
            try {
                String parseType = request.getParameterMap().get(KEY_PARSE_TYPE)[0];
                Workbook workbook = GlobalController.getWorkbook(fileMap);
                String pickedDate = request.getParameterMap().get(KEY_DATE)[0];
                boolean invalidFile = true;
                Long projectId = null;
                if (request.getParameterMap().get(KEY_PROJECT_Id) != null
                        && request.getParameterMap().get(KEY_PROJECT_Id)[0] != null) {
                    projectId = Long.valueOf(request.getParameterMap().get(KEY_PROJECT_Id)[0]);
                }
                LocalDate effectiveDate = LocalDate.parse(pickedDate, DateTimeFormatter.ofPattern("MM/dd/yyyy"));
                for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                    Sheet sheet = workbook.getSheetAt(i);
                    String sheetName = sheet.getSheetName().toLowerCase();
                    switch (parseType) {
                        case TYPE_DOT:
                            if ("data".equalsIgnoreCase(sheetName) || "sheet1".equalsIgnoreCase(sheetName)) {
                                parseCostCodeByDot(sheet, projectId, effectiveDate, commonResult);
                                invalidFile = false;
                                commonResult.addSuccess("Parse cost codes separated by dots complete");
                            }
                            break;
                        case TYPE_COLUMN:
                            if ("data".equalsIgnoreCase(sheetName) || "sheet1".equalsIgnoreCase(sheetName)) {
                                parseCostCodeByColumn(sheet, projectId, effectiveDate, commonResult);
                                invalidFile = false;
                                commonResult.addSuccess("Parse cost codes separated by columns complete");
                            }
                            break;
                        default:
                            break;
                    }
                }

                if (invalidFile) {
                    commonResult.setResult("failed");
                    commonResult.addError("Cannot parse this file, please make sure the sheet name equals to \"data\".");
                } else {
                    commonResult.setResult("success");
                }
                return commonResult;

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        commonResult.setResult("failed");
        commonResult.addError("Some exceptions happened.");
        return commonResult;
    }

    private void parseBillingCode(Sheet sheet, Long typeId, Long projectId, CommonResult commonResult) {
        List<BillingCode> billingCodes = new ArrayList<>();

        Map<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (isRowEmpty(row)) {
                break;
            }
            BillingCode billingCode = null;
            if (rowNum != 0) {
                billingCode = new BillingCode();
            }
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (!StringUtils.isEmpty(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    switch (title) {
                        case "code name":
                            billingCode.setCodeName(getStringValue(cell));
                            break;
                        case "client alias":
                            billingCode.setClientAlias(getStringValue(cell));
                            break;
                        case "description":
                            billingCode.setDescription(getStringValue(cell));
                            break;
                        default:
                            break;
                    }
                }
            }
            if (rowNum != 0) {
                if (null != billingCode.getCodeName()) {
                    billingCodes.add(billingCode);
                }
            }
            rowNum++;
        }
        codeService.saveBillingCode(typeId, billingCodes, projectId, commonResult);
    }

    private void parseCostCodeByDot(Sheet sheet, Long projectId, LocalDate effectiveDate, CommonResult commonResult) {
        List<CostCodeLog> costCodeLogs = new ArrayList<>();

        Map<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (isRowEmpty(row)) {
                break;
            }
            CostCodeLog costCodeLog = null;
            if (rowNum != 0) {
                costCodeLog = new CostCodeLog();
            }
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (!StringUtils.isEmpty(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    switch (title) {
                        case "cost code":
                            costCodeLog.setCostCodeFull(getStringValue(cell));
                            break;
                        case "description":
                            costCodeLog.setDescription(getStringValue(cell));
                            break;
                        default:
                            break;
                    }
                }
            }
            if (rowNum != 0) {
                if (StringUtils.hasText(costCodeLog.getCostCodeFull())) {
                    costCodeLog.setProjectId(projectId);
                    costCodeLogs.add(costCodeLog);
                }
            }
            rowNum++;
        }
        codeService.saveCostCodesSeparatedByDot(costCodeLogs, projectId, effectiveDate, commonResult);
    }

    private void parseCostCodeByColumn(Sheet sheet, Long projectId, LocalDate effectiveDate, CommonResult commonResult) {
        List<Map<String, String>> codes = new ArrayList<>();

        Map<Integer, String> indexTitleMap = new HashMap<>();

        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (isRowEmpty(row)) {
                break;
            }
            Map<String, String> codeMap = null;
            if (rowNum != 0) {
                codeMap = new HashMap<>();
            }
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    if (cIndex == 0) {
                        indexTitleMap.put(0, "--cost code--");
                    } else {
                        title = getStringValue(cell).toLowerCase().trim();
                        if (!StringUtils.isEmpty(title)) {
                            indexTitleMap.put(cIndex, title);
                        }
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    String value = getStringValue(cell);
                    if (StringUtils.hasText(value)) {
                        codeMap.put(title, value);
                    }
                }
            }
            if (rowNum != 0) {
                if (null != codeMap.get("--cost code--")) {
                    codes.add(codeMap);
                }
            }
            rowNum++;
        }
        codeService.saveCostCodesSeparatedByColumn(codes, projectId, effectiveDate, commonResult);
    }


    @PreAuthorize("hasAnyRole('ROLE_project_manager')")
    @PostMapping(value = "/tags")
    public CommonResult uploadTags(MultipartHttpServletRequest request) {
        // Getting uploaded files from the request object
        Map<String, MultipartFile> fileMap = request.getFileMap();
        CommonResult commonResult = new CommonResult();

        if (fileMap.size() == 1) {
            // Iterate through the map
            try {
                Workbook workbook = GlobalController.getWorkbook(fileMap);
                boolean invalidFile = true;
                Long projectId = null;
                if (request.getParameterMap().get(KEY_PROJECT_Id) != null
                        && request.getParameterMap().get(KEY_PROJECT_Id)[0] != null) {
                    projectId = Long.valueOf(request.getParameterMap().get(KEY_PROJECT_Id)[0]);
                }
                for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                    Sheet sheet = workbook.getSheetAt(i);
                    String sheetName = sheet.getSheetName().toLowerCase();
                    if ("data".equalsIgnoreCase(sheetName) || "sheet1".equalsIgnoreCase(sheetName)) {
                        parseTags(sheet, projectId, commonResult);
                        invalidFile = false;
                        commonResult.addSuccess("Parse tags complete");
                    }
                }

                if (invalidFile) {
                    commonResult.setResult("failed");
                    commonResult.addError("Cannot parse this file, please make sure the sheet name equals to \"data\".");
                } else {
                    commonResult.setResult("success");
                }
                return commonResult;

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        commonResult.setResult("failed");
        commonResult.addError("Some exceptions happened.");
        return commonResult;
    }

    private void parseTags(Sheet sheet, Long projectId, CommonResult commonResult) {
        List<TagCode> tagCodes = new ArrayList<>();
        Map<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (isRowEmpty(row)) {
                break;
            }
            TagCode tagCode = null;
            if (rowNum != 0) {
                tagCode = new TagCode();
            }
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (!StringUtils.isEmpty(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    switch (title) {
                        case "tag":
                        case "tag name":
                            tagCode.setTag(getStringValue(cell));
                            break;
                        case "tag description":
                            tagCode.setTagDescription(getStringValue(cell));
                            break;
                        case "code":
                        case "code name":
                            tagCode.setCode(getStringValue(cell));
                            break;
                        case "code description":
                            tagCode.setCodeDescription(getStringValue(cell));
                            break;
                        default:
                            break;
                    }
                }
            }
            if (rowNum != 0) {
                if (StringUtils.hasText(tagCode.getTag())) {
                    tagCodes.add(tagCode);
                }
            }
            rowNum++;
        }
        codeService.saveTags(tagCodes, projectId, commonResult);
    }

}

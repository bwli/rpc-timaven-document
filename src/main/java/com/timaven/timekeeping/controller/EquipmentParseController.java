package com.timaven.timekeeping.controller;

import com.timaven.timekeeping.model.CommonResult;
import com.timaven.timekeeping.model.Equipment;
import com.timaven.timekeeping.model.EquipmentPrice;
import com.timaven.timekeeping.model.EquipmentUsage;
import com.timaven.timekeeping.model.enums.EquipmentHourlyType;
import com.timaven.timekeeping.service.ParserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.math.BigDecimal;
import java.util.*;

import static com.timaven.timekeeping.util.ExcelUtil.*;

@RestController
@PreAuthorize("hasRole('ROLE_admin') or hasRole('ROLE_project_manager')")
public class EquipmentParseController extends BaseController {

    private final static String SHEET_NAME_EQUIPMENT_ROSTER = "equipment_roster";
    private final static String SHEET_NAME_EQUIPMENT_PRICE = "equipment_price";
    private final static String SHEET_NAME_EQUIPMENT_LOG = "equipment_log";

    private final ParserService mParserService;

    @Autowired
    public EquipmentParseController(ParserService parserService) {
        mParserService = parserService;
    }

    @PostMapping(value = "/equipment")
    public CommonResult equipment(MultipartHttpServletRequest request) {
        // Getting uploaded files from the request object
        Map<String, MultipartFile> fileMap = request.getFileMap();

        CommonResult commonResult = new CommonResult();

        if (fileMap.size() == 1) {
            // Iterate through the map
            try {
                Workbook workbook = GlobalController.getWorkbook(fileMap);
                boolean hasValidSheet = false;
                for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                    Sheet sheet = workbook.getSheetAt(i);
                    String sheetName = sheet.getSheetName().toLowerCase();
                    switch (sheetName) {
                        case SHEET_NAME_EQUIPMENT_ROSTER:
                            parseEquipmentRoster(sheet);
                            commonResult.addSuccess("Equipment roaster parse is complete.");
                            hasValidSheet = true;
                            break;
                        default:
                            break;
                    }
                }
                if (hasValidSheet) {
                    commonResult.setResult("success");
                } else {
                    commonResult.setResult("failed");
                    commonResult.addError("Couldn't find any valid sheet.");
                }
                return commonResult;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        commonResult.setResult("failed");
        commonResult.addError("Some exceptions happened.");
        return commonResult;
    }

    @PreAuthorize("hasRole('ROLE_project_manager')")
    @PostMapping(value = "/equipment-price")
    public CommonResult equipmentPrice(MultipartHttpServletRequest request, @RequestParam Long projectId) {
        // Getting uploaded files from the request object
        Map<String, MultipartFile> fileMap = request.getFileMap();

        CommonResult commonResult = new CommonResult();

        if (fileMap.size() == 1) {
            // Iterate through the map
            try {
                Workbook workbook = GlobalController.getWorkbook(fileMap);
                boolean hasValidSheet = false;
                for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                    Sheet sheet = workbook.getSheetAt(i);
                    String sheetName = sheet.getSheetName().toLowerCase();
                    switch (sheetName) {
                        case SHEET_NAME_EQUIPMENT_PRICE:
                            parseEquipmentPrice(sheet, projectId);
                            commonResult.addSuccess("Equipment Price parse is complete.");
                            hasValidSheet = true;
                            break;
                        default:
                            break;
                    }
                }
                if (hasValidSheet) {
                    commonResult.setResult("success");
                } else {
                    commonResult.setResult("failed");
                    commonResult.addError("Couldn't find any valid sheet.");
                }
                return commonResult;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        commonResult.setResult("failed");
        commonResult.addError("Some exceptions happened.");
        return commonResult;
    }

    @PreAuthorize("hasRole('ROLE_project_manager')")
    @PostMapping(value = "/equipment-log")
    public CommonResult equipmentLog(MultipartHttpServletRequest request, @RequestParam Long projectId) {
        // TM-300 improt equipment log info
        // Getting uploaded files from the request object
        Map<String, MultipartFile> fileMap = request.getFileMap();

        CommonResult commonResult = new CommonResult();

        if (fileMap.size() == 1) {
            // Iterate through the map
            try {
                Workbook workbook = GlobalController.getWorkbook(fileMap);
                boolean hasValidSheet = false;
                for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
                    Sheet sheet = workbook.getSheetAt(i);
                    String sheetName = sheet.getSheetName().toLowerCase();
                    switch (sheetName) {
                        case SHEET_NAME_EQUIPMENT_LOG:
                            parseEquipmentLog(sheet, projectId);
                            commonResult.addSuccess("Equipment Log parse is complete.");
                            hasValidSheet = true;
                            break;
                        default:
                            break;
                    }
                }
                if (hasValidSheet) {
                    commonResult.setResult("success");
                } else {
                    commonResult.setResult("failed");
                    commonResult.addError("Couldn't find any valid sheet.");
                }
                return commonResult;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        commonResult.setResult("failed");
        commonResult.addError("Some exceptions happened.");
        return commonResult;
    }

    private void parseEquipmentRoster(Sheet sheet) {
        List<Equipment> equipmentList = new ArrayList<>();

        HashMap<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (isRowEmpty(row)) {
                break;
            }
            Equipment equipment = null;
            if (rowNum != 0) {
                equipment = new Equipment();
            }
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (!StringUtils.isEmpty(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    switch (title) {
                        case "id":
                            equipment.setEquipmentId(getStringValue(cell));
                            break;
                        case "description":
                            equipment.setDescription(getStringValue(cell));
                            break;
                        case "serial number":
                            equipment.setSerialNumber(getStringValue(cell));
                            break;
                        case "department":
                            equipment.setDepartment(getStringValue(cell));
                            break;
                        case "alias":
                            equipment.setAlias(getStringValue(cell));
                            break;
                        case "class":
                            equipment.setEquipmentClass(getStringValue(cell));
                            break;
                        case "hourly type":
                            equipment.setHourlyType(EquipmentHourlyType.fromString(getStringValue(cell)));
                            break;
                        default:
                            break;
                    }
                }
            }
            if (rowNum != 0 && equipment.getEquipmentId() != null
                    && StringUtils.isNotEmpty(equipment.getEquipmentClass())) {
                equipmentList.add(equipment);
            }
            rowNum++;
        }

        mParserService.parseEquipments(equipmentList);
    }

    private void parseEquipmentPrice(Sheet sheet, Long projectId) {
        List<EquipmentPrice> equipmentPriceList = new ArrayList<>();

        HashMap<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (isRowEmpty(row)) {
                break;
            }
            EquipmentPrice equipmentPrice = null;
            if (rowNum != 0) {
                equipmentPrice = new EquipmentPrice();
            }
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (!StringUtils.isEmpty(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    switch (title) {
                        case "class":
                            equipmentPrice.setEquipmentClass(getStringValue(cell));
                            break;
                        case "description":
                            equipmentPrice.setDescription(getStringValue(cell));
                            break;
                        case "hourly rate":
                            Double hourlyRate = getDoubleValue(cell);
                            if (null != hourlyRate) {
                                equipmentPrice.setHourlyRate(BigDecimal.valueOf(hourlyRate));
                            }
                            break;
                        case "daily rate":
                            Double dailyRate = getDoubleValue(cell);
                            if (null != dailyRate) {
                                equipmentPrice.setDailyRate(BigDecimal.valueOf(dailyRate));
                            }
                            break;
                        case "weekly rate":
                            Double weeklyRate = getDoubleValue(cell);
                            if (null != weeklyRate) {
                                equipmentPrice.setWeeklyRate(BigDecimal.valueOf(weeklyRate));
                            }
                            break;
                        case "monthly rate":
                            Double monthlyRate = getDoubleValue(cell);
                            if (null != monthlyRate) {
                                equipmentPrice.setMonthlyRate(BigDecimal.valueOf(monthlyRate));
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            if (rowNum != 0 && StringUtils.isNotEmpty(equipmentPrice.getEquipmentClass())) {
                equipmentPrice.setProjectId(projectId);
                equipmentPriceList.add(equipmentPrice);
            }
            rowNum++;
        }

        mParserService.parseEquipmentPrices(equipmentPriceList, projectId);
    }

    private void parseEquipmentLog(Sheet sheet , Long projectId) {
        // TM-300 improt equipment log info
        Map<String , Equipment> equipmentMap = new HashMap<>();
        List<EquipmentUsage> equipmentUsageList = new ArrayList<>();

        HashMap<Integer, String> indexTitleMap = new HashMap<>();
        Iterator<Row> rowIterator = sheet.rowIterator();
        int rowNum = 0;
        Equipment equipmentNext = null;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            if (isRowEmpty(row)) {
                break;
            }
            EquipmentUsage equipmentUsage = null;
            Equipment equipment = null;
            if (rowNum != 0) {
                equipment = new Equipment();
                equipmentUsage = new EquipmentUsage();
            }
            for (Cell cell : row) {
                String title;
                int cIndex = cell.getColumnIndex();
                if (rowNum == 0) {
                    // Header
                    title = getStringValue(cell).toLowerCase().trim();
                    if (!StringUtils.isEmpty(title)) {
                        indexTitleMap.put(cIndex, title);
                    }
                } else {
                    if (!indexTitleMap.containsKey(cIndex)) {
                        continue;
                    }
                    title = indexTitleMap.get(cIndex);
                    switch (title) {
                        case "id":
                            equipment.setEquipmentId(getStringValue(cell));
                            break;
                        case "description":
                            equipment.setDescription(getStringValue(cell));
                            break;
                        case "serial number":
                            equipment.setSerialNumber(getStringValue(cell));
                            break;
                        case "department":
                            equipment.setDepartment(getStringValue(cell));
                            break;
                        case "alias":
                            equipment.setAlias(getStringValue(cell));
                            break;
                        case "class":
                            equipment.setEquipmentClass(getStringValue(cell));
                            break;
                        case "hourly type":
                            equipment.setHourlyType(EquipmentHourlyType.fromString(getStringValue(cell)));
                            break;
                        case "user":
                            equipment.setHourlyType(EquipmentHourlyType.fromString(getStringValue(cell)));
                            break;
                        case "start date":
                            equipmentUsage.setStartDate(getLocalDate(cell));
                            break;
                        case "end date":
                            equipmentUsage.setEndDate(getLocalDate(cell));
                            break;
                        default:
                            break;
                    }
                }
            }
            if (rowNum != 0) {
                // save equipment usage info
                if(equipment.getEquipmentId() != null && StringUtils.isNotEmpty(equipment.getEquipmentClass())){
                    equipmentMap.put(equipment.getEquipmentId(),equipment);
                    equipmentNext = equipment;
                    equipmentUsage.setEquipmentId(Long.valueOf(equipment.getEquipmentId()));
                }else{
                    equipmentUsage.setEquipmentId(Long.valueOf(equipmentNext.getEquipmentId()));
                }
                if(equipmentUsage.getStartDate() != null){
                    equipmentUsage.setProjectId(projectId);
                    equipmentUsageList.add(equipmentUsage);
                }
            }
            rowNum++;
        }
        mParserService.parseEquipmentLog(equipmentMap,equipmentUsageList);
    }
}

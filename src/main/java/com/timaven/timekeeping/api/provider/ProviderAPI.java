package com.timaven.timekeeping.api.provider;

import com.timaven.timekeeping.api.ApiBinding;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;
import com.timaven.timekeeping.model.dto.*;
import com.timaven.timekeeping.model.enums.BillingCodeTypeParsedBy;
import com.timaven.timekeeping.model.enums.CostCodeType;
import com.timaven.timekeeping.model.enums.WeeklyProcessType;
import com.timaven.timekeeping.model.dto.AllocationTimeFilter;
import com.timaven.timekeeping.model.dto.CostCodeBillingCodeFilter;
import com.timaven.timekeeping.model.dto.EmpWorkdays;
import com.timaven.timekeeping.model.dto.EquipmentAllocationTimeFilter;
import com.timaven.timekeeping.model.enums.*;
import lombok.Getter;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.util.CollectionUtils;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Getter
public class ProviderAPI extends ApiBinding {

    private final WebClient webClient;

    private final String version = "/v1";

    public ProviderAPI(String providerApiUrl) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String accessToken = "";
        if (authentication != null && authentication.getDetails() instanceof OAuth2AuthenticationDetails) {
            accessToken = ((OAuth2AuthenticationDetails) authentication.getDetails()).getTokenValue();
        }
        String finalAccessToken = accessToken;
        this.webClient = WebClient.builder()
                .baseUrl(providerApiUrl)
                .filter(((clientRequest, exchangeFunction) -> {
                    ClientRequest clientRequest1 = ClientRequest.from(clientRequest)
                            .header("Authorization", "Bearer " + finalAccessToken)
                            .build();
                    return exchangeFunction.exchange(clientRequest1);
                }))
                .exchangeStrategies(
                        ExchangeStrategies.builder()
                                .codecs(clientCodecConfigurer ->
                                        clientCodecConfigurer.defaultCodecs().maxInMemorySize(16 * 1024 * 1024))
                                .build())
                .build();
    }

    public ProviderAPI(WebClient webClient) {
        this.webClient = webClient;
    }

    public TransSubmission saveTransSubmission(TransSubmission transSubmission) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder.path(version).path("/trans-submissions").build())
                .bodyValue(transSubmission)
                .retrieve()
                .bodyToMono(TransSubmission.class)
                .block();
    }

    public Mono<List<Employee>> getEmployees(EmployeeFilter filter) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder.path(version).path("/employees-filtered").build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(filter)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<Rule> findRuleByProjectId(Long projectId) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/rules")
                        .queryParam("projectId", projectId)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(Rule.class);
    }

    public Mono<ProjectShift> findProjectShiftByProjectId(Long projectId) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/project-shifts")
                        .queryParam("projectId", projectId)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(ProjectShift.class);
    }

    public Mono<Shift> findShiftById(Long id) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/shifts/{id}")
                        .build(id))
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(Shift.class);
    }

    public Mono<List<CostCodeLog>> findCostCodesByProjectAndDate(Long projectId,
                                                                 LocalDate startDate,
                                                                 LocalDate endDate,
                                                                 Boolean includeGlobal,
                                                                 Collection<String> costCodes) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/cost-code-log-filtered")
                        .queryParam("projectId", projectId)
                        .queryParam("startDate", startDate)
                        .queryParam("endDate", endDate)
                        .queryParam("includeGlobal", includeGlobal)
                        .build())
                .bodyValue(!CollectionUtils.isEmpty(costCodes) ? costCodes : new ArrayList<>())
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<Equipment>> findEquipmentByEquipmentIdIn(Set<String> equipmentIds) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/equipment-filtered")
                        .build())
                .bodyValue(equipmentIds)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public void saveEquipmentList(List<Equipment> equipmentList) {
        webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/equipment-collection")
                        .build())
                .bodyValue(equipmentList)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                })
                .subscribe();
    }

    public List<EquipmentPrice> findEquipmentPricesByProjectId(Long projectId) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/equipment-prices")
                        .queryParam("projectId", projectId)
                        .build())
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<EquipmentPrice>>() {
                })
                .block();
    }

    public void saveEquipmentPrices(List<EquipmentPrice> equipmentPriceList) {
        webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/equipment-price-collection")
                        .build())
                .bodyValue(equipmentPriceList)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                })
                .subscribe();
    }

    public Mono<StageSubmission> saveStageSubmission(StageSubmission stageSubmission) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/stage-submissions")
                        .build())
                .bodyValue(stageSubmission)
                .retrieve()
                .bodyToMono(StageSubmission.class);
    }

    public Mono<List<StageRoster>> saveStageRosters(List<StageRoster> rosters) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/stage-roster-collection")
                        .build())
                .bodyValue(rosters)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<StageCostCode>> saveStageCostCodes(List<StageCostCode> costCodes) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/stage-cost-code-collection")
                        .build())
                .bodyValue(costCodes)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<StageTeamCostCode>> saveStageTeamCostCodes(List<StageTeamCostCode> teamCostCodes) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/stage-team-cost-code-collection")
                        .build())
                .bodyValue(teamCostCodes)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<StageCraft>> saveStageCrafts(List<StageCraft> crafts) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/stage-craft-collection")
                        .build())
                .bodyValue(crafts)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public List<BillingCode> findBillingCodesByTypeIdAndCodeNameIn(Long typeId, Set<String> codeNames) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/billing-codes-filtered")
                        .queryParam("typeId", typeId)
                        .build())
                .bodyValue(codeNames)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<BillingCode>>() {
                })
                .block();
    }

    public List<BillingCodeOverride> findBillingCodeOverridesByProjectIdAndTypeIdAndCodeNameIn(Long projectId, Long typeId, Set<String> codeNames) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/billing-code-overrides-filtered")
                        .queryParam("projectId", projectId)
                        .queryParam("typeId", typeId)
                        .build())
                .bodyValue(codeNames)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<BillingCodeOverride>>() {
                })
                .block();
    }

    public void saveBillingCodeOverrides(Collection<BillingCodeOverride> billingCodeOverrides) {
        webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/billing-code-override-collection")
                        .build())
                .bodyValue(billingCodeOverrides)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                })
                .subscribe();
    }

    public void saveBillingCodes(Collection<BillingCode> billingCodes) {
        webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/billing-code-collection")
                        .build())
                .bodyValue(billingCodes)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                })
                .subscribe();
    }

    public Mono<List<BillingCodeType>> findBillingCodeTypes(Boolean levelNotNull) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/billing-code-types")
                        .queryParam("levelNotNull", levelNotNull)
                        .build())
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<BillingCodeType>>() {
                });
    }

    public List<CostCodeLog> saveCostCodeLogs(Collection<CostCodeLog> costCodeLogList) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/cost-code-log-collection")
                        .build())
                .bodyValue(costCodeLogList)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<CostCodeLog>>() {
                })
                .block();
    }

    public Mono<BillingCode> findBillingCodeByTypeIdAndCodeName(Long typeId, String codeName) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/billing-codes")
                        .queryParam("typeId", typeId)
                        .queryParam("codeName", codeName)
                        .build())
                .retrieve()
                .bodyToMono(BillingCode.class);
    }

    public Mono<Void> saveCostCodeBillingCodeByIds(Long costCodeId, Long billingCodeId) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/cost-code-billing-codes")
                        .queryParam("costCodeId", costCodeId)
                        .queryParam("billingCodeId", billingCodeId)
                        .build())
                .retrieve()
                .bodyToMono(Void.class);
    }

    public Mono<CostCodeTagType> findCostCodeTagTypeByProjectIdAndTagType(Long projectId, String tag) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/cost-code-tag-types")
                        .queryParam("projectId", projectId)
                        .queryParam("tagType", tag)
                        .build())
                .retrieve()
                .bodyToMono(CostCodeTagType.class);
    }

    public Mono<CostCodeTagType> saveCostCodeTagType(CostCodeTagType costCodeTagType) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/cost-code-tag-types")
                        .build())
                .bodyValue(costCodeTagType)
                .retrieve()
                .bodyToMono(CostCodeTagType.class);
    }

    public Mono<CostCodeTag> findCostCodeTagByTypeIdAndCodeName(Long typeId, String codeName) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/cost-code-tags")
                        .queryParam("typeId", typeId)
                        .queryParam("codeName", codeName)
                        .build())
                .retrieve()
                .bodyToMono(CostCodeTag.class);
    }

    public Mono<CostCodeTag> saveCostCodeTag(CostCodeTag costCodeTag) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/cost-code-tags")
                        .build())
                .bodyValue(costCodeTag)
                .retrieve()
                .bodyToMono(CostCodeTag.class);
    }

    public Mono<PurchaseOrderSubmission> savePurchaseOrderSubmission(PurchaseOrderSubmission purchaseOrderSubmission) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/purchase-order-submissions")
                        .build())
                .bodyValue(purchaseOrderSubmission)
                .retrieve()
                .bodyToMono(PurchaseOrderSubmission.class);
    }

    public Flux<Requisition> findRequisitionsByPurchaseOrderNumberNull() {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/requisitions-without-po")
                        .build())
                .retrieve()
                .bodyToFlux(Requisition.class);
    }

    public Mono<Requisition> saveRequisition(Requisition requisition) {
        return webClient.put()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/requisitions")
                        .build())
                .bodyValue(requisition)
                .retrieve()
                .bodyToMono(Requisition.class);
    }

    public Mono<Void> deletePurchaseOrderGroupByPurchaseOrderNumbers(Collection<String> purchaseOrderNumbers) {
        return webClient
                .method(HttpMethod.DELETE)
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/purchase-order-groups")
                        .build())
                .bodyValue(purchaseOrderNumbers)
                .retrieve()
                .bodyToMono(Void.class);
    }

    public Flux<PurchaseOrderGroup> findPurchaseOrderGroupByPurchaseOrderNumbers(Set<String> purchaseOrderNumbers) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/purchase-order-groups-filtered")
                        .build())
                .bodyValue(purchaseOrderNumbers)
                .retrieve()
                .bodyToFlux(PurchaseOrderGroup.class);
    }

    public Mono<PurchaseOrderGroup> savePurchaseOrderGroup(PurchaseOrderGroup purchaseOrderGroup) {
        return webClient.put()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/purchase-order-groups")
                        .build())
                .bodyValue(purchaseOrderGroup)
                .retrieve()
                .bodyToMono(PurchaseOrderGroup.class);
    }

    public Mono<PurchaseOrderSubmission> getLatestPurchaseOrderSubmission() {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/latest-purchase-order-submission")
                        .build())
                .retrieve()
                .bodyToMono(PurchaseOrderSubmission.class);
    }

    public void savePurchaseOrders(Set<PurchaseOrder> purchaseOrders) {
        webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/purchase-order-collection")
                        .build())
                .bodyValue(purchaseOrders)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                })
                .subscribe();
    }

    public Mono<CommonResult> updateProjectSetUp() {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/stage-project")
                        .build())
                .retrieve()
                .bodyToMono(CommonResult.class);
    }

    public Mono<CommonResult> updateCraftSubmission() {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/stage-craft")
                        .build())
                .retrieve()
                .bodyToMono(CommonResult.class);
    }

    public Mono<List<EmpWorkdays>> findDayOffEmployees(Set<String> employeeIds, LocalDate dateOfService, long days) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder.path(version).path("/employees-day-off")
                        .queryParam("dateOfService", dateOfService)
                        .queryParam("days", days)
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(employeeIds)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<Project> findProjectById(Long projectId) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/projects/{id}")
                        .build(projectId))
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(Project.class);
    }

    public Mono<Shift> findDefaultShiftByProjectId(Long projectId) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/default-shifts")
                        .queryParam("projectId", projectId)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(Shift.class);
    }

    public void saveImportEquipmentUsageAll(List<EquipmentUsage> equipmentUsageList) {
        webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/equipment-usage-collection")
                        .build())
                .bodyValue(equipmentUsageList)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                })
                .subscribe();
    }

    public Mono<List<TeamCostCode>> findTeamCostCodesByProjectIdAndTeamsAndDateOfService(Long projectId, Set<String> teams, LocalDate startDate, LocalDate endDate) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/team-cost-codes-filtered")
                        .queryParam("startDate", startDate)
                        .queryParam("projectId", projectId)
                        .queryParam("endDate", endDate)
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(teams)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<AllocationTime>> getAllocationTimes(AllocationTimeFilter filter) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/allocation-times-filtered")
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(filter)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<Set<CostCodeBillingCode>> findCostCodeBillingCodesByCostCodeIds(CostCodeBillingCodeFilter filter) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/cost-code-billing-codes-filtered")
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(filter)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<Set<CostCodeBillingCode>>() {
                });
    }

    public Mono<List<BillingCodeOverride>> findBillingCodeOverridesByProjectIdAndBillingCodeIdIn(Long projectId, Set<Long> billingCodeIds) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/billing-code-overrides-filtered")
                        .queryParam("projectId", projectId)
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(billingCodeIds)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<CostCodeTagAssociation>> findCostCodeTagAssociationsByKeysAndType(Set<Long> keys, CostCodeType type) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/cost-code-tag-association-collection")
                        .queryParam("costCodeType", type)
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(keys)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<BillingCodeType> findBillingCodeTypeById(Long billingCodeTypeId) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/billing-code-types/{id}")
                        .build(billingCodeTypeId))
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(BillingCodeType.class);
    }

    public Mono<WeeklyProcess[]> findWeeklyProcessesByProjectIdAndTeamAndDateAndType(LocalDate startDate,
                                                                                     LocalDate endDate,
                                                                                     WeeklyProcessType type,
                                                                                     Long projectId,
                                                                                     String teamName,
                                                                                     Set<String> includes) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/weekly-processes")
                        .queryParam("startDate", startDate)
                        .queryParam("endDate", endDate)
                        .queryParam("type", type)
                        .queryParam("projectId", projectId)
                        .queryParam("includes", includes)
                        .queryParam("teamName", teamName)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(WeeklyProcess[].class);
    }

    public Flux<WeeklyProcess> findWeeklyProcessesById(List<Long> ids, Collection<String> includes) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/weekly-process-collection")
                        .queryParam("includes", includes)
                        .build())
                .bodyValue(ids)
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToFlux(WeeklyProcess.class);
    }

    public Mono<Void> updateAllocationSubmissionsExport(Set<Long> allocationSubmissionIds, boolean exported) {
        return webClient.put()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/allocation-submissions/exported")
                        .queryParam("exported", exported)
                        .build())
                .bodyValue(allocationSubmissionIds)
                .retrieve()
                .bodyToMono(Void.class);
    }

    public Mono<List<CostCodeAssociation>> findCostCodeAssociationsByProjectIdAndDateOfService(Long projectId, LocalDate dateOfService, Collection<String> costCodes) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/cost-code-association-collection")
                        .queryParam("projectId", projectId)
                        .queryParam("dateOfService", dateOfService)
                        .build())
                .bodyValue(costCodes)
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<User>> findUsersById(Set<Long> userIds) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/user-collection")
                        .build())
                .bodyValue(userIds)
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<CostCodeAssociation>> findCostCodeAssociationsByProjectIdAndDates(Long projectId,
                                                                                       LocalDate startDate,
                                                                                       LocalDate endDate,
                                                                                       Collection<String> costCodes) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/cost-code-association-collection")
                        .queryParam("projectId", projectId)
                        .queryParam("startDate", startDate)
                        .queryParam("endDate", endDate)
                        .build())
                .bodyValue(costCodes)
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<EquipmentAllocationTime>> findEquipmentAllocationTimes(EquipmentAllocationTimeFilter filter) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/equipment-allocation-times-filtered")
                        .build())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(filter)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<PurchaseVendorRoster>> savePurchaseVendorRoster(List<PurchaseVendorRoster> purchaseVendorRosterList) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/purchase-vendor-roster")
                        .build())
                .bodyValue(purchaseVendorRosterList)
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    // TM-421
    public List<CraftDto> findCraftsByProjectId(Long projectId, LocalDate start) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/craft-dtos")
                        .queryParam("projectId", projectId)
                        .queryParam("start", start)
                        .build())
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<CraftDto>>() {})
                .block();
    }

    public Mono<List<BillingCode>> findBillingCodesByTypeIdAndValues(Long projectId,
                                                                     Long typeId,
                                                                     List<String> values,
                                                                     BillingCodeTypeParsedBy parsedBy) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/billing-codes-filtered")
                        .queryParam("typeId", typeId)
                        .queryParam("projectId", projectId)
                        .queryParam("parsedBy", parsedBy)
                        .build())
                .bodyValue(values)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<List<BillingCode>>() {
                });
    }


    public Mono<List<WeeklyProcess>> findWeeklyProcessesByProjectIdAndWeeklyEndDate(Set<Long> projectIds, LocalDate weekEndDate,
                                                                                    LocalDate weekStartDate,
                                                                                    AllocationSubmissionStatus status) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/weekly-process-collection")
                        .queryParam("weekEndDate", weekEndDate)
                        .queryParam("weekStartDate", weekStartDate)
                        .queryParam("status", status)
                        .build())
                .bodyValue(projectIds)
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<Project>> findProjectByIds(Set<Long> projectIds) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/project-collection")
                        .build())
                .bodyValue(projectIds)
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<EmployeeView>> findEmployeeByProjectIdsAndEmployeeIds(Set<Long> projectIds,
                                                                           Set<String> employeeIds) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/employee-views-filtered")
                        .queryParam("projectIds",projectIds)
                        .queryParam("employeeIds",employeeIds)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<PurchaseOrderBilling> findPurchaseOrderBillingByIdFetchingDetails(Long id) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/purchase-order-billing-details")
                        .queryParam("id",id)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(PurchaseOrderBilling.class);
    }

    public Mono<Set<CostCodeAssociation>> findCostCodeAssociationsByProjectIdAndCodes(Long projectId, LocalDate dateOfService, Collection<String> codes) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/cost-code-association-collection")
                        .queryParam("projectId", projectId)
                        .queryParam("startDateOfService", dateOfService)
                        .build())
                .bodyValue(codes)
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<EquipmentAllocationTime>> findEquipmentAllocationTimesByDate(Long projectId, LocalDate dateOfService, LocalDate payrollDate,
                                                                                  EquipmentAllocationSubmissionStatus status, EquipmentOwnershipType type) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/equipment-allocation-time-collection")
                        .queryParam("projectId", projectId)
                        .queryParam("dateOfService", dateOfService)
                        .queryParam("payrollDate", payrollDate)
                        .queryParam("status", status)
                        .queryParam("type", type)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<EquipmentAllocationTime>> findEquipmentAllocationTimesByDateRange(Long projectId, LocalDate startDate,
                                                                                       LocalDate endDate,
                                                                                       Set<EquipmentAllocationSubmissionStatus> statuses) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/equipment-allocation-time-collection")
                        .queryParam("projectId", projectId)
                        .queryParam("startDate", startDate)
                        .queryParam("endDate", endDate)
                        .build())
                .bodyValue(statuses)
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<Equipment>> findEquipmentByOwnershipType(EquipmentOwnershipType ownershipType) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/equipment")
                        .queryParam("ownershipType", ownershipType)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<Equipment>> findEquipmentRentalAndBeingUsedEquipments(Long projectId) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/equipment-logs")
                        .queryParam("projectId", projectId)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<EquipmentUsage>> findEquipmentUsagesByIdsAndDates(Long equipmentId, Long projectId,
                                                                  LocalDate startDate, LocalDate endDate) {
        return webClient.get()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/equipment-usages")
                        .queryParam("equipmentId", equipmentId)
                        .queryParam("projectId", projectId)
                        .queryParam("startDate", startDate)
                        .queryParam("endDate", endDate)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<EmployeeView>> findEmployeeByProjectIdAndShowAll(Long projectId, Boolean showAll) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/employee-view-collection")
                        .queryParam("projectId", projectId)
                        .queryParam("showAll", showAll)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<CostCodeExportView>> findCostCodeExportViewByProjectId(Long projectId) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/cost-code-export-view-collection")
                        .queryParam("projectId", projectId)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<BillingCodeType>> findBillingCodeTypeSort(String column) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/billing-code-type-sort")
                        .queryParam("column", column)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<Equipment>> findEquipmentAllOrByActiveTrue(Boolean isAll) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/equipment-collection")
                        .queryParam("isAll", isAll)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<Employee>> findEmployeeByIds(Set<Long> employeeIds) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/employee-collection")
                        .queryParam("employeeIds", employeeIds)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }

    public Mono<List<Employee>> findEmployeeByTeamNameAndProjectIdAndDateOfService(String teamName, Long projectId, LocalDate dateOfService) {
        return webClient.post()
                .uri("", uriBuilder -> uriBuilder
                        .path(version)
                        .path("/employee-collection")
                        .queryParam("teamName",teamName)
                        .queryParam("projectId",projectId)
                        .queryParam("dateOfService",dateOfService)
                        .build())
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals,
                        clientResponse -> Mono.empty())
                .bodyToMono(new ParameterizedTypeReference<>() {
                });
    }
}

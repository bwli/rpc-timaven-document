package com.timaven.timekeeping.api;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.api.rpc.RpcAPI;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.RequestScope;

@Configuration
public class APIConfig {

    @Value("${api.url.provider}")
    private String providerApiUrl;

    @Value("${api.url.rpc}")
    private String rpcApiUrl;

    @Bean
    @RequestScope
    public ProviderAPI providerAPI() {
        return new ProviderAPI(providerApiUrl);
    }

    @Bean
    @RequestScope
    public RpcAPI rpcAPI() {
        return new RpcAPI(rpcApiUrl);
    }
}

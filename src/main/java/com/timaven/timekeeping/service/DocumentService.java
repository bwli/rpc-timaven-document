package com.timaven.timekeeping.service;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.util.List;

public interface DocumentService {

    List<S3ObjectSummary> getAwsS3ListInfo(boolean isTemplate);

    ResponseEntity<InputStreamResource> downloadFile(String fileName, boolean isTemplate);

    ResponseEntity<Object> uploadCorporateFileByTenantId(MultipartFile multipartFile);

    ResponseEntity<Object> removeCompanyDocumentByKey(String fileName);

    void dumpDocuments(MultipartHttpServletRequest request);
}

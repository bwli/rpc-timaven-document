package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.*;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface ParserService {

    TransSubmission saveTransSubmission(TransSubmission transSubmission);

    void parsePunchRecord(Collection<EmpPunchRecords> empPunchRecords, Long projectId) throws Exception;

    boolean hasRuleAndShiftOfProject(Long projectId);

    CommonResult updateProjectSetUp();

    CommonResult updateCraftSubmission();

    void parseEquipments(List<Equipment> equipmentList);

    void parseEquipmentPrices(List<EquipmentPrice> equipmentPriceList, Long projectId);

    void parseEquipmentLog(Map<String, Equipment> equipmentMap, List<EquipmentUsage> equipmentUsageList);
}

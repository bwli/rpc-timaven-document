package com.timaven.timekeeping.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;

public interface ReportCorporateWeeklyProcessService {
    ResponseEntity<InputStreamResource> reportCorporateWeeklyProcess(LocalDate weeklyEndDate, String teamName);
}

package com.timaven.timekeeping.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;

public interface ReportEquipmentService {
    ResponseEntity<InputStreamResource> reportEquipemtByEquipmentOwnershipType(Long projectId, LocalDate dateOfService, LocalDate payrollDate, Integer type);

    ResponseEntity<InputStreamResource> reportEquipemt(Boolean all);
}

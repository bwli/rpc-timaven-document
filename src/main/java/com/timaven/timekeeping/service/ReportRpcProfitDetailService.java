package com.timaven.timekeeping.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;

public interface ReportRpcProfitDetailService {
    ResponseEntity<InputStreamResource> reportRpcProfitDetails(Long projectId, LocalDate startDate, LocalDate endDate);
}

package com.timaven.timekeeping.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;

public interface ReportOtService {
    ResponseEntity<InputStreamResource> generateOtReport1(Long projectId, LocalDate weekEndDate, LocalDate payrollDate);
}

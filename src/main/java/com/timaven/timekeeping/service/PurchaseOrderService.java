package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.PurchaseOrder;
import com.timaven.timekeeping.model.PurchaseOrderSubmission;
import com.timaven.timekeeping.model.PurchaseVendorRoster;
import com.timaven.timekeeping.model.dto.ReceivedPurchaseOrderDto;

import java.util.List;

public interface PurchaseOrderService {
    void savePurchaseOrders(List<PurchaseOrder> purchaseOrders, String submittedBy);

    void saveReceivedPurchases(List<ReceivedPurchaseOrderDto> receivedPurchaseOrderDtos, String submittedBy);

    PurchaseOrderSubmission getLatestPurchaseOrderSubmission();

    void savePurchaseVendorRoster(List<PurchaseVendorRoster> purchaseVendorRosterList);
}

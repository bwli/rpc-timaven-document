package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.dto.WorkbookInfo;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ExcelService {
    ResponseEntity<InputStreamResource> downloadEmpty();

    ResponseEntity<InputStreamResource> downloadExcel(String fname, Workbook workbook);

    ResponseEntity<InputStreamResource> downloadExcel(String fname, Workbook workbook, int firstRowNum);

    ResponseEntity<InputStreamResource> downloadZip(String zipFileName, List<WorkbookInfo> workbookInfoList);
}

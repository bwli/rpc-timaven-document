package com.timaven.timekeeping.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

public interface ReportDataExportService {
    ResponseEntity<InputStreamResource> reportDataExport(Long projectId, LocalDate startDate, LocalDate endDate, boolean approvedOnly);
}

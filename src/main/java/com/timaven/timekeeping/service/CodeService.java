package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.BillingCode;
import com.timaven.timekeeping.model.CommonResult;
import com.timaven.timekeeping.model.CostCodeLog;
import com.timaven.timekeeping.model.dto.TagCode;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface CodeService {
    void saveBillingCode(Long typeId, List<BillingCode> billingCodes, Long projectId, CommonResult commonResult);

    void saveCostCodesSeparatedByDot(List<CostCodeLog> costCodeLogs, Long projectId, LocalDate effectiveDate, CommonResult commonResult);

    void saveCostCodesSeparatedByColumn(List<Map<String, String>> codes, Long projectId, LocalDate effectiveDate, CommonResult commonResult);

    void saveTags(List<TagCode> tagCodes, Long projectId, CommonResult commonResult);
}

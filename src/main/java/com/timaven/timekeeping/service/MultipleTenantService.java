package com.timaven.timekeeping.service;

import com.timaven.timekeeping.model.dto.ReportHourAndBillingParam;
import com.timaven.timekeeping.model.enums.ReportHourAndBillingType;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public interface MultipleTenantService {
    String getTenantName();

    ResponseEntity<InputStreamResource> exportSignInSheet(Long projectId, String team, LocalDate dateOfService);

    ResponseEntity<InputStreamResource> exportTimesheet(Long projectId, String team, LocalDate dateOfService);

    ResponseEntity<InputStreamResource> generateOtReport(Long projectId, LocalDate weekEndDate, LocalDate payrollDate);

    ResponseEntity<InputStreamResource> reportHourAndBilling(ReportHourAndBillingParam param, ReportHourAndBillingType type);

    ResponseEntity<InputStreamResource> reportLabor(Long projectId, String teamName, LocalDate dateOfService, LocalDate payrollDate);
}

package com.timaven.timekeeping.service;


import com.timaven.timekeeping.model.dto.ECMSMapping;

public interface ECMSService {
    ECMSMapping getECMSMapping();
}

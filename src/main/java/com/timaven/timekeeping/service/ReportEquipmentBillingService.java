package com.timaven.timekeeping.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;

public interface ReportEquipmentBillingService {
    ResponseEntity<InputStreamResource> reportEquipemtBilling(Long projectId, LocalDate startDate, LocalDate endDate);
}

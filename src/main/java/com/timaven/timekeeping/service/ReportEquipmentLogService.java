package com.timaven.timekeeping.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;

public interface ReportEquipmentLogService {
    ResponseEntity<InputStreamResource> reportEquipemtTemplateLog(Long projectId);
}

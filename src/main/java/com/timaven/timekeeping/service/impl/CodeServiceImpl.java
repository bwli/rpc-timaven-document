package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.TagCode;
import com.timaven.timekeeping.model.enums.BillingCodeTypeParsedBy;
import com.timaven.timekeeping.service.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Transactional
public class CodeServiceImpl implements CodeService {

    private final ProviderAPI providerAPI;

    @Autowired
    public CodeServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public void saveBillingCode(Long typeId, List<BillingCode> billingCodes, Long projectId, CommonResult commonResult) {
        if (CollectionUtils.isEmpty(billingCodes)) return;
        Map<String, BillingCode> nameCodeMap = billingCodes.stream()
                .collect(Collectors.toMap(BillingCode::getCodeName, Function.identity()));
        Set<String> codeNames = nameCodeMap.keySet();
        List<BillingCode> existingCodes = providerAPI.findBillingCodesByTypeIdAndCodeNameIn(typeId, codeNames);
        Set<String> existingCodeNames= existingCodes.stream()
                .map(BillingCode::getCodeName).collect(Collectors.toSet());

        if (projectId != null) {
            List<BillingCodeOverride> existingBillingCodeOverrides = new ArrayList<>();
            if (!CollectionUtils.isEmpty(existingCodeNames)) {
                existingBillingCodeOverrides = providerAPI
                        .findBillingCodeOverridesByProjectIdAndTypeIdAndCodeNameIn(projectId, typeId, existingCodeNames);
            }
            Map<String, BillingCodeOverride> codeOverrideMap = existingBillingCodeOverrides.stream()
                    .collect(Collectors.toMap(BillingCodeOverride::getCodeName, Function.identity()));
            Set<BillingCodeOverride> billingCodeOverrides = new HashSet<>();
            for (BillingCode billingCode : existingCodes) {
                String codeName = billingCode.getCodeName();
                BillingCodeOverride override = codeOverrideMap.getOrDefault(codeName,
                        new BillingCodeOverride(billingCode.getId(), codeName, projectId));
                BillingCode newBillingCode = nameCodeMap.get(codeName);
                override.setDescription(newBillingCode.getDescription());
                override.setClientAlias(newBillingCode.getClientAlias());
                billingCodeOverrides.add(override);
            }
            billingCodeOverrides = billingCodeOverrides.stream()
                    .filter(o -> !codeOverrideMap.containsKey(o.getCodeName()))
                    .collect(Collectors.toSet());
            // Persist new overrides
            providerAPI.saveBillingCodeOverrides(billingCodeOverrides);
            Set<String> nonExistCodeNames = codeNames.stream()
                    .filter(c -> !existingCodeNames.contains(c))
                    .collect(Collectors.toSet());
            nonExistCodeNames.forEach(c -> commonResult.addWarning(String.format("Couldn't find code %s in the system", c)));
        } else {
            List<BillingCode> billingCodeUpdated = new ArrayList<>();
            for (BillingCode billingCode : existingCodes) {
                BillingCode updatedBillingCode = nameCodeMap.get(billingCode.getCodeName());
                billingCode.setClientAlias(updatedBillingCode.getClientAlias());
                billingCode.setDescription(updatedBillingCode.getDescription());
                billingCodeUpdated.add(billingCode);
            }
            List<BillingCode> newBillingCodes = billingCodes.stream()
                    .filter(c -> !existingCodeNames.contains(c.getCodeName())).collect(Collectors.toList());
            newBillingCodes.forEach(c -> c.setTypeId(typeId));
            providerAPI.saveBillingCodes(Stream.of(billingCodeUpdated, newBillingCodes).flatMap(Collection::stream).collect(Collectors.toList()));
        }
    }

    @Override
    public void saveCostCodesSeparatedByDot(List<CostCodeLog> costCodeLogs, Long projectId,
                                            LocalDate effectiveDate, CommonResult commonResult) {
        List<BillingCodeType> billingCodeTypes = providerAPI.findBillingCodeTypes(true).block();
        if (billingCodeTypes == null) billingCodeTypes = new ArrayList<>();

//        int subJobLevel = -1;
//        Optional<BillingCodeType> typeOptional =  billingCodeTypes.stream().filter(t -> "Sub Job".equals(t.getCodeType())).findFirst();
//        if (typeOptional.isPresent()) {
//            subJobLevel = typeOptional.get().getLevel();
//        }

        Set<String> costCodes = costCodeLogs.stream()
                .map(CostCodeLog::getCostCodeFull).collect(Collectors.toSet());
        Map<String, String> costCodeDescriptionMap = costCodeLogs.stream()
                .collect(Collectors.toMap(CostCodeLog::getCostCodeFull, CostCodeLog::getDescription, (v1, v2) -> v1));

        List<CostCodeLog> costCodeLogs1 = providerAPI.findCostCodesByProjectAndDate(projectId, LocalDate.now(), null, false, costCodes).block();
        if (costCodeLogs == null) costCodeLogs = new ArrayList<>();
        Set<String> existingCodes = costCodeLogs1.stream()
                        .map(CostCodeLog::getCostCodeFull).collect(Collectors.toSet());

        Set<String> costCodeToBeAdded = costCodes.stream()
                .filter(c -> !existingCodes.contains(c)).collect(Collectors.toSet());

        Map<String, String[]> codeSplit = costCodes.stream()
                .collect(Collectors.toMap(Function.identity(), c -> Arrays.stream(c.split("\\."))
                        .map(String::trim).toArray(String[]::new)));

        List<CostCodeLog> costCodeLogList = new ArrayList<>();

//        Set<Project> projectsWithSubJob = projectRepository.findBySubJobNotNull();
//
//        Map<String, Project> subJobProjectMap = projectsWithSubJob.stream()
//                .collect(Collectors.toMap(Project::getSubJob, Function.identity()));

        for (String costCode : costCodeToBeAdded) {
            CostCodeLog costCodeLog = new CostCodeLog();
            costCodeLog.setCostCodeFull(costCode);
            costCodeLog.setStartDate(effectiveDate);
            costCodeLog.setProjectId(projectId);
            costCodeLog.setDescription(costCodeDescriptionMap.get(costCode));

//            if (subJobLevel != -1) {
//                String[] splitCode = codeSplit.get(costCode);
//                if (splitCode.length >= subJobLevel) {
//                    String subJob = splitCode[subJobLevel - 1];
//                    if (subJobProjectMap.containsKey(subJob)) {
//                        costCodeLog.setProjectId(subJobProjectMap.get(subJob).getId());
//                    }
//                }
//            }
            costCodeLogList.add(costCodeLog);
        }

        Set<String> lockedCostCodes = costCodeLogs1.stream()
                .filter(CostCodeLog::isLocked)
                .map(CostCodeLog::getCostCodeFull)
                .collect(Collectors.toSet());

        if (!CollectionUtils.isEmpty(lockedCostCodes)) {
            costCodeLogs1.removeIf(CostCodeLog::isLocked);
            lockedCostCodes.forEach(cc -> commonResult
                    .addWarning(String.format("%s won't be updated because it's locked.", cc)));
        }
        costCodeLogs1.forEach(c -> c.setStartDate(effectiveDate));
        costCodeLogList = providerAPI.saveCostCodeLogs(Stream.of(costCodeLogs1, costCodeLogList).flatMap(Collection::stream).collect(Collectors.toList()));

        Map<String, Long> costCodeIdMap = costCodeLogList.stream()
                .collect(Collectors.toMap(CostCodeLog::getCostCodeFull, CostCodeLog::getId));

        Map<Long, Map<String, Long>> typeIdCodeIdMap = new HashMap<>();

        List<CostCodeBillingCode> costCodeBillingCodes = new ArrayList<>();
        for (BillingCodeType billingCodeType : billingCodeTypes) {
            Long typeId = billingCodeType.getId();
            typeIdCodeIdMap.putIfAbsent(typeId, new HashMap<>());
            Map<String, Long> codeIdMap = typeIdCodeIdMap.get(typeId);
            int level = billingCodeType.getLevel();
            for (String costCode : costCodes) {
                String[] splitCode = codeSplit.get(costCode);
                if (splitCode.length >= level) {
                    String codeName = splitCode[level - 1];
                    Long billingCodeId;
                    if (codeIdMap.containsKey(codeName)) {
                        billingCodeId = codeIdMap.get(codeName);
                    } else {
                        BillingCode billingCode = providerAPI.findBillingCodeByTypeIdAndCodeName(typeId, codeName).block();
                        billingCodeId = billingCode == null ? null : billingCode.getId();
                        codeIdMap.put(codeName, billingCodeId);
                    }
                    if (billingCodeId != null) {
                        CostCodeBillingCode costCodeBillingCode = new CostCodeBillingCode();
                        costCodeBillingCode.setCostCodeId(costCodeIdMap.get(costCode));
                        costCodeBillingCode.setBillingCodeId(billingCodeId);
                        costCodeBillingCodes.add(costCodeBillingCode);
                    }
                }
            }
        }
        Flux.fromIterable(costCodeBillingCodes)
                .flatMap(c -> providerAPI.saveCostCodeBillingCodeByIds(c.getCostCodeId(), c.getBillingCodeId()))
                .blockLast();
    }

    @Override
    public void saveCostCodesSeparatedByColumn(List<Map<String, String>> codes, Long projectId,
                                               LocalDate effectiveDate, CommonResult commonResult) {

        Set<String> fullCostCodes = codes.stream()
                .filter(Objects::nonNull)
                .map(m -> m.get("--cost code--"))
                .collect(Collectors.toSet());

        Map<String, String> costCodeDescriptionMap = codes.stream()
                .filter(m -> m.containsKey("--cost code--") && m.get("--cost code--") != null)
                .filter(m -> m.containsKey("description") && m.get("description") != null)
                .map(m -> Map.of(m.get("--cost code--"), m.get("description")))
                .flatMap(map -> map.entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (v1, v2) -> v1));

        List<CostCodeLog> costCodeLogs = providerAPI.findCostCodesByProjectAndDate(projectId, LocalDate.now(), null, false, fullCostCodes).block();
        if (costCodeLogs == null) costCodeLogs = new ArrayList<>();
        Set<String> existingCodes = costCodeLogs.stream()
                        .map(CostCodeLog::getCostCodeFull).collect(Collectors.toSet());
        Set<String> costCodeToBeAdded = fullCostCodes.stream()
                .filter(s -> !existingCodes.contains(s))
                .collect(Collectors.toSet());
        List<CostCodeLog> costCodeLogList = new ArrayList<>();

//        Set<Project> projectsWithSubJob = projectRepository.findBySubJobNotNull();
//
//        Map<String, Project> subJobProjectMap = projectsWithSubJob.stream()
//                .collect(Collectors.toMap(Project::getSubJob, Function.identity()));

        for (String costCode : costCodeToBeAdded) {
            CostCodeLog costCodeLog = new CostCodeLog();
            costCodeLog.setCostCodeFull(costCode);
            costCodeLog.setStartDate(effectiveDate);
            costCodeLog.setProjectId(projectId);
            costCodeLog.setDescription(costCodeDescriptionMap.get(costCode));
//            Map<String, String> codeTypeMap = codes.stream()
//                    .filter(m -> m.get("cost code").equalsIgnoreCase(costCode))
//                    .findFirst().orElse(new HashMap<>());
//            if (codeTypeMap.containsKey("sub job")) {
//                String subJob = codeTypeMap.get("sub job");
//                if (subJobProjectMap.containsKey(subJob)) {
//                    costCodeLog.setProjectId(subJobProjectMap.get(subJob).getId());
//                }
//            }
            costCodeLogList.add(costCodeLog);
        }

        Set<String> lockedCostCodes = costCodeLogs.stream()
                .filter(CostCodeLog::isLocked)
                .map(CostCodeLog::getCostCodeFull)
                .collect(Collectors.toSet());

        if (!CollectionUtils.isEmpty(lockedCostCodes)) {
            costCodeLogs.removeIf(CostCodeLog::isLocked);
            lockedCostCodes.forEach(cc -> commonResult
                    .addWarning(String.format("%s won't be updated because it's locked.", cc)));
        }

        costCodeLogs.forEach(c -> c.setStartDate(effectiveDate));
        costCodeLogList = providerAPI.saveCostCodeLogs(Stream.of(costCodeLogs, costCodeLogList).flatMap(Collection::stream).collect(Collectors.toList()));

        Map<String, Long> costCodeIdMap = costCodeLogList.stream()
                .collect(Collectors.toMap(CostCodeLog::getCostCodeFull, CostCodeLog::getId));

        List<CostCodeBillingCode> costCodeBillingCodes = new ArrayList<>();

        Map<String, List<String>> typeCodesMap = new HashMap<>();

        List<BillingCodeType> billingCodeTypes = providerAPI.findBillingCodeTypes(false).block();
        if (billingCodeTypes == null) billingCodeTypes = new ArrayList<>();
        Map<String, BillingCodeType> typeMap = billingCodeTypes.stream()
                .collect(Collectors.toMap(t -> t.getCodeType().toLowerCase().trim(),
                        Function.identity()));

        for (Map<String, String> codeTypeMap : codes) {
            String costCode = codeTypeMap.get("--cost code--");
            if (!StringUtils.hasText(costCode)) continue;
            for (Map.Entry<String, String> entry : codeTypeMap.entrySet()) {
                String title = entry.getKey();
                String value = entry.getValue();
                if (title.equalsIgnoreCase("--cost code--")) continue;
                if (typeMap.containsKey(title)) {
                    typeCodesMap.putIfAbsent(title, new ArrayList<>());
                    typeCodesMap.get(title).add(value);
                }
            }
        }

        List<Tuple2<BillingCodeType, List<BillingCode>>> typeBillingCodesTuple = Flux.fromIterable(typeCodesMap.entrySet())
                .flatMap(entry -> {
                    BillingCodeType billingCodeType = typeMap.get(entry.getKey());
                    Mono<List<BillingCode>> billingCodesMono = providerAPI
                            .findBillingCodesByTypeIdAndValues(projectId, billingCodeType.getId(),
                                    entry.getValue(), billingCodeType.getBillingCodeTypeParsedBy());
                    return Mono.zip(Mono.just(billingCodeType), billingCodesMono);
                })
                .collectList()
                .block();
        if (CollectionUtils.isEmpty(typeBillingCodesTuple)) return;
        Map<String, Map<String, BillingCode>> typeCodeBillingCodeMap = typeBillingCodesTuple
                .stream()
                .map(tuple -> {
                    BillingCodeType billingCodeType = tuple.getT1();
                    List<BillingCode> billingCodes = tuple.getT2();
                    return Map.of(billingCodeType.getCodeType().toLowerCase(),
                            billingCodes.stream().
                                    collect(Collectors.toMap(billingCodeType.getBillingCodeTypeParsedBy() == BillingCodeTypeParsedBy.CLIENT_ALIAS ? BillingCode::getClientAlias : BillingCode::getCodeName,
                                            Function.identity(), (e1, e2) -> e2)));
                })
                .flatMap(m -> m.entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2));

        for (Map<String, String> codeTypeMap : codes) {
            String costCode = codeTypeMap.get("--cost code--");
            if (!StringUtils.hasText(costCode)) continue;
            for (Map.Entry<String, String> entry : codeTypeMap.entrySet()) {
                String key = entry.getKey();
                String codeName = entry.getValue();
                if (key.equalsIgnoreCase("--cost code--")) continue;
                Map<String, BillingCode> codeBillingCodeMap = typeCodeBillingCodeMap.getOrDefault(key, new HashMap<>());
                BillingCode billingCode = codeBillingCodeMap.get(codeName);
                if (billingCode != null) {
                    CostCodeBillingCode costCodeBillingCode = new CostCodeBillingCode();
                    costCodeBillingCode.setCostCodeId(costCodeIdMap.get(costCode));
                    costCodeBillingCode.setBillingCodeId(billingCode.getId());
                    costCodeBillingCodes.add(costCodeBillingCode);
                }
            }
        }

        Flux.fromIterable(costCodeBillingCodes)
                .flatMap(c -> providerAPI.saveCostCodeBillingCodeByIds(c.getCostCodeId(), c.getBillingCodeId()))
                .blockLast();
    }

    @Override
    public void saveTags(List<TagCode> tagCodes, Long projectId, CommonResult commonResult) {
        if (CollectionUtils.isEmpty(tagCodes)) return;
        Map<String, List<TagCode>> tagCodeMap = tagCodes.stream()
                .collect(Collectors.groupingBy(TagCode::getTag));
        Flux.fromIterable(tagCodeMap.entrySet())
                .flatMap(entry -> providerAPI.findCostCodeTagTypeByProjectIdAndTagType(projectId, entry.getValue().get(0).getTag())
                        .switchIfEmpty(Mono.deferContextual(ctx -> {
                            CostCodeTagType costCodeTagType = new CostCodeTagType();
                            costCodeTagType.setProjectId(projectId);
                            costCodeTagType.setTagType(entry.getValue().get(0).getTag());
                            costCodeTagType.setDescription(entry.getValue().get(0).getTagDescription());
                            return ctx.get(ProviderAPI.class).saveCostCodeTagType(costCodeTagType);
                        }))
                        .flatMapMany(costCodeTagType -> {
                            Long costCodeTagTypeId = costCodeTagType.getId();
                            List<TagCode> tagCodeList = entry.getValue();
                            return Flux.fromIterable(tagCodeList)
                                    .flatMap(tagCode -> Mono.deferContextual(Mono::just)
                                            .flatMap(ctx -> {
                                                ProviderAPI api = ctx.get(ProviderAPI.class);
                                                return api.findCostCodeTagByTypeIdAndCodeName(costCodeTagTypeId, tagCode.getCode())
                                                        .switchIfEmpty(Mono.defer(() -> {
                                                            CostCodeTag costCodeTag = new CostCodeTag();
                                                            costCodeTag.setTypeId(costCodeTagTypeId);
                                                            costCodeTag.setCodeName(tagCode.getCode());
                                                            costCodeTag.setDescription(tagCode.getCodeDescription());
                                                            return api.saveCostCodeTag(costCodeTag);
                                                        }))
                                                        .flatMap(costCodeTag -> {
                                                            costCodeTag.setDescription(tagCode.getCodeDescription());
                                                            return api.saveCostCodeTag(costCodeTag);
                                                        });
                                            })
                                    );
                        })
                )
                .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())))
                .blockLast();
    }
}

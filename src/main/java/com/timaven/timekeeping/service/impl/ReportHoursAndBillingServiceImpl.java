package com.timaven.timekeeping.service.impl;

import be.quodlibet.boxable.BaseTable;
import be.quodlibet.boxable.HorizontalAlignment;
import be.quodlibet.boxable.Row;
import be.quodlibet.boxable.VerticalAlignment;
import be.quodlibet.boxable.image.Image;
import be.quodlibet.boxable.line.LineStyle;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.configuration.tenant.TenantProperties;
import com.timaven.timekeeping.configuration.web.ThreadTenantStorage;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.*;
import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;
import com.timaven.timekeeping.model.enums.BillingCodeTypeParsedBy;
import com.timaven.timekeeping.model.enums.CostCodeType;
import com.timaven.timekeeping.model.enums.ReportHourAndBillingType;
import com.timaven.timekeeping.service.ReportHoursAndBillingService;
import com.timaven.timekeeping.util.LocalDateUtil;
import com.timaven.timekeeping.util.NumberUtil;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuple4;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.timaven.timekeeping.util.PdfUtility.generateCells;
import static com.timaven.timekeeping.util.PdfUtility.hex2Rgb;
import static java.util.stream.Collectors.toSet;

@Service
public class ReportHoursAndBillingServiceImpl extends PdfBaseService implements ReportHoursAndBillingService {

    protected ReportHoursAndBillingServiceImpl(ProviderAPI providerAPI, TenantProperties tenantProperties) {
        super(providerAPI, tenantProperties);
    }

    @Override
    public ResponseEntity<InputStreamResource> reportHourAndBilling(ReportHourAndBillingParam param, ReportHourAndBillingType type) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        Set<Long> projectIds = param.getProjectIds();
        List<String> groupingBy = param.getGroupingBy();
        final LocalDate startDate = LocalDate.parse(param.getStartDate(), formatter);
        final LocalDate endDate = LocalDate.parse(param.getEndDate(), formatter);
        final boolean approvedOnly = param.getApprovedOnly() != null && param.getApprovedOnly();
        final boolean hideDetail = param.getHideDetail() != null && param.getHideDetail();
        final String shift = param.getShift();
        final boolean requireLabor;
        final boolean requirePerDiem;
        final boolean requireProject;
        final boolean requireMob;
        final boolean requireBillingCode = !CollectionUtils.isEmpty(groupingBy);
        final Function<? super Map<String, Object>, ?> groupKeyMapper;
        String headerTitle = "";
        String filename;
        switch (type) {
            case EMPLOYEE_HOURS_BILLING -> {
                requireLabor = true;
                requirePerDiem = true;
                requireProject = false;
                requireMob = true;
                headerTitle = "Summary of Employee Hours and Billing by Tracking Category";
                groupKeyMapper = (e) -> e.get("empId");
                filename = "Summary of Employee Hours and Billing by Tracking Category.pdf";
            }
            case EMPLOYEE_HOURS -> {
                requireLabor = true;
                requirePerDiem = false;
                requireProject = true;
                requireMob = false;
                headerTitle = "Employee Hours Summary";
                groupKeyMapper = (e) -> String.format("%s %s %s", e.get("empId"), e.get("craft"), e.get("dateOfService")).toLowerCase();
                filename = "Summary of Employee Hours.pdf";
            }
            case CLASSIFICATION_CODE -> {
                requireLabor = true;
                requirePerDiem = false;
                requireProject = true;
                requireMob = false;
                headerTitle = "Classification Codes";
                groupKeyMapper = (e) -> String.format("%s %s", e.get("craft"), e.get("timesheet")).toLowerCase();
                filename = "Classification Codes.pdf";
            }
            case PER_DIEM_EXPENSE -> {
                requireLabor = false;
                requirePerDiem = true;
                requireProject = true;
                requireMob = false;
                headerTitle = "Per Diem Expense Summary";
                groupKeyMapper = (e) -> String.format("%s %s %s", e.get("empId"), e.get("craft"), e.get("dateOfService")).toLowerCase();
                filename = "Per Diem Expense Summary..pdf";
            }
            case COST_BY_TOTALS -> {
                requireLabor = true;
                requirePerDiem = false;
                requireProject = true;
                requireMob = false;
                headerTitle = "Cost Report by Totals";
                groupKeyMapper = (e) -> ((String) e.get("timesheet")).toLowerCase();
                filename = "Cost Report by Totals.pdf";
            }
            case TRAVEL_PAY_SUMMARY -> {
                requireLabor = false;
                requirePerDiem = false;
                requireProject = true;
                requireMob = true;
                headerTitle = "Weekly Travel Pay Expense Summary";
                groupKeyMapper = (e) -> String.format("%s %s %s", e.get("empId"), e.get("craft"), e.get("dateOfService")).toLowerCase();
                filename = "Weekly Travel Pay Expense Summary.pdf";
            }
            default -> throw new IllegalArgumentException("Not implemented");
        }

        final List<String> finalGroupingBy = groupingBy.stream()
                .map(s -> {
                    if (s.startsWith("segment")) {
                        return s.substring("segment".length() + 1);
                    } else if (s.startsWith("tag")) {
                        return s.substring("tag".length() + 1);
                    }
                    return "";
                }).collect(Collectors.toList());

        List<Tuple2<Project, Tuple2<List<AllocationTime>, Tuple4<List<Map<LocalDate, Map<String, List<BillingCode>>>>,
                List<CostCodeTagAssociation>, List<CostCodeTagAssociation>, List<CostCodeTagAssociation>>>>> result =
                Flux.fromIterable(projectIds)
                        .flatMap(projectId -> {
                            AllocationTimeFilter allocationTimeFilter = new AllocationTimeFilter.AllocationTimeFilterBuilder()
                                    .startDate(startDate)
                                    .endDate(endDate)
                                    .projectId(projectId)
                                    .status(approvedOnly ? AllocationSubmissionStatus.APPROVED : null)
                                    .notStatus(AllocationSubmissionStatus.DENIED)
                                    .includes(List.of("crafts"))
                                    .build();
                            Mono<Project> projectMono = requireProject ? providerAPI.findProjectById(projectId) : Mono.just(new Project());
                            return Mono.zip(projectMono, providerAPI.getAllocationTimes(allocationTimeFilter)
                                    .defaultIfEmpty(new ArrayList<>())
                                    .flatMap(allocationTimes -> {
                                        Map<LocalDate, List<String>> dateCostCodesMap = allocationTimes.stream()
                                                .collect(Collectors.groupingBy(t -> t.getAllocationSubmission().getDateOfService(),
                                                        Collectors.flatMapping(t -> {
                                                            Set<String> costCodes = new HashSet<>();
                                                            if (t.isHasPerDiem() && StringUtils.hasText(t.getPerDiemCostCode()) && requirePerDiem) {
                                                                costCodes.add(t.getPerDiemCostCode());
                                                            }
                                                            if (t.isHasMob() && StringUtils.hasText(t.getMobCostCode()) && requireMob) {
                                                                costCodes.add(t.getMobCostCode());
                                                            }
                                                            if (!CollectionUtils.isEmpty(t.getCostCodePercs()) && requireLabor) {
                                                                costCodes.addAll(t.getCostCodePercs().stream().map(CostCodePerc::getCostCodeFull).filter(StringUtils::hasText).collect(toSet()));
                                                            }
                                                            return costCodes.stream();
                                                        }, Collectors.toList())));
                                        Set<Long> allocationTimeIds = allocationTimes.stream()
                                                .map(AllocationTime::getId)
                                                .collect(toSet());
                                        Set<Long> laborKeys = allocationTimes.stream()
                                                .map(AllocationTime::getCostCodePercs)
                                                .filter(ccp -> !CollectionUtils.isEmpty(ccp))
                                                .flatMap(Collection::stream)
                                                .map(CostCodePerc::getId)
                                                .collect(toSet());
                                        return Mono.zip(Mono.just(allocationTimes), Mono.deferContextual(Mono::just).flatMap(
                                                ctx -> {
                                                    ProviderAPI api = ctx.get(ProviderAPI.class);

                                                    Mono<List<CostCodeTagAssociation>> laborAssociationMono = requireLabor ? api.findCostCodeTagAssociationsByKeysAndType(laborKeys, CostCodeType.DEFAULT).defaultIfEmpty(new ArrayList<>()) : Mono.just(new ArrayList<>());
                                                    Mono<List<CostCodeTagAssociation>> perDiemAssociationMono = requirePerDiem ? api.findCostCodeTagAssociationsByKeysAndType(allocationTimeIds, CostCodeType.PERDIEM).defaultIfEmpty(new ArrayList<>()) : Mono.just(new ArrayList<>());
                                                    Mono<List<CostCodeTagAssociation>> mobAssociationMono = requireMob ? api.findCostCodeTagAssociationsByKeysAndType(allocationTimeIds, CostCodeType.MOB).defaultIfEmpty(new ArrayList<>()) : Mono.just(new ArrayList<>());

                                                    return Mono.zip(Flux.fromIterable(dateCostCodesMap.entrySet())
                                                                    .flatMap(entry -> {
                                                                        LocalDate dateOfService = entry.getKey();
                                                                        List<String> costCodes = entry.getValue();
                                                                        Map<String, List<BillingCode>> emptyCodeBillingCodeMap = new HashMap<>();
                                                                        Map<LocalDate, Map<String, List<BillingCode>>> emptyDateCodeBillingCodeMap = new HashMap<>();
                                                                        emptyDateCodeBillingCodeMap.put(dateOfService, emptyCodeBillingCodeMap);
                                                                        if (!requireBillingCode) return Mono.just(emptyDateCodeBillingCodeMap);
                                                                        return api.findCostCodesByProjectAndDate(projectId, dateOfService, null, true, costCodes)
                                                                                .defaultIfEmpty(new ArrayList<>())
                                                                                .flatMap(costCodeLogs -> {
                                                                                    Set<Long> costCodeIds = costCodeLogs.stream().map(CostCodeLog::getId).collect(toSet());
                                                                                    CostCodeBillingCodeFilter filter = new CostCodeBillingCodeFilter(costCodeIds, Set.of("costCodeLog"));
                                                                                    return api.findCostCodeBillingCodesByCostCodeIds(filter)
                                                                                            .defaultIfEmpty(new HashSet<>())
                                                                                            .flatMap(costCodeBillingCodes -> {
                                                                                                Set<Long> billingCodeIds = costCodeBillingCodes.stream()
                                                                                                        .map(CostCodeBillingCode::getBillingCodeId)
                                                                                                        .collect(toSet());
                                                                                                Map<String, List<BillingCode>> codeBillingCodeMap = costCodeBillingCodes.stream()
                                                                                                        .collect(Collectors.groupingBy(ccbc -> ccbc.getCostCodeLog().getCostCodeFull(), Collectors.mapping(CostCodeBillingCode::getBillingCode, Collectors.toList())));
                                                                                                return api.findBillingCodeOverridesByProjectIdAndBillingCodeIdIn(projectId, billingCodeIds)
                                                                                                        .defaultIfEmpty(new ArrayList<>())
                                                                                                        .flatMap(billingCodeOverrides -> {
                                                                                                            Map<Long, BillingCodeOverride> idOverrideMap = billingCodeOverrides.stream()
                                                                                                                    .collect(Collectors.toMap(BillingCodeOverride::getBillingCodeId, Function.identity(), (e1, e2) -> e2));
                                                                                                            codeBillingCodeMap.values()
                                                                                                                    .stream()
                                                                                                                    .flatMap(Collection::stream)
                                                                                                                    .forEach(bc -> {
                                                                                                                        if (idOverrideMap.containsKey(bc.getId())) {
                                                                                                                            BillingCodeOverride override = idOverrideMap.get(bc.getId());
                                                                                                                            bc.setClientAlias(override.getClientAlias());
                                                                                                                            bc.setDescription(override.getDescription());
                                                                                                                        }
                                                                                                                    });
                                                                                                            Map<LocalDate, Map<String, List<BillingCode>>> dateCodeBillingCodeMap = new HashMap<>();
                                                                                                            dateCodeBillingCodeMap.put(dateOfService, codeBillingCodeMap);
                                                                                                            return Mono.just(dateCodeBillingCodeMap);
                                                                                                        });

                                                                                            });
                                                                                });
                                                                    }).collectList(),
                                                            laborAssociationMono,
                                                            perDiemAssociationMono,
                                                            mobAssociationMono);
                                                }));
                                    })
                                    .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient()))));
                        })
                        .collectList()
                        .block();

        if (CollectionUtils.isEmpty(result))
            return ResponseEntity.status(HttpServletResponse.SC_NO_CONTENT).build();


        List<Project> projects = result.stream()
                .map(Tuple2::getT1)
                .sorted()
                .collect(Collectors.toList());
        Map<Long, Project> idProjectMap = projects.stream()
                .filter(p -> p.getId() != null)
                .collect(Collectors.toMap(Project::getId, Function.identity()));

        List<Map<String, Object>> data = result.stream()
                .map(tuple -> {

                    List<AllocationTime> allocationTimes = tuple.getT2().getT1();
                    List<Map<LocalDate, Map<String, List<BillingCode>>>> dateCodeBillingCodesMapList = tuple.getT2().getT2().getT1();
                    Map<LocalDate, Map<String, List<BillingCode>>> dateCodeBillingCodesMap = dateCodeBillingCodesMapList.stream()
                            .flatMap(e -> e.entrySet().stream())
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (v1, v2) -> v1));

                    List<CostCodeTagAssociation> laborAssociations = tuple.getT2().getT2().getT2();
                    List<CostCodeTagAssociation> perDiemAssociations = tuple.getT2().getT2().getT3();
                    List<CostCodeTagAssociation> mobAssociations = tuple.getT2().getT2().getT4();
                    Map<Long, List<CostCodeTagAssociation>> laborAssociationMap =
                            laborAssociations.stream()
                                    .collect(Collectors.groupingBy(CostCodeTagAssociation::getKey));
                    Map<Long, List<CostCodeTagAssociation>> perDiemAssociationMap = perDiemAssociations.stream()
                            .collect(Collectors.groupingBy(CostCodeTagAssociation::getKey));
                    Map<Long, List<CostCodeTagAssociation>> mobAssociationMap = mobAssociations.stream()
                            .collect(Collectors.groupingBy(CostCodeTagAssociation::getKey));

                    List<Map<String, Object>> rows = new ArrayList<>();
                    for (AllocationTime allocationTime : allocationTimes) {
                        LocalDate dateOfService = allocationTime.getAllocationSubmission().getDateOfService();
                        Set<CostCodePerc> costCodePercs = allocationTime.getCostCodePercs();
                        if (!CollectionUtils.isEmpty(costCodePercs) && requireLabor) {
                            Craft craft = allocationTime.getEmployee().getCraftEntity();
                            if (craft == null) craft = new Craft();
                            BigDecimal billableSt = craft.getBillableST();
                            BigDecimal billableOt = craft.getBillableOT();
                            BigDecimal billableDt = craft.getBillableDT();
                            costCodePercs.forEach(ccp -> {
                                // Labor rows
                                List<CostCodeTagAssociation> associations = laborAssociationMap.getOrDefault(ccp.getId(), new ArrayList<>());
                                Map<String, Object> row = new HashMap<>();
                                row.put("empId", allocationTime.getEmpId());
                                row.put("badgeId", Optional.ofNullable(allocationTime.getEmployee().getBadge()).orElse(""));
                                row.put("timesheet", hideDetail ? "" : Optional.ofNullable(allocationTime.getAllocationSubmission().getTeamName()).orElse("Unknown"));
                                row.put("name", allocationTime.getEmployee().getFullName());
                                row.put("craft", Optional.ofNullable(allocationTime.getEmployee().getCraft()).orElse("Unknown"));
                                row.put("craftDescription", Optional.ofNullable(Optional.ofNullable(allocationTime.getEmployee().getCraftEntity()).orElse(new Craft()).getDescription()).orElse(""));
                                row.put("dateOfService", allocationTime.getAllocationSubmission().getDateOfService().toString());
                                row.put("projectId", allocationTime.getAllocationSubmission().getProjectId());
                                row.put("stHour", ccp.getStHour());
                                row.put("otHour", ccp.getOtHour());
                                row.put("dtHour", ccp.getDtHour());
                                row.put("totalHour", ccp.getTotalHour());
                                BigDecimal stBilling = ccp.getStHour().multiply(billableSt);
                                BigDecimal otBilling = ccp.getOtHour().multiply(billableOt);
                                BigDecimal dtBilling = ccp.getDtHour().multiply(billableDt);
                                row.put("stBilling", stBilling);
                                row.put("otBilling", otBilling);
                                row.put("dtBilling", dtBilling);
                                row.put("pdBilling", BigDecimal.ZERO);
                                row.put("mobBilling", BigDecimal.ZERO);
                                row.put("totalBilling", stBilling.add(otBilling).add(dtBilling));
                                for (CostCodeTagAssociation association : associations) {
                                    row.put(association.getTagType().trim(), String.format("%s  %s", association.getCodeName(),
                                            StringUtils.hasText(association.getCodeDescription()) ? association.getCodeDescription() : ""));
                                }
                                String costCode = ccp.getCostCodeFull();
                                List<BillingCode> billingCodes = dateCodeBillingCodesMap.getOrDefault(dateOfService, new HashMap<>())
                                        .getOrDefault(costCode, new ArrayList<>());
                                for (BillingCode billingCode : billingCodes) {
                                    BillingCodeType billingCodeType = billingCode.getBillingCodeType();
                                    String key = billingCodeType.getBillingCodeTypeParsedBy() == BillingCodeTypeParsedBy.CODE_NAME ? billingCode.getCodeName() : billingCode.getClientAlias();
                                    row.put(billingCode.getBillingCodeType().getCodeType().trim(),
                                            String.format("%s  %s", key,
                                                    StringUtils.hasText(billingCode.getDescription()) ? billingCode.getDescription() : ""));
                                }
                                rows.add(row);
                            });
                        }
                        if (allocationTime.isHasPerDiem() && requirePerDiem) {
                            List<CostCodeTagAssociation> associations = perDiemAssociationMap.getOrDefault(allocationTime.getId(), new ArrayList<>());
                            Map<String, Object> row = new HashMap<>();
                            row.put("empId", allocationTime.getEmpId());
                            row.put("badgeId", Optional.ofNullable(allocationTime.getEmployee().getBadge()).orElse(""));
                            row.put("name", allocationTime.getEmployee().getFullName());
                            row.put("timesheet", hideDetail ? "" : Optional.ofNullable(allocationTime.getAllocationSubmission().getTeamName()).orElse("Unknown"));
                            row.put("craft", Optional.ofNullable(allocationTime.getEmployee().getCraft()).orElse("Unknown"));
                            row.put("craftDescription", Optional.ofNullable(Optional.ofNullable(allocationTime.getEmployee().getCraftEntity()).orElse(new Craft()).getDescription()).orElse(""));
                            row.put("dateOfService", allocationTime.getAllocationSubmission().getDateOfService().toString());
                            row.put("projectId", allocationTime.getAllocationSubmission().getProjectId());
                            row.put("stHour", BigDecimal.ZERO);
                            row.put("otHour", BigDecimal.ZERO);
                            row.put("dtHour", BigDecimal.ZERO);
                            row.put("totalHour", BigDecimal.ZERO);
                            row.put("stBilling", BigDecimal.ZERO);
                            row.put("otBilling", BigDecimal.ZERO);
                            row.put("dtBilling", BigDecimal.ZERO);
                            row.put("pdBilling", allocationTime.getPerDiemAmount());
                            row.put("mobBilling", BigDecimal.ZERO);
                            row.put("totalBilling", allocationTime.getPerDiemAmount());
                            for (CostCodeTagAssociation association : associations) {
                                row.put(association.getTagType().trim(), String.format("%s  %s", association.getCodeName(),
                                        StringUtils.hasText(association.getCodeDescription()) ? association.getCodeDescription() : ""));
                            }
                            String costCode = allocationTime.getPerDiemCostCode();
                            List<BillingCode> billingCodes = dateCodeBillingCodesMap.getOrDefault(dateOfService, new HashMap<>())
                                    .getOrDefault(costCode, new ArrayList<>());
                            for (BillingCode billingCode : billingCodes) {
                                BillingCodeType billingCodeType = billingCode.getBillingCodeType();
                                String key = billingCodeType.getBillingCodeTypeParsedBy() == BillingCodeTypeParsedBy.CODE_NAME ? billingCode.getCodeName() : billingCode.getClientAlias();
                                row.put(billingCodeType.getCodeType().trim(),
                                        String.format("%s  %s", key,
                                                StringUtils.hasText(billingCode.getDescription()) ? billingCode.getDescription() : ""));
                            }
                            rows.add(row);
                        }
                        if (allocationTime.isHasMob() && requireMob) {
                            List<CostCodeTagAssociation> associations = mobAssociationMap.getOrDefault(allocationTime.getId(), new ArrayList<>());
                            Map<String, Object> row = new HashMap<>();
                            row.put("empId", allocationTime.getEmpId());
                            row.put("badgeId", Optional.ofNullable(allocationTime.getEmployee().getBadge()).orElse(""));
                            row.put("name", allocationTime.getEmployee().getFullName());
                            row.put("timesheet", hideDetail ? "" : Optional.ofNullable(allocationTime.getAllocationSubmission().getTeamName()).orElse("Unknown"));
                            row.put("craft", Optional.ofNullable(allocationTime.getEmployee().getCraft()).orElse("Unknown"));
                            row.put("craftDescription", Optional.ofNullable(Optional.ofNullable(allocationTime.getEmployee().getCraftEntity()).orElse(new Craft()).getDescription()).orElse(""));
                            row.put("dateOfService", allocationTime.getAllocationSubmission().getDateOfService().toString());
                            row.put("projectId", allocationTime.getAllocationSubmission().getProjectId());
                            row.put("stHour", BigDecimal.ZERO);
                            row.put("otHour", BigDecimal.ZERO);
                            row.put("dtHour", BigDecimal.ZERO);
                            row.put("totalHour", BigDecimal.ZERO);
                            row.put("stBilling", BigDecimal.ZERO);
                            row.put("otBilling", BigDecimal.ZERO);
                            row.put("dtBilling", BigDecimal.ZERO);
                            row.put("pdBilling", BigDecimal.ZERO);
                            row.put("mobBilling", allocationTime.getMobAmount());
                            row.put("totalBilling", allocationTime.getMobAmount());
                            for (CostCodeTagAssociation association : associations) {
                                row.put(association.getTagType().trim(), String.format("%s  %s", association.getCodeName(),
                                        StringUtils.hasText(association.getCodeDescription()) ? association.getCodeDescription() : ""));
                            }
                            String costCode = allocationTime.getMobCostCode();
                            List<BillingCode> billingCodes = dateCodeBillingCodesMap.getOrDefault(dateOfService, new HashMap<>())
                                    .getOrDefault(costCode, new ArrayList<>());
                            for (BillingCode billingCode : billingCodes) {
                                BillingCodeType billingCodeType = billingCode.getBillingCodeType();
                                String key = billingCodeType.getBillingCodeTypeParsedBy() == BillingCodeTypeParsedBy.CODE_NAME ? billingCode.getCodeName() : billingCode.getClientAlias();
                                row.put(billingCode.getBillingCodeType().getCodeType().trim(),
                                        String.format("%s  %s", key,
                                                StringUtils.hasText(billingCode.getDescription()) ? billingCode.getDescription() : ""));
                            }
                            rows.add(row);
                        }
                    }
                    return rows;
                })
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        Map<List<String>, List<Map<String, Object>>> groupedDate = data.stream()
                .collect(Collectors.groupingBy(map -> finalGroupingBy.stream().map(g -> String.format("%s %s", g, map.getOrDefault(g, "")))
                        .collect(Collectors.toList())));

        groupedDate.entrySet().forEach(entry -> {
            List<Map<String, Object>> mergedList = new ArrayList<>(entry.getValue().stream()
                    .collect(Collectors.toMap(groupKeyMapper, Function.identity(), this::mergeMaps)).values());
            entry.setValue(mergedList);
        });

        PDDocument document = new PDDocument();

        PDRectangle pageRectangle = type == ReportHourAndBillingType.PER_DIEM_EXPENSE || type == ReportHourAndBillingType.TRAVEL_PAY_SUMMARY
                ? PDRectangle.LETTER : new PDRectangle(PDRectangle.LETTER.getHeight(), PDRectangle.LETTER.getWidth());
        PDPage page = new PDPage(pageRectangle);
        document.addPage(page);

        float height = page.getMediaBox().getHeight();
        float width = page.getMediaBox().getWidth();
        float topMargin = 10f;
        float bottomMargin = 50f;
        float leftMargin = 36f;
        float rightMargin = 16f;

        // starting y position is whole page height subtracted by top and bottom margin
        float yStartNewPage = height - (topMargin + bottomMargin);
        // we want table across whole page width (subtracted by left and right margin of course)
        float tableWidth = width - (rightMargin + leftMargin);

        // y position is your coordinate of top left corner of the table
        float yPosition = height - topMargin;

        // Add header
        try {
            final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

            PDPageContentStream contentStream = new PDPageContentStream(document, page,
                    PDPageContentStream.AppendMode.APPEND, true);

            String criteria = CollectionUtils.isEmpty(finalGroupingBy) ? "All" : String.join(", ", finalGroupingBy);
            // Draw header
            if (type == ReportHourAndBillingType.EMPLOYEE_HOURS_BILLING) {

                AWSCredentials credentials = new BasicAWSCredentials(s3_access_key, s3_secret_key);
                AmazonS3 s3client = AmazonS3ClientBuilder
                        .standard()
                        .withCredentials(new AWSStaticCredentialsProvider(credentials))
                        .withRegion(Regions.US_EAST_2)
                        .build();
                String s3LogoKey = tenantProperties.getTenants().getOrDefault(ThreadTenantStorage.getTenantId(), new TenantDto()).getS3Logo();

                Image image = null;
                if (StringUtils.hasText(s3LogoKey)) {
                    try {
                        image = new Image(ImageIO.read(s3client.getObject(buck_name, s3LogoKey).getObjectContent()));
                        float imageWidth = 150f;
                        image = image.scaleByWidth(imageWidth);
                    } catch (Exception ignore) {
                    }
                }

                BaseTable table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, leftMargin,
                        document, page, false, true);

                // set default line spacing for entire table
                table.setLineSpacing(1f);

                Row<PDPage> row = table.createRow(54f);

                generateCells(row, new int[]{36, 610, 756},
                        new CellAttributes[]{
                                new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE)
                                        .value(headerTitle)
                                        .textColor(hex2Rgb("#c55910"))
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .fontSize(25f)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.MIDDLE)
                                        .image(image)
                                        .build(),
                        });

                yPosition = table.draw();

                yPosition -= 10;

                table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, leftMargin,
                        document, page, true, true);

                row = table.createRow(20f);

                LineStyle greyLine = new LineStyle(hex2Rgb("#767070"), 0);
                generateCells(row, new int[]{36, 64, 144, 450, 525, 608, 670, 756},
                        new CellAttributes[]{
                                new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE)
                                        .value("")
                                        .fillColor(hex2Rgb("#767070"))
                                        .textColor(Color.white)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .borderStyle(greyLine)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.MIDDLE)
                                        .value("Report Criteria:")
                                        .fillColor(hex2Rgb("#767070"))
                                        .textColor(Color.white)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .borderStyle(greyLine)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE)
                                        .value(criteria)
                                        .fillColor(hex2Rgb("#767070"))
                                        .textColor(Color.white)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .borderStyle(greyLine)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.MIDDLE)
                                        .value("From Date:")
                                        .fillColor(hex2Rgb("#767070"))
                                        .textColor(Color.white)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .borderStyle(greyLine)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE)
                                        .value(String.format("%s [%s]", dateFormatter.format(startDate), LocalDateUtil.getDayOfWeekName(startDate)))
                                        .fillColor(hex2Rgb("#767070"))
                                        .textColor(Color.white)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .borderStyle(greyLine)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.MIDDLE)
                                        .value("To Date:")
                                        .fillColor(hex2Rgb("#767070"))
                                        .textColor(Color.white)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .borderStyle(greyLine)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE)
                                        .value(String.format("%s [%s]", dateFormatter.format(endDate), LocalDateUtil.getDayOfWeekName(endDate)))
                                        .fillColor(hex2Rgb("#767070"))
                                        .textColor(Color.white)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .borderStyle(greyLine)
                                        .build(),
                        });

                yPosition = table.draw();

            } else {
                String jobSubJobs = projects.stream()
                        .map(Project::getJobSubJob)
                        .map(s -> String.format("<p>%s</p>", s))
                        .collect(Collectors.joining(""));

                yPosition = drawHeader1(yPosition, tableWidth, leftMargin, document, page, criteria, headerTitle,
                        String.format("%s [%s]", dateFormatter.format(startDate), LocalDateUtil.getDayOfWeekName(startDate)),
                        String.format("%s [%s]", dateFormatter.format(endDate), LocalDateUtil.getDayOfWeekName(endDate)), shift, jobSubJobs);

            }

            yPosition -= 6f;

            BaseTable table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, leftMargin,
                    document, page, true, true);
            table.setLineSpacing(1.5f);

            Row<PDPage> row = table.createRow(20f);

            switch (type) {
                case EMPLOYEE_HOURS_BILLING -> drawEmployeeHoursAndBillingHeaderRow(row);
                case EMPLOYEE_HOURS -> drawEmployeeHoursHeaderRow(row);
                case CLASSIFICATION_CODE -> drawClassificationCodeHeaderRow(row);
                case PER_DIEM_EXPENSE, TRAVEL_PAY_SUMMARY -> drawPerDiemExpenseHeaderRow(row);
                case COST_BY_TOTALS -> drawCostByTotalsHeaderRow(row);
                default -> throw new IllegalArgumentException("Not implemented");
            }

            table.addHeaderRow(row);

            int rowBgIndex = 0;
            Color bgColor;

            Map<String, Object> totals = new HashMap<>();
            Map<String, Map<String, Object>> categoryMap = new HashMap<>();

            if (CollectionUtils.isEmpty(groupingBy)) {
                Map<String, Object> result1 = drawCategory(groupedDate, 0, table, rowBgIndex, categoryMap, type, idProjectMap);
                mergeMaps(totals, result1);
            } else {
                TreeSet<String> firstGroupCategorySet = groupedDate.keySet().stream()
                        .map(e -> e.get(0))
                        .collect(Collectors.toCollection(TreeSet::new));

                final int level = 0;
                for (String firstCategory : firstGroupCategorySet) {
                    Map<List<String>, List<Map<String, Object>>> filteredDate = groupedDate.entrySet().stream()
                            .filter(map -> {
                                List<String> key = map.getKey();
                                return key.get(level).equals(firstCategory);
                            })
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

                    Map<String, Object> result1 = drawCategory(filteredDate, level, table, rowBgIndex, categoryMap, type, idProjectMap);
                    mergeMaps(totals, result1);
                    rowBgIndex = (int) result1.get("rowBgIndex");
                }
            }

            BigDecimal stHour = (BigDecimal) totals.get("stHour");
            BigDecimal otHour = (BigDecimal) totals.get("otHour");
            BigDecimal dtHour = (BigDecimal) totals.get("dtHour");
            BigDecimal totalHour = (BigDecimal) totals.get("totalHour");
            BigDecimal stBilling = (BigDecimal) totals.get("stBilling");
            BigDecimal otBilling = (BigDecimal) totals.get("otBilling");
            BigDecimal dtBilling = (BigDecimal) totals.get("dtBilling");
            BigDecimal pdBilling = (BigDecimal) totals.get("pdBilling");
            BigDecimal mobBilling = (BigDecimal) totals.get("mobBilling");
            BigDecimal totalBilling = (BigDecimal) totals.get("totalBilling");

            row = table.createRow(20f);
            bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;
            generateCells(row, new int[]{36, 756},
                    new CellAttributes[]{
                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM).fillColor(bgColor).build(),
                    });

            row = table.createRow(20f);
            bgColor = rowBgIndex % 2 == 0 ? Color.lightGray : Color.white;

            switch (type) {
                case EMPLOYEE_HOURS_BILLING -> generateCells(row, new int[]{36, 248, 292, 342, 392, 442, 494, 546, 598, 650, 703, 756},
                        new CellAttributes[]{
                                new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                        .value("Totals")
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .fontSize(15f)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format(stHour))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format(otHour))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format(dtHour))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format(totalHour))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat(stBilling))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat(otBilling))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat(dtBilling))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat(pdBilling))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat(mobBilling))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat(totalBilling))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                        });
                case EMPLOYEE_HOURS -> generateCells(row, new int[]{36, 522, 572, 638, 690, 752},
                        new CellAttributes[]{
                                new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                        .value("Totals For Report Data/Shift Range")
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format(stHour))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format(otHour))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format(dtHour))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format(totalHour))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                        });
                case CLASSIFICATION_CODE -> generateCells(row, new int[]{36, 212, 288, 342, 396, 458, 546, 636, 752},
                        new CellAttributes[]{
                                new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                        .value("Totals")
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                        .fillColor(bgColor)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format(stHour))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format(otHour))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format(totalHour))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat(stBilling))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat(otBilling))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat(totalBilling))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                        });
                case PER_DIEM_EXPENSE -> {
                    long employeeSize = result.stream()
                            .map(tuple -> tuple.getT2().getT1())
                            .flatMap(Collection::stream)
                            .filter(AllocationTime::isHasPerDiem)
                            .map(AllocationTime::getEmpId)
                            .distinct()
                            .count();

                    generateCells(row, new int[]{20, 250, 480, 528, 590},
                            new CellAttributes[]{
                                    new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                            .value(String.format("%s Employees included in this report", employeeSize))
                                            .fillColor(bgColor)
                                            .font(PDType1Font.HELVETICA_BOLD)
                                            .build(),
                                    new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM)
                                            .value("Total Per Diem for this report")
                                            .fillColor(bgColor)
                                            .font(PDType1Font.HELVETICA_BOLD)
                                            .textColor(Color.black)
                                            .build(),
                                    new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                            .fillColor(bgColor)
                                            .build(),
                                    new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                            .value(NumberUtil.currencyFormat(pdBilling))
                                            .fillColor(bgColor)
                                            .textColor(Color.black)
                                            .build(),
                            });
                }
                case COST_BY_TOTALS -> generateCells(row, new int[]{36, 258, 317, 376, 443, 534, 631, 756},
                        new CellAttributes[]{
                                new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                        .value("Totals")
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .fontSize(15f)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format(stHour))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format(otHour))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format(totalHour))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat(stBilling))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat(otBilling))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat(totalBilling))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                        });
                case TRAVEL_PAY_SUMMARY -> {
                    long employeeSize = result.stream()
                            .map(tuple -> tuple.getT2().getT1())
                            .flatMap(Collection::stream)
                            .filter(AllocationTime::isHasMob)
                            .map(AllocationTime::getEmpId)
                            .distinct()
                            .count();

                    generateCells(row, new int[]{20, 250, 480, 528, 590},
                            new CellAttributes[]{
                                    new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                            .value(String.format("%s Employees included in this report", employeeSize))
                                            .fillColor(bgColor)
                                            .font(PDType1Font.HELVETICA_BOLD)
                                            .build(),
                                    new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM)
                                            .value("Total Travel Pay for this report")
                                            .fillColor(bgColor)
                                            .font(PDType1Font.HELVETICA_BOLD)
                                            .textColor(Color.black)
                                            .build(),
                                    new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                            .fillColor(bgColor)
                                            .build(),
                                    new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                            .value(NumberUtil.currencyFormat(mobBilling))
                                            .fillColor(bgColor)
                                            .textColor(Color.black)
                                            .build(),
                            });
                }
                default -> throw new IllegalArgumentException("Not implemented");
            }


            yPosition = table.draw();

            yPosition -= 25;

            if (table.getCurrentPage() != page) {
                contentStream.close();
                page = table.getCurrentPage();
                contentStream = new PDPageContentStream(document, page,
                        PDPageContentStream.AppendMode.APPEND, true);
            }

            addSignature(yPosition, yStartNewPage, tableWidth, leftMargin, document, page);

            contentStream.close();

            addFooter(document);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            document.save(outputStream);
            document.close();
            InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
            headers.add("Content-Type", "application/force-download");
            headers.add("Content-Disposition", String.format("%s%s", "attachment; filename=", filename));
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_PDF)
                    .body(new InputStreamResource(inputStream));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }

    private Map<String, Object> mergeMaps(Map<String, Object> v1, Map<String, Object> v2) {
        BigDecimal stHour1 = (BigDecimal) v1.getOrDefault("stHour", BigDecimal.ZERO);
        BigDecimal otHour1 = (BigDecimal) v1.getOrDefault("otHour", BigDecimal.ZERO);
        BigDecimal dtHour1 = (BigDecimal) v1.getOrDefault("dtHour", BigDecimal.ZERO);
        BigDecimal totalHour1 = (BigDecimal) v1.getOrDefault("totalHour", BigDecimal.ZERO);
        BigDecimal stBilling1 = (BigDecimal) v1.getOrDefault("stBilling", BigDecimal.ZERO);
        BigDecimal otBilling1 = (BigDecimal) v1.getOrDefault("otBilling", BigDecimal.ZERO);
        BigDecimal dtBilling1 = (BigDecimal) v1.getOrDefault("dtBilling", BigDecimal.ZERO);
        BigDecimal totalBilling1 = (BigDecimal) v1.getOrDefault("totalBilling", BigDecimal.ZERO);
        BigDecimal pdBilling1 = (BigDecimal) v1.getOrDefault("pdBilling", BigDecimal.ZERO);
        BigDecimal mobBilling1 = (BigDecimal) v1.getOrDefault("mobBilling", BigDecimal.ZERO);
        BigDecimal stHour2 = (BigDecimal) v2.getOrDefault("stHour", BigDecimal.ZERO);
        BigDecimal otHour2 = (BigDecimal) v2.getOrDefault("otHour", BigDecimal.ZERO);
        BigDecimal dtHour2 = (BigDecimal) v2.getOrDefault("dtHour", BigDecimal.ZERO);
        BigDecimal totalHour2 = (BigDecimal) v2.getOrDefault("totalHour", BigDecimal.ZERO);
        BigDecimal stBilling2 = (BigDecimal) v2.getOrDefault("stBilling", BigDecimal.ZERO);
        BigDecimal otBilling2 = (BigDecimal) v2.getOrDefault("otBilling", BigDecimal.ZERO);
        BigDecimal dtBilling2 = (BigDecimal) v2.getOrDefault("dtBilling", BigDecimal.ZERO);
        BigDecimal totalBilling2 = (BigDecimal) v2.getOrDefault("totalBilling", BigDecimal.ZERO);
        BigDecimal pdBilling2 = (BigDecimal) v2.getOrDefault("pdBilling", BigDecimal.ZERO);
        BigDecimal mobBilling2 = (BigDecimal) v2.getOrDefault("mobBilling", BigDecimal.ZERO);
        v1.put("stHour", stHour1.add(stHour2));
        v1.put("otHour", otHour1.add(otHour2));
        v1.put("dtHour", dtHour1.add(dtHour2));
        v1.put("totalHour", totalHour1.add(totalHour2));
        v1.put("stBilling", stBilling1.add(stBilling2));
        v1.put("otBilling", otBilling1.add(otBilling2));
        v1.put("dtBilling", dtBilling1.add(dtBilling2));
        v1.put("pdBilling", pdBilling1.add(pdBilling2));
        v1.put("mobBilling", mobBilling1.add(mobBilling2));
        v1.put("totalBilling", totalBilling1.add(totalBilling2));
        return v1;
    }

    private Map<String, Object> drawCategory(Map<List<String>, List<Map<String, Object>>> filteredDate, int level,
                                             BaseTable table, int rowBgIndex,
                                             Map<String, Map<String, Object>> categoryMap, ReportHourAndBillingType type,
                                             Map<Long, Project> idProjectMap) {

        List<String> groupKey = filteredDate.keySet().stream().findFirst().orElse(new ArrayList<>());
        String category = groupKey.size() > level ? groupKey.get(level) : "";
        String categoryHierarchy = IntStream.range(0, level + 1)
                .mapToObj(i -> groupKey.size() > i ? groupKey.get(i) : " ")
                .collect(Collectors.joining(" "));
        Map<String, Object> result;
        if (StringUtils.hasText(category)) {
            categoryMap.putIfAbsent(categoryHierarchy, new HashMap<>());

            Row<PDPage> row = table.createRow(20f);
            Color bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;

            generateCells(row, new int[]{36, 756},
                    new CellAttributes[]{
                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                    .value(category)
                                    .fillColor(bgColor)
                                    .textColor(Color.black)
                                    .font(PDType1Font.HELVETICA_BOLD)
                                    .fontSize(12f)
                                    .build(),
                    });

            if (level == groupKey.size() - 1) {
                result = drawRows(filteredDate.values().stream().flatMap(Collection::stream).collect(Collectors.toList()), table, rowBgIndex, type, idProjectMap);
                rowBgIndex = (int) result.get("rowBgIndex");
            } else {
                level++;
                final int finalLevel = level;
                TreeSet<String> nextGroupCategorySet = filteredDate.keySet().stream()
                        .map(e -> e.get(finalLevel))
                        .collect(Collectors.toCollection(TreeSet::new));
                Map<String, Object> subResult;
                result = new HashMap<>();
                for (String nextCategory : nextGroupCategorySet) {
                    Map<List<String>, List<Map<String, Object>>> filteredDate1 = filteredDate.entrySet().stream()
                            .filter(map -> {
                                List<String> key = map.getKey();
                                return key.get(finalLevel).equals(nextCategory);
                            })
                            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
                    subResult = drawCategory(filteredDate1, level, table, rowBgIndex, categoryMap, type, idProjectMap);
                    mergeMaps(result, subResult);
                }
            }

            categoryMap.put(categoryHierarchy, mergeMaps(categoryMap.get(categoryHierarchy), result));
            Map<String, Object> categorySummary = categoryMap.get(categoryHierarchy);

            long spaceSize = category.chars().filter(c -> c == ' ').count();
            String categoryWithoutDesc = spaceSize > 1 ? category.substring(0, category.indexOf(" ", category.indexOf(" ") + 1)) : category.substring(0, category.indexOf(" "));
            row = table.createRow(20f);
            bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;

            switch (type) {
                case EMPLOYEE_HOURS_BILLING -> generateCells(row, new int[]{36, 248, 292, 342, 392, 442, 494, 546, 598, 650, 703, 756},
                        new CellAttributes[]{
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(categoryWithoutDesc)
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .fontSize(12f)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format((BigDecimal) categorySummary.get("stHour")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format((BigDecimal) categorySummary.get("otHour")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format((BigDecimal) categorySummary.get("dtHour")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format((BigDecimal) categorySummary.get(("totalHour"))))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat((BigDecimal) categorySummary.get("stBilling")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat((BigDecimal) categorySummary.get("otBilling")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat((BigDecimal) categorySummary.get("dtBilling")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat((BigDecimal) categorySummary.get("pdBilling")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat((BigDecimal) categorySummary.get("mobBilling")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat((BigDecimal) categorySummary.get("totalBilling")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                        });
                case EMPLOYEE_HOURS -> generateCells(row, new int[]{36, 522, 572, 638, 690, 752},
                        new CellAttributes[]{
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(categoryWithoutDesc)
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .fontSize(12f)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format((BigDecimal) categorySummary.get("stHour")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format((BigDecimal) categorySummary.get("otHour")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format((BigDecimal) categorySummary.get("dtHour")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format((BigDecimal) categorySummary.get(("totalHour"))))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                        });
                case CLASSIFICATION_CODE -> generateCells(row, new int[]{36, 288, 342, 396, 458, 546, 636, 752},
                        new CellAttributes[]{
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(categoryWithoutDesc)
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .fontSize(12f)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format((BigDecimal) categorySummary.get("stHour")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format((BigDecimal) categorySummary.get("otHour")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format((BigDecimal) categorySummary.get(("totalHour"))))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat((BigDecimal) categorySummary.get("stBilling")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat((BigDecimal) categorySummary.get("otBilling")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat((BigDecimal) categorySummary.get("totalBilling")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                        });
                case PER_DIEM_EXPENSE -> generateCells(row, new int[]{20, 480, 590},
                        new CellAttributes[]{
                                new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                        .value(categoryWithoutDesc)
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .fontSize(12f)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat((BigDecimal) categorySummary.get("pdBilling")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                        });
                case COST_BY_TOTALS -> generateCells(row, new int[]{36, 258, 317, 376, 443, 534, 631, 756},
                        new CellAttributes[]{
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(categoryWithoutDesc)
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .fontSize(12f)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format((BigDecimal) categorySummary.get("stHour")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format((BigDecimal) categorySummary.get("otHour")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.format((BigDecimal) categorySummary.get(("totalHour"))))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat((BigDecimal) categorySummary.get("stBilling")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat((BigDecimal) categorySummary.get("otBilling")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat((BigDecimal) categorySummary.get("totalBilling")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                        });
                case TRAVEL_PAY_SUMMARY -> generateCells(row, new int[]{20, 480, 590},
                        new CellAttributes[]{
                                new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                        .value(categoryWithoutDesc)
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .font(PDType1Font.HELVETICA_BOLD)
                                        .fontSize(12f)
                                        .build(),
                                new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                        .value(NumberUtil.currencyFormat((BigDecimal) categorySummary.get("mobBilling")))
                                        .fillColor(bgColor)
                                        .textColor(Color.black)
                                        .build(),
                        });
            }
            result.put("rowBgIndex", rowBgIndex);
        } else {
            result = drawRows(filteredDate.values().stream().flatMap(Collection::stream).collect(Collectors.toList()), table, rowBgIndex, type, idProjectMap);
        }
        return result;
    }

    private Map<String, Object> drawRows(List<Map<String, Object>> values, BaseTable table, int rowBgIndex, ReportHourAndBillingType type, Map<Long, Project> idProjectMap) {

        BigDecimal categoryStHour = BigDecimal.ZERO;
        BigDecimal categoryOtHour = BigDecimal.ZERO;
        BigDecimal categoryDtHour = BigDecimal.ZERO;
        BigDecimal categoryTotalHour = BigDecimal.ZERO;
        BigDecimal categoryStBilling = BigDecimal.ZERO;
        BigDecimal categoryOtBilling = BigDecimal.ZERO;
        BigDecimal categoryDtBilling = BigDecimal.ZERO;
        BigDecimal categoryTotalBilling = BigDecimal.ZERO;
        BigDecimal categoryPdBilling = BigDecimal.ZERO;
        BigDecimal categoryMobBilling = BigDecimal.ZERO;

        final Function<? super Map<String, Object>, ? extends String> groupKeyMapper = switch (type) {
            case EMPLOYEE_HOURS_BILLING -> (e) -> (String) e.get("empId");
            case EMPLOYEE_HOURS, PER_DIEM_EXPENSE, TRAVEL_PAY_SUMMARY -> (e) -> String.format("%s %s", e.get("empId"), e.get("craft")).toLowerCase();
            case CLASSIFICATION_CODE -> (e) -> String.format("%s %s", e.get("craft"), e.get("timesheet")).toLowerCase();
            case COST_BY_TOTALS -> (e) -> ((String) e.get("timesheet")).toLowerCase();
        };

        final Function<? super Map<String, Object>, ? extends String> sortKeyInGroupMapper = switch (type) {
            case EMPLOYEE_HOURS_BILLING -> (e) -> (String) e.get("empId");
            case EMPLOYEE_HOURS, PER_DIEM_EXPENSE, TRAVEL_PAY_SUMMARY -> (e) -> String.format("%s %s %s", e.get("name"), e.get("craft"), e.get("dateOfService")).toLowerCase();
            case CLASSIFICATION_CODE -> (e) -> String.format("%s %s", e.get("craft"), e.get("timesheet")).toLowerCase();
            case COST_BY_TOTALS -> (e) -> ((String) e.get("timesheet")).toLowerCase();
        };

        Map<String, List<Map<String, Object>>> groupedValues = values.stream()
                .collect(Collectors.groupingBy(groupKeyMapper,
                        Collectors.mapping(Function.identity(),
                                Collectors.collectingAndThen(Collectors.toList(),
                                        e -> e.stream()
                                                .sorted(Comparator.comparing(sortKeyInGroupMapper, Comparator.nullsLast(Comparator.naturalOrder())))
                                                .collect(Collectors.toList())))));

        final Function<? super Map.Entry<String, List<Map<String, Object>>>, ? extends String> sortKeyMapper = switch (type) {
            case EMPLOYEE_HOURS_BILLING -> (e) -> ((String) e.getValue().stream()
                    .findFirst()
                    .orElse(new HashMap<>())
                    .getOrDefault("name", "")).toLowerCase();
            case EMPLOYEE_HOURS, PER_DIEM_EXPENSE, TRAVEL_PAY_SUMMARY -> (e) -> {
                Map<String, Object> map = e.getValue().stream()
                                .reduce((first, second) -> second)
                                .orElse(new HashMap<>());
                return String.format("%s %s %s", map.get("name"), map.get("craft"), map.get("dateOfService")).toLowerCase();
            };
            case CLASSIFICATION_CODE -> (e) -> {
                Map<String, Object> map = e.getValue().stream()
                        .reduce((first, second) -> second)
                        .orElse(new HashMap<>());
                return String.format("%s %s", map.get("craft"), map.get("timesheet")).toLowerCase();
            };
            case COST_BY_TOTALS -> (e) -> {
                Map<String, Object> map = e.getValue().stream()
                        .reduce((first, second) -> second)
                        .orElse(new HashMap<>());
                return  ((String) map.get("timesheet")).toLowerCase();
            };
        };

        groupedValues = groupedValues.entrySet().stream()
                .sorted(Comparator.comparing(sortKeyMapper, Comparator.nullsLast(Comparator.naturalOrder())))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        BigDecimal employeeStHour = BigDecimal.ZERO;
        BigDecimal employeeOtHour = BigDecimal.ZERO;
        BigDecimal employeeDtHour = BigDecimal.ZERO;
        BigDecimal employeeTotalHour = BigDecimal.ZERO;
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("MM/dd/yyyy");

        BigDecimal employeePerDiem = BigDecimal.ZERO;
        BigDecimal employeeMob = BigDecimal.ZERO;
        int perDiemDays = 0;
        int mobDays = 0;
        for (Map.Entry<String, List<Map<String, Object>>> entry : groupedValues.entrySet()) {
            List<Map<String, Object>> sortedValues = entry.getValue();
            for (int i = 0; i < sortedValues.size(); i++) {
                Map<String, Object> value = sortedValues.get(i);
                String empId = (String) value.get("empId");
                String badgeId = (String) value.get("badgeId");
                String name = (String) value.get("name");
                String craft = (String) value.get("craft");
                String craftDescription = (String) value.get("craftDescription");
                String timesheet = (String) value.get("timesheet");
                String dateOfServiceStr = (String) value.get("dateOfService");
                BigDecimal stHour = (BigDecimal) value.get("stHour");
                BigDecimal otHour = (BigDecimal) value.get("otHour");
                BigDecimal dtHour = (BigDecimal) value.get("dtHour");
                BigDecimal totalHour = (BigDecimal) value.get("totalHour");
                BigDecimal stBilling = (BigDecimal) value.get("stBilling");
                BigDecimal otBilling = (BigDecimal) value.get("otBilling");
                BigDecimal dtBilling = (BigDecimal) value.get("dtBilling");
                BigDecimal pdBilling = (BigDecimal) value.get("pdBilling");
                BigDecimal mobBilling = (BigDecimal) value.get("mobBilling");
                BigDecimal totalBilling = (BigDecimal) value.get("totalBilling");

                categoryStHour = categoryStHour.add(stHour);
                categoryOtHour = categoryOtHour.add(otHour);
                categoryDtHour = categoryDtHour.add(dtHour);
                categoryTotalHour = categoryTotalHour.add(totalHour);
                categoryStBilling = categoryStBilling.add(stBilling);
                categoryOtBilling = categoryOtBilling.add(otBilling);
                categoryDtBilling = categoryDtBilling.add(dtBilling);
                categoryTotalBilling = categoryTotalBilling.add(totalBilling);
                categoryPdBilling = categoryPdBilling.add(pdBilling);
                categoryMobBilling = categoryMobBilling.add(mobBilling);

                switch (type) {
                    case EMPLOYEE_HOURS_BILLING -> {
                        Row<PDPage> row = table.createRow(20f);
                        Color bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;
                        generateCells(row, new int[]{36, 90, 186, 248, 292, 342, 392, 442, 494, 546, 598, 650, 703, 756},
                                new CellAttributes[]{
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(empId)
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(name)
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(craft)
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.format(stHour))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.format(otHour))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.format(dtHour))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.format(totalHour))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(stBilling))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(otBilling))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(dtBilling))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(pdBilling))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(mobBilling))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(totalBilling))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                });
                    }
                    case EMPLOYEE_HOURS -> {
                        Row<PDPage> row;
                        Color bgColor;
                        Long projectId = (Long) value.get("projectId");
                        Project project = idProjectMap.getOrDefault(projectId, new Project());
                        employeeStHour = employeeStHour.add(stHour);
                        employeeOtHour = employeeOtHour.add(otHour);
                        employeeDtHour = employeeDtHour.add(dtHour);
                        employeeTotalHour = employeeTotalHour.add(totalHour);

                        row = table.createRow(20f);
                        bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;
                        generateCells(row, new int[]{36, 90, 170, 296, 380, 438, 522, 572, 638, 690, 752},
                                new CellAttributes[]{
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(i == 0 ? empId : "")
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(i == 0 ? badgeId : "")
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(i == 0 ? String.format("%s   %s", name, craft) : "")
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(project.getJobSubJob())
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(formatter2.format(LocalDate.parse(dateOfServiceStr, formatter1)))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(Optional.ofNullable(timesheet).orElse(""))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.format(stHour))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.format(otHour))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.format(dtHour))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.format(totalHour))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                });

                        if (i == sortedValues.size() - 1) {
                            row = table.createRow(20f);
                            bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;
                            generateCells(row, new int[]{36, 90, 170, 296, 522, 572, 638, 690, 752},
                                    new CellAttributes[]{
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM)
                                                    .value(String.format("Totals for %s %s %s", empId, name, craft))
                                                    .font(PDType1Font.HELVETICA_BOLD)
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                    .value(NumberUtil.format(employeeStHour))
                                                    .font(PDType1Font.HELVETICA_BOLD)
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                    .value(NumberUtil.format(employeeOtHour))
                                                    .font(PDType1Font.HELVETICA_BOLD)
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                    .value(NumberUtil.format(employeeDtHour))
                                                    .font(PDType1Font.HELVETICA_BOLD)
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                    .value(NumberUtil.format(employeeTotalHour))
                                                    .font(PDType1Font.HELVETICA_BOLD)
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .build(),
                                    });
                            row = table.createRow(20f);
                            bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;
                            generateCells(row, new int[]{36, 756},
                                    new CellAttributes[]{
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM).fillColor(bgColor).build(),
                                    });
                            employeeStHour = BigDecimal.ZERO;
                            employeeOtHour = BigDecimal.ZERO;
                            employeeDtHour = BigDecimal.ZERO;
                            employeeTotalHour = BigDecimal.ZERO;
                        }
                    }
                    case CLASSIFICATION_CODE -> {
                        Color bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;
                        Row<PDPage> row = table.createRow(20f);
                        StringBuilder craftBuffer = new StringBuilder(craft);
                        if (StringUtils.hasText(craftDescription)) {
                            craftBuffer.append(" ").append(craftDescription);
                        }
                        generateCells(row, new int[]{36, 212, 288, 342, 396, 458, 546, 636, 752},
                                new CellAttributes[]{
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(craftBuffer.toString())
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .leftPadding(10f)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(timesheet)
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.format(stHour))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.format(otHour))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.format(stHour.add(otHour)))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(stBilling))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(otBilling))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(stBilling.add(otBilling)))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                });
                    }
                    case PER_DIEM_EXPENSE -> {
                        Row<PDPage> row;
                        Color bgColor;
                        Long projectId = (Long) value.get("projectId");
                        Project project = idProjectMap.getOrDefault(projectId, new Project());
                        perDiemDays++;

                        employeePerDiem = employeePerDiem.add(pdBilling);

                        if (i == 0) {
                            row = table.createRow(20f);
                            bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;
                            generateCells(row, new int[]{20, 118, 250, 334, 418, 480, 528, 590},
                                    new CellAttributes[]{
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .value(empId)
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .value(String.format("%s - %s", name, craft))
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .build(),
                                    });
                        }

                        row = table.createRow(20f);
                        bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;
                        generateCells(row, new int[]{20, 118, 250, 334, 418, 480, 528, 590},
                                new CellAttributes[]{
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .fillColor(bgColor)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(project.getJobSubJob())
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(timesheet)
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(String.format("%s [%s]",
                                                        formatter2.format(LocalDate.parse(dateOfServiceStr, formatter1)),
                                                        LocalDateUtil.getDayOfWeekName(LocalDate.parse(dateOfServiceStr, formatter1))))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(pdBilling))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value("1")
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(pdBilling))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                });

                        if (i == sortedValues.size() - 1) {
                            row = table.createRow(20f);
                            bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;
                            generateCells(row, new int[]{20, 118, 250, 480, 528, 590},
                                    new CellAttributes[]{
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM)
                                                    .value(String.format("Totals for Employee %s %s %s", empId, name, craft))
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .font(PDType1Font.HELVETICA_BOLD)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                    .value(String.valueOf(perDiemDays))
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .font(PDType1Font.HELVETICA_BOLD)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                    .value(NumberUtil.currencyFormat(employeePerDiem))
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .font(PDType1Font.HELVETICA_BOLD)
                                                    .build(),
                                    });
                            row = table.createRow(20f);
                            bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;
                            generateCells(row, new int[]{36, 756},
                                    new CellAttributes[]{
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM).fillColor(bgColor).build(),
                                    });
                            employeePerDiem = BigDecimal.ZERO;
                            perDiemDays = 0;
                        }
                    }
                    case COST_BY_TOTALS -> {
                        Row<PDPage> row = table.createRow(20f);
                        Color bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;
                        generateCells(row, new int[]{36, 258, 317, 376, 443, 534, 631, 756},
                                new CellAttributes[]{
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(timesheet)
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.format(stHour))
                                                .textColor(Color.black)
                                                .fillColor(bgColor)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.format(otHour))
                                                .textColor(Color.black)
                                                .fillColor(bgColor)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.format(totalHour))
                                                .textColor(Color.black)
                                                .fillColor(bgColor)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(stBilling))
                                                .textColor(Color.black)
                                                .fillColor(bgColor)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(otBilling))
                                                .textColor(Color.black)
                                                .fillColor(bgColor)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(totalBilling))
                                                .textColor(Color.black)
                                                .fillColor(bgColor)
                                                .build(),
                                });
                    }
                    case TRAVEL_PAY_SUMMARY -> {
                        Row<PDPage> row;
                        Color bgColor;
                        Long projectId = (Long) value.get("projectId");
                        Project project = idProjectMap.getOrDefault(projectId, new Project());
                        mobDays++;

                        employeeMob = employeeMob.add(mobBilling);

                        if (i == 0) {
                            row = table.createRow(20f);
                            bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;
                            generateCells(row, new int[]{20, 118, 250, 334, 418, 480, 528, 590},
                                    new CellAttributes[]{
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .value(empId)
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .value(String.format("%s - %s", name, craft))
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .build(),
                                    });
                        }

                        row = table.createRow(20f);
                        bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;
                        generateCells(row, new int[]{20, 118, 250, 334, 418, 480, 528, 590},
                                new CellAttributes[]{
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .fillColor(bgColor)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(project.getJobSubJob())
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(timesheet)
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(String.format("%s [%s]",
                                                        formatter2.format(LocalDate.parse(dateOfServiceStr, formatter1)),
                                                        LocalDateUtil.getDayOfWeekName(LocalDate.parse(dateOfServiceStr, formatter1))))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(mobBilling))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                .value("1")
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                .value(NumberUtil.currencyFormat(mobBilling))
                                                .fillColor(bgColor)
                                                .textColor(Color.black)
                                                .build(),
                                });

                        if (i == sortedValues.size() - 1) {
                            row = table.createRow(20f);
                            bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;
                            generateCells(row, new int[]{20, 118, 250, 480, 528, 590},
                                    new CellAttributes[]{
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                                    .fillColor(bgColor)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM)
                                                    .value(String.format("Totals for Employee %s %s %s", empId, name, craft))
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .font(PDType1Font.HELVETICA_BOLD)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                    .value(String.valueOf(mobDays))
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .font(PDType1Font.HELVETICA_BOLD)
                                                    .build(),
                                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                                    .value(NumberUtil.currencyFormat(employeeMob))
                                                    .fillColor(bgColor)
                                                    .textColor(Color.black)
                                                    .font(PDType1Font.HELVETICA_BOLD)
                                                    .build(),
                                    });
                            row = table.createRow(20f);
                            bgColor = rowBgIndex++ % 2 == 0 ? Color.lightGray : Color.white;
                            generateCells(row, new int[]{36, 756},
                                    new CellAttributes[]{
                                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM).fillColor(bgColor).build(),
                                    });
                            employeeMob = BigDecimal.ZERO;
                            mobDays = 0;
                        }
                    }
                    default -> throw new IllegalArgumentException("Not implemented");
                }

            }
        }



        Map<String, Object> result = new HashMap<>();
        result.put("stHour", categoryStHour);
        result.put("otHour", categoryOtHour);
        result.put("dtHour", categoryDtHour);
        result.put("totalHour", categoryTotalHour);
        result.put("stBilling", categoryStBilling);
        result.put("otBilling", categoryOtBilling);
        result.put("dtBilling", categoryDtBilling);
        result.put("totalBilling", categoryTotalBilling);
        result.put("pdBilling", categoryPdBilling);
        result.put("mobBilling", categoryMobBilling);
        result.put("rowBgIndex", rowBgIndex);
        return result;
    }


    private void drawEmployeeHoursAndBillingHeaderRow(Row<PDPage> row) {
        generateCells(row, new int[]{36, 90, 186, 248, 292, 342, 392, 442, 494, 546, 598, 650, 703, 756},
                new CellAttributes[]{
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Emp. ID")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Name")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Classification")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("ST Hours")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("OT Hours")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("DT Hours")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Total Hours")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("ST Billing")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("OT Billing")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("DT Billing")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("PD")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Travel")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Total Billing")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                });
    }

    private void drawCostByTotalsHeaderRow(Row<PDPage> row) {
        generateCells(row, new int[]{36, 258, 317, 376, 443, 534, 631, 756},
                new CellAttributes[]{
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Timesheet")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("ST")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("OT")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Total")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("ST Client Billing")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("OT Client Billing")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Total Client Billing")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                });
    }

    private void drawEmployeeHoursHeaderRow(Row<PDPage> row) {
        generateCells(row, new int[]{36, 90, 170, 296, 380, 438, 522, 572, 638, 690, 752},
                new CellAttributes[]{
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Number")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Badge ID")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Name and Class")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("JobNumber")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Date")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Time Sheet")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("ST Hours")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("OT Hours")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("DT Hours")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Total Hours")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                });
    }

    private void drawPerDiemExpenseHeaderRow(Row<PDPage> row) {
        generateCells(row, new int[]{20, 118, 250, 334, 418, 480, 528, 590},
                new CellAttributes[]{
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Employee Number")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Employee Name")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Time Sheet")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Date")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Rate")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("# of Days")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Amount")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                });
    }

    private void drawClassificationCodeHeaderRow(Row<PDPage> row) {
        generateCells(row, new int[]{36, 212, 288, 342, 396, 458, 546, 636, 752},
                new CellAttributes[]{
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Tracking Category - Classification")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Timesheet")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("ST Hours")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("OT Hours")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Total Hours")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("ST Client Billing")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("OT Client Billing")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                        new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                .value("Total Client Billing")
                                .fillColor(hex2Rgb("#767070"))
                                .textColor(Color.white)
                                .build(),
                });
    }
}

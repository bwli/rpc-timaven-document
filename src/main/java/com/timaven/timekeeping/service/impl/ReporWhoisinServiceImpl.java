package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.Employee;
import com.timaven.timekeeping.model.Project;
import com.timaven.timekeeping.service.ExcelService;
import com.timaven.timekeeping.service.ReportWhoisinService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Service
public class ReporWhoisinServiceImpl implements ReportWhoisinService {

    private final ProviderAPI providerAPI;
    private final ExcelService excelService;

    @Autowired
    public ReporWhoisinServiceImpl(ProviderAPI providerAPI, ExcelService excelService) {
        this.providerAPI = providerAPI;
        this.excelService = excelService;
    }

    @Override
    public ResponseEntity<InputStreamResource> reportWhoisin(Long projectId, LocalDate dateOfService, Set<Long> empIds) {
        List<Employee> employees = providerAPI.findEmployeeByTeamNameAndProjectIdAndDateOfService("", projectId, dateOfService).block();
        Project project = providerAPI.findProjectById(projectId).block();

        if (CollectionUtils.isEmpty(employees)) {
            return excelService.downloadEmpty();
        }
        try{
            XSSFWorkbook workbook = new XSSFWorkbook();

            XSSFCellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont headerFont = workbook.createFont();
            headerFont.setFontName("Arial");
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setBold(true);
            headerStyle.setFont(headerFont);

            XSSFCellStyle rightStyle = workbook.createCellStyle();
            rightStyle.setAlignment(HorizontalAlignment.RIGHT);

            XSSFCellStyle centerStyle = workbook.createCellStyle();
            centerStyle.setAlignment(HorizontalAlignment.CENTER);

            XSSFCellStyle centerBoldStyle = workbook.createCellStyle();
            centerBoldStyle.setAlignment(HorizontalAlignment.CENTER);
            centerBoldStyle.setFont(headerFont);

            Sheet sheet = workbook.createSheet("data");

            int currentRowIndex = -1;
            int currentColumnIndex = -1;

            final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

            Row row = sheet.createRow(++currentRowIndex);

            Cell cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(dateFormatter.format(dateOfService));

            ++currentColumnIndex;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("WHO's IN");

            String jobNumber = project.getJobNumber() == null ? "" : project.getJobNumber();
            String subJob = project.getSubJob() == null ? "" : project.getSubJob();
            String jobSubJob = String.format("%s %s-%s", "JOB#", jobNumber, subJob);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(jobSubJob);
            cell.setCellStyle(rightStyle);

            currentRowIndex++;
            currentColumnIndex = -1;

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex++;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("On-site Count");
            cell.setCellStyle(centerBoldStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(employees.size());
            cell.setCellStyle(centerBoldStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(String.format("%s %d", "Absent Count", empIds.size()));
            cell.setCellStyle(centerBoldStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Emp No.");
            cell.setCellStyle(centerStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Name");
            cell.setCellStyle(centerStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Craft");
            cell.setCellStyle(centerStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Accounted for in Evacuation");
            cell.setCellStyle(centerStyle);

            List<Employee> employeeList = new ArrayList<>(employees);
            Collections.sort(employeeList);
            for (Employee employee : employeeList) {
                row = sheet.createRow(++currentRowIndex);
                currentColumnIndex = -1;
                String lastName = employee.getLastName() == null ? "" : employee.getLastName();
                String firstName = employee.getFirstName() == null ? "" : employee.getFirstName();
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(employee.getEmpId());
                cell.setCellStyle(centerStyle);
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(String.format("%s, %s", lastName, firstName));
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(employee.getCraft() == null ? "" : employee.getCraft());
                cell.setCellStyle(centerStyle);
                cell = row.createCell(++currentColumnIndex);
                // old cod empIds.contains(employee.getEmpId())
                if (empIds.contains(employee.getId())) {
                    cell.setCellValue("ABSENT");
                }
                cell.setCellStyle(centerStyle);
            }

            return excelService.downloadExcel(String.format("Who is in on %s", dateOfService), workbook);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }
}

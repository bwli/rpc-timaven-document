package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;
import com.timaven.timekeeping.service.ExcelService;
import com.timaven.timekeeping.service.ReportLaborTimeSummaryService;
import com.timaven.timekeeping.util.LocalDateUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class ReportLaborTimeSummaryServiceImpl implements ReportLaborTimeSummaryService {

    private final ProviderAPI providerAPI;
    private final ExcelService excelService;

    @Autowired
    public ReportLaborTimeSummaryServiceImpl(ProviderAPI providerAPI, ExcelService excelService) {
        this.providerAPI = providerAPI;
        this.excelService = excelService;
    }

    @Override
    public ResponseEntity<InputStreamResource> reportLaborTimeSummary(Set<Long> projectIds, LocalDate weekEndDate, Boolean approvedOnly) {
        try{
            LocalDate weekStartDate = weekEndDate.minusDays(6);
            List<WeeklyProcess> weeklyProcessesList = providerAPI.findWeeklyProcessesByProjectIdAndWeeklyEndDate(projectIds, weekEndDate,
                    weekStartDate, approvedOnly ? AllocationSubmissionStatus.APPROVED : null).block();

            Map<String, String> employees = new HashMap<>();
            Map<String, Set<String>> employeesCraft = new HashMap<>();
            Map<Long, String> projectJobSubJob = new HashMap<>();
            Map<Long , Map<String, BigDecimal>> totalDataByProjectIdAndEmployeeId = new HashMap<>();

            weeklyProcessesList.stream()
                    .filter(e -> !CollectionUtils.isEmpty(e.getAllocationTimes()))
                    .map(e -> e.getAllocationTimes())
                    .flatMap(Set::stream)
                    .sorted(Comparator.comparing(AllocationTime::getLastName, Comparator.nullsLast(Comparator.naturalOrder()))
                            .thenComparing(AllocationTime::getFirstName, Comparator.nullsLast(Comparator.naturalOrder())))
                    .forEach(e -> {
                        if(!employees.containsKey(e.getEmpId())){
                            employees.put(e.getEmpId(), e.getLastName() + " " + e.getFirstName());
                        }
                    });

            List<Project> projectList = providerAPI.findProjectByIds(projectIds).block();
            List<EmployeeView> employeeInfoList = providerAPI.findEmployeeByProjectIdsAndEmployeeIds(projectIds, employees.keySet()).block();

            projectJobSubJob = projectList.stream().collect(Collectors.toMap(e -> e.getId(), e -> e.getJobSubJob()));

            employeeInfoList.stream().forEach(e -> {
                if(employeesCraft.containsKey(e.getEmpId())){
                    employeesCraft.get(e.getEmpId()).add(e.getCraft());
                }else{
                    employeesCraft.put(e.getEmpId(), new HashSet<>(){{add(e.getCraft());}});
                }
            });

            weeklyProcessesList.stream()
                    .filter(e -> !CollectionUtils.isEmpty(e.getAllocationTimes()))
                    .forEach(e -> {
                        if(!totalDataByProjectIdAndEmployeeId.containsKey(e.getProjectId())){
                            totalDataByProjectIdAndEmployeeId.put(e.getProjectId(), new HashMap<>());
                        }
                        e.getAllocationTimes().forEach(k -> {
                            if(!totalDataByProjectIdAndEmployeeId.get(e.getProjectId()).containsKey(k.getEmpId())){
                                totalDataByProjectIdAndEmployeeId.get(e.getProjectId()).put(k.getEmpId(), k.getTotalHour());
                            }else{
                                BigDecimal now = totalDataByProjectIdAndEmployeeId.get(e.getProjectId()).get(k.getEmpId());
                                now = now.add(k.getTotalHour());
                                totalDataByProjectIdAndEmployeeId.get(e.getProjectId()).put(k.getEmpId(),now);
                            }
                        });
                    });

            XSSFWorkbook workbook = new XSSFWorkbook();
            Sheet sheet = workbook.createSheet("Week Total");
            int rowIndex = 0;

            XSSFCellStyle headerStyle = workbook.createCellStyle();
            XSSFCellStyle centerStyle = workbook.createCellStyle();
            XSSFCellStyle cellStyle = workbook.createCellStyle();

            XSSFFont font = workbook.createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            font.setBold(true);

            centerStyle.setAlignment(org.apache.poi.ss.usermodel.HorizontalAlignment.CENTER);
            centerStyle.setVerticalAlignment(org.apache.poi.ss.usermodel.VerticalAlignment.CENTER);
            centerStyle.setFont(font);
            centerStyle.setBorderTop(BorderStyle.MEDIUM);
            centerStyle.setBorderBottom(BorderStyle.MEDIUM);
            centerStyle.setBorderLeft(BorderStyle.MEDIUM);
            centerStyle.setBorderRight(BorderStyle.MEDIUM);

            headerStyle.setFont(font);
            headerStyle.setBorderTop(BorderStyle.MEDIUM);
            headerStyle.setBorderBottom(BorderStyle.MEDIUM);
            headerStyle.setBorderLeft(BorderStyle.MEDIUM);
            headerStyle.setBorderRight(BorderStyle.MEDIUM);

            final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

            Row header = sheet.createRow(rowIndex++);
            CellRangeAddress cellAddresses = new CellRangeAddress(0, 0, 0, 2 + projectIds.size());
            sheet.addMergedRegion(cellAddresses);
            Cell headerCell = header.createCell(0);
            headerCell.setCellValue("Week Total     " +
                    String.format("%s [%s]", dateFormatter.format(weekStartDate), LocalDateUtil.getDayOfWeekName(weekStartDate))
                    + "   TO  " +
                    String.format("%s [%s]", dateFormatter.format(weekEndDate), LocalDateUtil.getDayOfWeekName(weekEndDate)));
            headerCell.setCellStyle(headerStyle);


            header = sheet.createRow(rowIndex++);
            int headCellIndex = 0;
            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("EMP#");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("CRAFT");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("EMPLOYEE NAME");
            headerCell.setCellStyle(headerStyle);

            for(Long projectId : projectIds){
                headerCell = header.createCell(headCellIndex++);
                headerCell.setCellValue(projectJobSubJob.get(projectId));
                headerCell.setCellStyle(headerStyle);
            }

            List<String> employeeIds = employees.entrySet().stream()
                    .sorted(Comparator.comparing(Map.Entry::getValue))
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());

            for(String employeeId :  employeeIds){
                Row row = sheet.createRow(rowIndex++);
                int cellIndex = 0;

                Cell cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeId);
                cell.setCellStyle(cellStyle);

                StringBuffer craft = new StringBuffer();
                employeesCraft.get(employeeId).stream().forEach(e -> {
                    if(craft.isEmpty()){
                        craft.append(e);
                    }else{
                        craft.append(","+e);
                    }
                });
                cell = row.createCell(cellIndex++);
                cell.setCellValue(craft.toString());
                cell.setCellStyle(cellStyle);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employees.get(employeeId));
                cell.setCellStyle(cellStyle);

                for(Long projectId : projectIds){
                    String data = !Objects.isNull(totalDataByProjectIdAndEmployeeId.get(projectId)) && !Objects.isNull(totalDataByProjectIdAndEmployeeId.get(projectId).get(employeeId))
                            ? totalDataByProjectIdAndEmployeeId.get(projectId).get(employeeId).toString() : "";

                    cell = row.createCell(cellIndex++);
                    cell.setCellValue(data);
                    cell.setCellStyle(cellStyle);
                }
            }

            return excelService.downloadExcel("Labor Time Sheet Summary", workbook);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }
}

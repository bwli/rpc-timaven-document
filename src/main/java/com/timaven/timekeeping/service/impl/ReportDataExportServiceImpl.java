package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.AllocationTimeFilter;
import com.timaven.timekeeping.model.dto.EquipmentAllocationTimeFilter;
import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;
import com.timaven.timekeeping.model.enums.CostCodeType;
import com.timaven.timekeeping.model.enums.EquipmentAllocationSubmissionStatus;
import com.timaven.timekeeping.service.ExcelService;
import com.timaven.timekeeping.service.ReportDataExportService;
import com.timaven.timekeeping.util.NumberUtil;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuple6;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

@Service
public class ReportDataExportServiceImpl implements ReportDataExportService {

    private final ProviderAPI providerAPI;
    private final ExcelService excelService;

    @Autowired
    public ReportDataExportServiceImpl(ProviderAPI providerAPI, ExcelService excelService) {
        this.providerAPI = providerAPI;
        this.excelService = excelService;
    }

    @Override
    public ResponseEntity<InputStreamResource> reportDataExport(Long projectId, LocalDate startDate, LocalDate endDate, boolean approvedOnly) {
        try {

            Tuple2<Rule, List<BillingCodeType>> tuple = Mono.zip(providerAPI.findRuleByProjectId(projectId), providerAPI.findBillingCodeTypes(true)).block();
            if (tuple == null) {
                return ResponseEntity.status(HttpServletResponse.SC_NO_CONTENT).build();
            }
            Rule rule = tuple.getT1();
            List<BillingCodeType> billingCodeTypes = tuple.getT2();

            XSSFWorkbook workbook = new XSSFWorkbook();
            final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
            final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd HH:mm");


            CellStyle numberStyle = workbook.createCellStyle();
            numberStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.##"));

            Sheet sheet = workbook.createSheet("labor");
            addLaborSheet(workbook, sheet, numberStyle, projectId, startDate, endDate, approvedOnly, dateFormatter,
                    dateTimeFormatter, timeFormatter, rule, billingCodeTypes);
            sheet = workbook.createSheet("equipment");
            addEquipmentSheet(workbook, sheet, numberStyle, projectId, startDate, endDate, approvedOnly, dateFormatter, rule, billingCodeTypes);
            return excelService.downloadExcel(String.format("Data Export %s-%s", startDate.toString(),
                    endDate.toString()), workbook);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }

    private void addLaborSheet(XSSFWorkbook workbook, Sheet sheet, CellStyle numberStyle,
                               Long projectId, LocalDate startDate, LocalDate endDate,
                               boolean approvedOnly, DateTimeFormatter dateFormatter, DateTimeFormatter dateTimeFormatter,
                               DateTimeFormatter timeFormatter, Rule rule, List<BillingCodeType> billingCodeTypes) {
        AllocationTimeFilter allocationTimeFilter = new AllocationTimeFilter.AllocationTimeFilterBuilder()
                .startDate(startDate)
                .endDate(endDate)
                .projectId(projectId)
                .status(approvedOnly ? AllocationSubmissionStatus.APPROVED : null)
                .notStatus(AllocationSubmissionStatus.DENIED)
                .includes(List.of("crafts", "all"))
                .excludePerDiem(true)
                .build();

        Tuple6<List<AllocationTime>, List<User>, List<CostCodeTagAssociation>, List<CostCodeTagAssociation>, List<CostCodeTagAssociation>, List<CostCodeAssociation>> result =
                providerAPI.getAllocationTimes(allocationTimeFilter)
                        .defaultIfEmpty(new ArrayList<>())
                        .flatMap(allocationTimes -> {
                            Set<Long> approvedUserIds = allocationTimes.stream()
                                    .map(AllocationTime::getAllocationSubmission)
                                    .map(AllocationSubmission::getApproveUserId)
                                    .filter(Objects::nonNull)
                                    .collect(toSet());
                            Set<Long> allocationTimeIds = allocationTimes.stream()
                                    .map(AllocationTime::getId)
                                    .collect(toSet());
                            Set<Long> laborKeys = allocationTimes.stream()
                                    .map(AllocationTime::getCostCodePercs)
                                    .filter(ccp -> !CollectionUtils.isEmpty(ccp))
                                    .flatMap(Collection::stream)
                                    .map(CostCodePerc::getId)
                                    .collect(toSet());
                            return Mono.deferContextual(Mono::just).flatMap(
                                    ctx -> {
                                        ProviderAPI api = ctx.get(ProviderAPI.class);
                                        Mono<List<User>> usersMono = api.findUsersById(approvedUserIds);
                                        Mono<List<CostCodeTagAssociation>> laborAssociationMono = api.findCostCodeTagAssociationsByKeysAndType(laborKeys, CostCodeType.DEFAULT).defaultIfEmpty(new ArrayList<>());
                                        Mono<List<CostCodeTagAssociation>> perDiemAssociationMono = api.findCostCodeTagAssociationsByKeysAndType(allocationTimeIds, CostCodeType.PERDIEM).defaultIfEmpty(new ArrayList<>());
                                        Mono<List<CostCodeTagAssociation>> mobAssociationMono = api.findCostCodeTagAssociationsByKeysAndType(allocationTimeIds, CostCodeType.MOB).defaultIfEmpty(new ArrayList<>());
                                        Set<String> laborCostCodeSet = allocationTimes.stream()
                                                .map(AllocationTime::getCostCodePercs)
                                                .filter(ccp -> !CollectionUtils.isEmpty(ccp))
                                                .flatMap(Collection::stream)
                                                .map(CostCodePerc::getCostCodeFull)
                                                .collect(toSet());
                                        Set<String> perDiemCostCodeSet = allocationTimes.stream()
                                                .filter(AllocationTime::isHasPerDiem)
                                                .map(AllocationTime::getPerDiemCostCode)
                                                .filter(StringUtils::hasText)
                                                .collect(toSet());
                                        Set<String> mobCostCodeSet = allocationTimes.stream()
                                                .filter(AllocationTime::isHasMob)
                                                .map(AllocationTime::getMobCostCode)
                                                .filter(StringUtils::hasText)
                                                .collect(toSet());
                                        Set<String> costCodeSet = Stream.of(laborCostCodeSet, perDiemCostCodeSet, mobCostCodeSet)
                                                .flatMap(Collection::stream)
                                                .collect(toSet());

                                        Mono<List<CostCodeAssociation>> costCodeAssociationsMono = CollectionUtils.isEmpty(costCodeSet) ?
                                                Mono.just(new ArrayList<>()) :
                                                api.findCostCodeAssociationsByProjectIdAndDates(projectId, startDate, endDate, costCodeSet).defaultIfEmpty(new ArrayList<>());
                                        return Mono.zip(Mono.just(allocationTimes), usersMono, laborAssociationMono, perDiemAssociationMono, mobAssociationMono, costCodeAssociationsMono);
                                    });

                        })
                        .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())))
                        .block();

        if (result == null) {
            return;
        }

        List<AllocationTime> allocationTimes = result.getT1();
        if (CollectionUtils.isEmpty(allocationTimes)) return;
        allocationTimes = allocationTimes.stream()
                .sorted(Comparator.comparing((AllocationTime t) -> t.getAllocationSubmission().getDateOfService(), Comparator.nullsLast(Comparator.naturalOrder()))
                        .thenComparing(AllocationTime.getAllocationTimeComparatorByName()))
                .collect(Collectors.toList());

        List<User> users = result.getT2();
        Map<Long, User> idUserMap = users.stream()
                .collect(Collectors.toMap(User::getId, Function.identity()));
        List<CostCodeTagAssociation> laborAssociations = result.getT3();
        List<CostCodeTagAssociation> perDiemAssociations = result.getT4();
        List<CostCodeTagAssociation> mobAssociations = result.getT5();
        List<CostCodeAssociation> associations = result.getT6();

        try {
            Row header = sheet.createRow(0);
            XSSFCellStyle headerStyle = workbook.createCellStyle();

            XSSFFont font = workbook.createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            font.setBold(true);
            headerStyle.setFont(font);

            headerStyle.setBorderTop(BorderStyle.MEDIUM);
            headerStyle.setBorderBottom(BorderStyle.MEDIUM);
            headerStyle.setBorderLeft(BorderStyle.MEDIUM);
            headerStyle.setBorderRight(BorderStyle.MEDIUM);

            int headCellIndex = 0;
            Cell headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Date Of Service");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("WE Date");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Name");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Unique ID");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Craft");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("First In");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Last Out");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Net Time");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Total Hours");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Exception Hours");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Adjusted Time");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Reason for Exception");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Approved by");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Approved at");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("ST Billing");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("OT Billing");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Per diem");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Mob/Demob");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Total Billing");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Cost Code");
            headerCell.setCellStyle(headerStyle);

            for (BillingCodeType billingCodeType : billingCodeTypes) {
                headerCell = header.createCell(headCellIndex++);
                headerCell.setCellValue(billingCodeType.getCodeType());
                headerCell.setCellStyle(headerStyle);
            }

            CellStyle style = workbook.createCellStyle();

            List<String> tagHeaders = Stream.of(laborAssociations, perDiemAssociations, mobAssociations)
                    .flatMap(Collection::stream)
                    .map(CostCodeTagAssociation::getTagType)
                    .distinct()
                    .sorted()
                    .collect(Collectors.toList());

            Map<String, Integer> tagHeaderIndexMap = new HashMap<>();
            for (String tagHead : tagHeaders) {
                tagHeaderIndexMap.put(tagHead, headCellIndex);
                headerCell = header.createCell(headCellIndex++);
                headerCell.setCellValue(tagHead);
                headerCell.setCellStyle(headerStyle);
            }

            Map<Long, List<CostCodeTagAssociation>> laborIdAssociationsMap = laborAssociations.stream()
                    .collect(Collectors.groupingBy(CostCodeTagAssociation::getKey));
            Map<Long, List<CostCodeTagAssociation>> perDiemIdAssociationsMap = perDiemAssociations.stream()
                    .collect(Collectors.groupingBy(CostCodeTagAssociation::getKey));
            Map<Long, List<CostCodeTagAssociation>> mobAssociationsMap = mobAssociations.stream()
                    .collect(Collectors.groupingBy(CostCodeTagAssociation::getKey));

            Map<String, List<CostCodeAssociation>> codeAssociationMap = associations.stream()
                    .collect(Collectors.groupingBy(CostCodeAssociation::getCostCode));

            int rowIndex = 1;

            for (AllocationTime allocationTime : allocationTimes) {
                LocalDate dateOfService = allocationTime.getAllocationSubmission().getDateOfService();
                LocalDate weekEndDate = rule.getEndOfWeek(dateOfService);
                AllocationSubmission submission = allocationTime.getAllocationSubmission();
                String approvedBy = "";
                if (submission.getApproveUserId() != null && idUserMap.containsKey(submission.getApproveUserId())) {
                    approvedBy = idUserMap.get(submission.getApproveUserId()).getDisplayName();
                }
                String approvedAt = "";
                if (submission.getApprovedAt() != null) {
                    approvedAt = submission.getApprovedAt().format(dateTimeFormatter);
                }

                Set<AllocationRecord> allocationRecordSet = allocationTime.getAllocationRecords();

                Optional<AllocationRecord> firstIn = allocationRecordSet.stream()
                        .filter(AllocationRecord::isValid)
                        .filter(AllocationRecord::getSide)
                        .findFirst();

                Optional<AllocationRecord> lastOut = allocationRecordSet.stream()
                        .filter(AllocationRecord::isValid)
                        .filter(e -> !e.getSide())
                        .reduce((first, second) -> second);

                String actualIn =
                        firstIn.map(allocationRecord -> allocationRecord.getNetEventTime().format(timeFormatter)).orElse("");

                String actualOut =
                        lastOut.map(allocationRecord -> allocationRecord.getNetEventTime().format(timeFormatter)).orElse("");

                BigDecimal exceptionHours =
                        allocationTime.getAllocatedHour().subtract(allocationTime.getTotalHour()).stripTrailingZeros();

                AllocationException allocationException = allocationTime.getAllocationException();
                String exception = allocationException == null ? "" : allocationException.getException();

                Craft craft = allocationTime.getEmployee().getCraftEntity();
                BigDecimal billableSt = craft.getBillableST();
                BigDecimal billableOt = craft.getBillableOT();

                allocationTime.getCostCodePercs().forEach(ccp -> ccp.setCostCodeTagAssociations(laborIdAssociationsMap.getOrDefault(ccp.getId(), new ArrayList<>())));

                Set<CostCodePerc> costCodePercs = new TreeSet<>(allocationTime.getCostCodePercs());


                for (CostCodePerc costCodePerc : costCodePercs) {
                    String costCode = costCodePerc.getCostCodeFull() == null ? "" : costCodePerc.getCostCodeFull();

                    BigDecimal stHour = costCodePerc.getStHour();
                    stHour = stHour == null ? BigDecimal.ZERO : stHour;
                    BigDecimal otHour = costCodePerc.getOtHour();
                    otHour = otHour == null ? BigDecimal.ZERO : otHour;

                    if (stHour.compareTo(BigDecimal.ZERO) == 0 && otHour.compareTo(BigDecimal.ZERO) == 0) continue;

                    BigDecimal stBilling = new BigDecimal(stHour.multiply(billableSt).stripTrailingZeros().toPlainString());
                    BigDecimal otBilling = new BigDecimal(otHour.multiply(billableOt).stripTrailingZeros().toPlainString());
                    BigDecimal totalStOtBilling = new BigDecimal(stBilling.add(otBilling).stripTrailingZeros().toPlainString());

                    Row row = sheet.createRow(rowIndex++);
                    fillAllocationTime(allocationTime, dateOfService, approvedBy, approvedAt, row, weekEndDate,
                            style, numberStyle, dateFormatter, actualIn, actualOut,
                            allocationTime.getNetHour(), allocationTime.getTotalHour(), exceptionHours,
                            allocationTime.getAllocatedHour(), exception, NumberUtil.currencyFormat(stBilling),
                            NumberUtil.currencyFormat(otBilling), "$0", "$0",
                            NumberUtil.currencyFormat(totalStOtBilling), costCode,
                            codeAssociationMap, billingCodeTypes, tagHeaderIndexMap, costCodePerc.getCostCodeTagAssociations());
                }

                if (allocationTime.isHasPerDiem()) {
                    Row row = sheet.createRow(rowIndex++);
                    BigDecimal perDiemAmountDec = (allocationTime.isHasPerDiem() && allocationTime.getPerDiemAmount() != null) ? allocationTime.getPerDiemAmount() : BigDecimal.ZERO;
                    String perDiemAmount = NumberUtil.currencyFormat(new BigDecimal(perDiemAmountDec.stripTrailingZeros().toPlainString()));

                    String perDiemCostCode = allocationTime.getPerDiemCostCode() == null ? "" : allocationTime.getPerDiemCostCode();
                    fillAllocationTime(allocationTime, dateOfService, approvedBy, approvedAt, row, weekEndDate,
                            style, numberStyle, dateFormatter, "", "",
                            BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO,
                            BigDecimal.ZERO, "", "$0", "$0", perDiemAmount, "$0",
                            perDiemAmount, perDiemCostCode, codeAssociationMap, billingCodeTypes,
                            tagHeaderIndexMap, perDiemIdAssociationsMap.getOrDefault(allocationTime.getId(), new ArrayList<>()));
                }

                BigDecimal mobAmountDec = allocationTime.getMobAmount() == null ? BigDecimal.ZERO : allocationTime.getMobAmount();
                String mobAmount = NumberUtil.currencyFormat(new BigDecimal(mobAmountDec.stripTrailingZeros().toPlainString()));

                if (StringUtils.hasText(allocationTime.getMobCostCode()) && allocationTime.isHasMob()) {
                    Row row = sheet.createRow(rowIndex++);
                    String mobCostCode = allocationTime.getMobCostCode();
                    fillAllocationTime(allocationTime, dateOfService, approvedBy, approvedAt, row, weekEndDate,
                            style, numberStyle, dateFormatter, "", "",
                            BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO,
                            BigDecimal.ZERO, "", "$0", "$0", "$0", mobAmount, mobAmount,
                            mobCostCode, codeAssociationMap, billingCodeTypes,
                            tagHeaderIndexMap, mobAssociationsMap.getOrDefault(allocationTime.getId(), new ArrayList<>()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fillAllocationTime(AllocationTime allocationTime, LocalDate dateOfService, String approvedBy,
                                    String approvedAt, Row row, LocalDate weekEndDate,
                                    CellStyle style, CellStyle numberStyle, DateTimeFormatter dateFormatter,
                                    String actualIn, String actualOut, BigDecimal netHour, BigDecimal totalHour,
                                    BigDecimal exceptionHours, BigDecimal adjustedTime, String reasonForException,
                                    String stBilling, String otBilling, String perDiemAmount,
                                    String mobAmount, String totalBilling, String costCode,
                                    Map<String, List<CostCodeAssociation>> codeAssociationMap,
                                    List<BillingCodeType> billingCodeTypes, Map<String, Integer> tagHeaderIndexMap,
                                    List<CostCodeTagAssociation> associations) {


        int cellIndex = 0;

        // DOS
        Cell cell = row.createCell(cellIndex++);
        cell.setCellValue(dateOfService.format(dateFormatter));
        cell.setCellStyle(style);

        cell = row.createCell(cellIndex++);
        cell.setCellValue(weekEndDate.format(dateFormatter));
        cell.setCellStyle(style);

        // Name
        cell = row.createCell(cellIndex++);
        cell.setCellValue(allocationTime.getFullName());
        cell.setCellStyle(style);

        // Unique ID
        cell = row.createCell(cellIndex++);
        cell.setCellValue(allocationTime.getEmpId());
        cell.setCellStyle(style);

        cell = row.createCell(cellIndex++);
        cell.setCellValue(allocationTime.getEmployee().getCraft());
        cell.setCellStyle(style);

        // First In
        cell = row.createCell(cellIndex++);
        cell.setCellValue(actualIn);
        cell.setCellStyle(style);

        // Last Out
        cell = row.createCell(cellIndex++);
        cell.setCellValue(actualOut);
        cell.setCellStyle(style);

        // Net Time
        cell = row.createCell(cellIndex++);
        if (netHour == null) {
            cell.setCellValue(0);
        } else {
            cell.setCellValue(netHour.doubleValue());
        }
        cell.setCellStyle(numberStyle);

        // Total Hours
        cell = row.createCell(cellIndex++);
        if (totalHour == null) {
            cell.setCellValue(0);
        } else {
            cell.setCellValue(totalHour.doubleValue());
        }
        cell.setCellStyle(numberStyle);

        // Exception Hours
        cell = row.createCell(cellIndex++);
        if (exceptionHours == null) {
            cell.setCellValue(0);
        } else {
            cell.setCellValue(exceptionHours.doubleValue());
        }
        cell.setCellStyle(numberStyle);

        // Adjusted Time
        cell = row.createCell(cellIndex++);
        if (adjustedTime == null) {
            cell.setCellValue(0);
        } else {
            cell.setCellValue(adjustedTime.doubleValue());
        }
        cell.setCellStyle(numberStyle);

        // Reason for Exception
        cell = row.createCell(cellIndex++);
        cell.setCellValue(reasonForException);
        cell.setCellStyle(style);

        // Exception approved by
        cell = row.createCell(cellIndex++);
        if (StringUtils.hasText(approvedBy)) {
            cell.setCellValue(approvedBy);
        } else {
            cell.setBlank();
        }
        cell.setCellStyle(style);


        // Exception approved at
        cell = row.createCell(cellIndex++);
        if (StringUtils.hasText(approvedAt)) {
            cell.setCellValue(approvedAt);
        } else {
            cell.setBlank();
        }
        cell.setCellStyle(style);

        // ST Billing

        cell = row.createCell(cellIndex++);
        cell.setCellValue(stBilling);
        cell.setCellStyle(style);

        // OT Billing
        cell = row.createCell(cellIndex++);
        cell.setCellValue(otBilling);
        cell.setCellStyle(style);

        // Per Diem
        cell = row.createCell(cellIndex++);
        cell.setCellValue(perDiemAmount);
        cell.setCellStyle(style);

        // Mob/Demob
        cell = row.createCell(cellIndex++);
        cell.setCellValue(mobAmount);
        cell.setCellStyle(style);

        // Total Billing
        cell = row.createCell(cellIndex++);
        cell.setCellValue(totalBilling);
        cell.setCellStyle(style);

        cell = row.createCell(cellIndex++);
        cell.setCellValue(costCode);
        cell.setCellStyle(style);

        Optional<CostCodeAssociation> associationOpt = codeAssociationMap.getOrDefault(costCode, new ArrayList<>())
                .stream()
                .filter(e -> (e.getStartDate().isBefore(dateOfService)
                        || e.getStartDate().isEqual(dateOfService)))
                .max(Comparator.comparing(CostCodeAssociation::getCreatedAt, Comparator.nullsFirst(Comparator.naturalOrder())));

        if (associationOpt.isPresent() && (associationOpt.get().getEndDate() == null
                || associationOpt.get().getEndDate().isAfter(dateOfService)
                || associationOpt.get().getEndDate().isEqual(dateOfService))) {
            CostCodeAssociation association = associationOpt.get();
            for (int k = 1; k <= billingCodeTypes.size(); k++) {
                cell = row.createCell(cellIndex++);
                String levelK = association.getLevel(k);
                if (levelK == null) {
                    cell.setBlank();
                } else {
                    cell.setCellValue(levelK);
                }
                cell.setCellStyle(style);
            }
        }
        if (associations.size() > 0) {
            Map<String, String> tagCodesMap = associations.stream()
                    .collect(Collectors.groupingBy(CostCodeTagAssociation::getTagType,
                            Collectors.mapping(CostCodeTagAssociation::getCodeName, Collectors.joining(", "))));
            for (Map.Entry<String, String> entry : tagCodesMap.entrySet()) {
                int index = tagHeaderIndexMap.get(entry.getKey());
                cell = row.createCell(index);
                if (StringUtils.hasText(entry.getKey())) {
                    cell.setCellValue(entry.getValue());
                } else {
                    cell.setBlank();
                }
                cell.setCellStyle(style);
            }
        }
    }

    private void addEquipmentSheet(XSSFWorkbook workbook, Sheet sheet, CellStyle numberStyle,
                                   Long projectId, LocalDate startDate, LocalDate endDate,
                                   boolean approvedOnly, DateTimeFormatter dateFormatter, Rule rule, List<BillingCodeType> billingCodeTypes) {
        try {
            EquipmentAllocationTimeFilter allocationTimeFilter = new EquipmentAllocationTimeFilter.EquipmentAllocationTimeFilterBuilder()
                    .startDate(startDate)
                    .endDate(endDate)
                    .projectId(projectId)
                    .status(approvedOnly ? EquipmentAllocationSubmissionStatus.APPROVED : null)
                    .build();

            Tuple2<Project, Tuple2<List<EquipmentAllocationTime>, List<CostCodeAssociation>>> result =
                    Mono.zip(providerAPI.findProjectById(projectId), providerAPI.findEquipmentAllocationTimes(allocationTimeFilter)
                                    .flatMap(allocationTimes -> Mono.deferContextual(Mono::just).flatMap(
                                            ctx -> {
                                                ProviderAPI api = ctx.get(ProviderAPI.class);
                                                Set<String> costCodeSet = allocationTimes.stream()
                                                        .map(EquipmentAllocationTime::getEquipmentCostCodePercs)
                                                        .filter(ccp -> !CollectionUtils.isEmpty(ccp))
                                                        .flatMap(Collection::stream)
                                                        .map(EquipmentCostCodePerc::getCostCodeFull)
                                                        .collect(toSet());
                                                Mono<List<CostCodeAssociation>> costCodeAssociationsMono = CollectionUtils.isEmpty(costCodeSet) ?
                                                        Mono.just(new ArrayList<>()) :
                                                        api.findCostCodeAssociationsByProjectIdAndDates(projectId, startDate, endDate, costCodeSet).defaultIfEmpty(new ArrayList<>());

                                                return Mono.zip(Mono.just(allocationTimes), costCodeAssociationsMono);
                                            })))
                            .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())))
                            .block();

            int currentRowIndex = -1;
            int currentColumnIndex = -1;

            Row row = sheet.createRow(++currentRowIndex);

            XSSFCellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont headerFont = workbook.createFont();
            headerFont.setFontName("Arial");
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setBold(true);
            headerStyle.setFont(headerFont);

            Cell cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Work Date");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("WE Date");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("NRC Badge Number");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Equipment Number");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Equipment Name");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Work Classification");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Work Authorization Number");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Job");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Subjob");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Activity Number");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("ST Hours");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("OT Hours");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Total Hours");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("ST Billing");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("OT Billing");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Total Billing");
            cell.setCellStyle(headerStyle);

            int costCodeStartIndex = currentColumnIndex + 1;

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Cost Code");
            cell.setCellStyle(headerStyle);

            for (BillingCodeType billingCodeType : billingCodeTypes) {
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(billingCodeType.getCodeType());
                cell.setCellStyle(headerStyle);
            }

            if (result == null) {
                return;
            }
            List<EquipmentAllocationTime> equipmentAllocationTimes = result.getT2().getT1();
            if (CollectionUtils.isEmpty(equipmentAllocationTimes)) return;
            Project project = result.getT1();
            List<CostCodeAssociation> associations = result.getT2().getT2();

            equipmentAllocationTimes = equipmentAllocationTimes.stream()
                    .sorted(Comparator.comparing((EquipmentAllocationTime t) -> t.getEquipmentAllocationSubmission().getDateOfService())
                            .thenComparing(EquipmentAllocationTime::getEquipmentId))
                    .collect(Collectors.toList());

            Map<String, List<CostCodeAssociation>> codeAssociationMap = associations.stream()
                    .collect(Collectors.groupingBy(CostCodeAssociation::getCostCode));

            CellStyle dateStyle = workbook.createCellStyle();
            CreationHelper creationHelper = workbook.getCreationHelper();
            dateStyle.setDataFormat(creationHelper.createDataFormat().getFormat("m/dd/yyyy"));

            int rowIndex = 1;
            for (EquipmentAllocationTime allocationTime : equipmentAllocationTimes) {

                Set<EquipmentCostCodePerc> costCodePercs = new TreeSet<>(allocationTime.getEquipmentCostCodePercs());
                LocalDate dateOfService = allocationTime.getEquipmentAllocationSubmission().getDateOfService();
                LocalDate weekEndDate = rule.getEndOfWeek(dateOfService);

                if (!CollectionUtils.isEmpty(costCodePercs)) {
                    row = sheet.createRow(rowIndex++);
                    int j = 0;
                    for (EquipmentCostCodePerc costCodePerc : costCodePercs) {
                        if (j == 0) {
                            addEquipmentAllocationTimeLine(row, allocationTime,
                                    dateStyle, numberStyle, dateFormatter, weekEndDate, project);
                        }
                        Optional<CostCodeAssociation> associationOpt = codeAssociationMap.getOrDefault(costCodePerc.getCostCodeFull(), new ArrayList<>())
                                .stream()
                                .filter(e -> (e.getStartDate().isBefore(dateOfService)
                                        || e.getStartDate().isEqual(dateOfService)))
                                .max(Comparator.comparing(CostCodeAssociation::getCreatedAt, Comparator.nullsFirst(Comparator.naturalOrder())));

                        if (associationOpt.isPresent() && (associationOpt.get().getEndDate() == null
                                || associationOpt.get().getEndDate().isAfter(dateOfService)
                                || associationOpt.get().getEndDate().isEqual(dateOfService))) {
                            CostCodeAssociation association = associationOpt.get();
                            int cellIndex = costCodeStartIndex;

                            cell = row.createCell(cellIndex++);
                            cell.setCellValue(association.getCostCode());

                            for (int k = 1; k <= billingCodeTypes.size(); k++) {
                                cell = row.createCell(cellIndex++);
                                String levelK = association.getLevel(k);
                                if (levelK == null) {
                                    cell.setBlank();
                                } else {
                                    cell.setCellValue(levelK);
                                }
                            }
                        }
                        j++;
                    }
                } else {
                    row = sheet.createRow(rowIndex++);
                    addEquipmentAllocationTimeLine(row, allocationTime,
                            dateStyle, numberStyle, dateFormatter, weekEndDate, project);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addEquipmentAllocationTimeLine(Row row, EquipmentAllocationTime allocationTime,
                                                CellStyle dateStyle, CellStyle numberStyle,
                                                DateTimeFormatter dateFormatter,
                                                LocalDate weekEndDate, Project project) {
        int currentColumnIndex = -1;
        LocalDate dateOfService = allocationTime.getEquipmentAllocationSubmission().getDateOfService();
        Cell cell = row.createCell(++currentColumnIndex);
        cell.setCellValue(dateOfService.format(dateFormatter));
        cell.setCellStyle(dateStyle);

        cell = row.createCell(++currentColumnIndex);
        cell.setCellValue(weekEndDate.format(dateFormatter));
        cell.setCellStyle(dateStyle);

        currentColumnIndex++;
        cell = row.createCell(++currentColumnIndex);
        cell.setCellValue(allocationTime.getEquipmentId());

        cell = row.createCell(++currentColumnIndex);
        cell.setCellValue(allocationTime.getDescription());

        cell = row.createCell(++currentColumnIndex);
        cell.setCellValue(allocationTime.getEquipmentClass());

        cell = row.createCell(++currentColumnIndex);
        cell.setCellValue(allocationTime.getSerialNumber());

        String jobNumber = project.getJobNumber();
        jobNumber = jobNumber == null ? "" : jobNumber.trim();
        String subJob = project.getSubJob();
        subJob = subJob == null ? "" : subJob.trim();

        cell = row.createCell(++currentColumnIndex);
        cell.setCellValue(jobNumber);

        cell = row.createCell(++currentColumnIndex);
        cell.setCellValue(subJob);

        cell = row.createCell(++currentColumnIndex);
        cell.setCellValue(String.valueOf(allocationTime.getId()));

        cell = row.createCell(++currentColumnIndex);
        cell.setCellValue(allocationTime.getTotalHour().doubleValue());
        cell.setCellStyle(numberStyle);

        cell = row.createCell(++currentColumnIndex);
        cell.setCellValue(BigDecimal.ZERO.doubleValue());
        cell.setCellStyle(numberStyle);

        cell = row.createCell(++currentColumnIndex);
        cell.setCellValue(allocationTime.getTotalHour().doubleValue());
        cell.setCellStyle(numberStyle);

        BigDecimal stBilling = allocationTime.getTotalCharge();

        cell = row.createCell(++currentColumnIndex);
        cell.setCellValue(NumberUtil.currencyFormat(stBilling));
        cell.setCellStyle(numberStyle);

        cell = row.createCell(++currentColumnIndex);
        cell.setCellValue(NumberUtil.currencyFormat(BigDecimal.ZERO));
        cell.setCellStyle(numberStyle);

        cell = row.createCell(++currentColumnIndex);
        cell.setCellValue(NumberUtil.currencyFormat(stBilling));
        cell.setCellStyle(numberStyle);

    }
}

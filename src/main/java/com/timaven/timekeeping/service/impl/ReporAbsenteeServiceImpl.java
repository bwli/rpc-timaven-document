package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.Employee;
import com.timaven.timekeeping.service.ExcelService;
import com.timaven.timekeeping.service.ReportAbsenteeService;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Service
public class ReporAbsenteeServiceImpl implements ReportAbsenteeService {

    private final ProviderAPI providerAPI;
    private final ExcelService excelService;

    @Autowired
    public ReporAbsenteeServiceImpl(ProviderAPI providerAPI, ExcelService excelService) {
        this.providerAPI = providerAPI;
        this.excelService = excelService;
    }

    @Override
    public ResponseEntity<InputStreamResource> reportAbsentee(LocalDate dateOfService, Set<Long> employeeIds) {
        List<Employee> employees = providerAPI.findEmployeeByIds(employeeIds).block();
        if (CollectionUtils.isEmpty(employees)) {
            return excelService.downloadEmpty();
        }
        try{
            XSSFWorkbook workbook = new XSSFWorkbook();

            XSSFCellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont headerFont = workbook.createFont();
            headerFont.setFontName("Arial");
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setBold(true);
            headerStyle.setFont(headerFont);

            Sheet sheet = workbook.createSheet("data");

            Row header = sheet.createRow(0);
            int headCellIndex = 0;
            Cell headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Employee");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("ID");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Craft Code");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Supervisor");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Team");
            headerCell.setCellStyle(headerStyle);


            int currentRowIndex = 0;
            for (Employee employee : employees) {
                int currentColumnIndex = -1;
                Row row = sheet.createRow(++currentRowIndex);

                Cell cell = row.createCell(++currentColumnIndex);
                String lastName = employee.getLastName() == null ? "" : employee.getLastName();
                String firstName = employee.getFirstName() == null ? "" : employee.getFirstName();
                cell.setCellValue(String.format("%s, %s", lastName, firstName));

                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(employee.getEmpId());

                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(employee.getCraft());

                cell = row.createCell(++currentColumnIndex);
                String crew = employee.getCrew() == null ? "" : employee.getCrew();
                cell.setCellValue(crew);

                String team = employee.getTeamName() == null ? "" : employee.getTeamName();
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(team);
            }
            return excelService.downloadExcel(String.format("Absentee report for %s", dateOfService), workbook);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }
}

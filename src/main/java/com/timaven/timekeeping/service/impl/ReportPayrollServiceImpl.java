package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.controller.BaseController;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.CostCodeBillingCodeFilter;
import com.timaven.timekeeping.model.dto.ECMSMapping;
import com.timaven.timekeeping.model.dto.WeeklyPayrollDto;
import com.timaven.timekeeping.model.enums.AllocationTimeHourType;
import com.timaven.timekeeping.model.enums.WeeklyProcessType;
import com.timaven.timekeeping.service.ECMSService;
import com.timaven.timekeeping.service.ExcelService;
import com.timaven.timekeeping.service.ReportPayrollService;
import com.timaven.timekeeping.util.DateRange;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple5;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toSet;

@Service
public class ReportPayrollServiceImpl implements ReportPayrollService {

    private final ProviderAPI providerAPI;
    private final ExcelService excelService;
    private final ECMSService ecmsService;

    @Autowired
    public ReportPayrollServiceImpl(ProviderAPI providerAPI, ExcelService excelService, ECMSService ecmsService) {
        this.providerAPI = providerAPI;
        this.excelService = excelService;
        this.ecmsService = ecmsService;
    }

    @Override
    public ResponseEntity<InputStreamResource> exportPayrollReport(WeeklyPayrollDto weeklyPayrollDto) {
        boolean hasAny = false;

        try {
            XSSFWorkbook workbook = new XSSFWorkbook();
            final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("M/dd/yyyy");
            XSSFCellStyle headerStyle = workbook.createCellStyle();

            XSSFFont headerFont = workbook.createFont();
            headerFont.setFontName("Arial");
            headerFont.setFontHeightInPoints((short) 10);
            headerFont.setBold(true);
            headerStyle.setFont(headerFont);

            headerStyle.setBorderTop(BorderStyle.MEDIUM);
            headerStyle.setBorderBottom(BorderStyle.MEDIUM);
            headerStyle.setBorderLeft(BorderStyle.MEDIUM);
            headerStyle.setBorderRight(BorderStyle.MEDIUM);

            CellStyle style = workbook.createCellStyle();
            CellStyle numberStyle = workbook.createCellStyle();
            numberStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.##"));

            CellStyle dateStyle = workbook.createCellStyle();
            CreationHelper creationHelper = workbook.getCreationHelper();
            dateStyle.setDataFormat(creationHelper.createDataFormat().getFormat("m/dd/yyyy"));

            ECMSMapping mapping = ecmsService.getECMSMapping();
            Map<String, String> craftCodeEmplClassMap = mapping.getCraftCodeEmplClassMap();
            Map<String, String> craftCodeEmplTypeMap = mapping.getCraftCodeEmplTypeMap();
//            Map<String, String> costCodeOhCodeMap = mapping.getOHCodeMap();

            Set<String> sheetNames = new HashSet<>();

            List<Tuple5<WeeklyProcess, Rule, List<CostCodeAssociation>, Project, Map<LocalDate, Map<String, BillingCode>>>> result = providerAPI.findWeeklyProcessesById(weeklyPayrollDto.getIds(), Set.of("costCodePercs", "crafts"))
                    .flatMap(weeklyProcess -> {

                        Long projectId = weeklyProcess.getProjectId();
                        Set<AllocationTime> allocationTimes = weeklyProcess.getAllocationTimes();
                        Map<LocalDate, List<String>> dateCostCodesMap = allocationTimes.stream()
                                .collect(Collectors.groupingBy(t -> t.getAllocationSubmission().getDateOfService(),
                                        Collectors.flatMapping(t -> {
                                            Set<String> costCodes = new HashSet<>();
                                            if (t.isHasPerDiem() && StringUtils.hasText(t.getPerDiemCostCode())) {
                                                costCodes.add(t.getPerDiemCostCode());
                                            }
                                            if (t.isHasMob() && StringUtils.hasText(t.getMobCostCode())) {
                                                costCodes.add(t.getMobCostCode());
                                            }
                                            if (!CollectionUtils.isEmpty(t.getCostCodePercs())) {
                                                costCodes.addAll(t.getCostCodePercs().stream().map(CostCodePerc::getCostCodeFull).filter(StringUtils::hasText).collect(toSet()));
                                            }
                                            return costCodes.stream();
                                        }, Collectors.toList())));

                        return Mono.deferContextual(Mono::just).flatMap(
                                ctx -> {
                                    ProviderAPI api = ctx.get(ProviderAPI.class);
                                    Mono<Rule> ruleMono = api.findRuleByProjectId(projectId).defaultIfEmpty(new Rule());
                                    Mono<List<CostCodeAssociation>> costCodeAssociationSetMono = api.findCostCodeAssociationsByProjectIdAndDateOfService(projectId, weeklyProcess.getWeekEndDate(), new HashSet<>());
                                    Mono<Project> projectMono = api.findProjectById(projectId);
                                    return Mono.zip(Mono.just(weeklyProcess), ruleMono, costCodeAssociationSetMono, projectMono, Flux.fromIterable(dateCostCodesMap.entrySet())
                                            .flatMap(entry -> {
                                                LocalDate dateOfService = entry.getKey();
                                                List<String> costCodes = entry.getValue();
                                                return api.findCostCodesByProjectAndDate(projectId, dateOfService, null, true, costCodes)
                                                        .defaultIfEmpty(new ArrayList<>())
                                                        .flatMap(costCodeLogs -> {
                                                            Set<Long> costCodeIds = costCodeLogs.stream().map(CostCodeLog::getId).collect(toSet());
                                                            CostCodeBillingCodeFilter filter = new CostCodeBillingCodeFilter(costCodeIds, Set.of("costCodeLog"));
                                                            return api.findCostCodeBillingCodesByCostCodeIds(filter)
                                                                    .defaultIfEmpty(new HashSet<>())
                                                                    .flatMap(costCodeBillingCodes -> {
                                                                        Map<String, BillingCode> codeBillingCodeMap = costCodeBillingCodes.stream()
                                                                                .filter(ccbc -> ccbc.getBillingCode().getBillingCodeType().getCodeType().equals("Class"))
                                                                                .collect(Collectors.toMap(ccbc -> ccbc.getCostCodeLog().getCostCodeFull(), CostCodeBillingCode::getBillingCode, (e1, e2) -> e1)); // Class billing code support to have only one
                                                                        return Mono.just(Map.of(dateOfService, codeBillingCodeMap));
                                                                    });
                                                        });
                                            }).collectList()
                                            .flatMap(dateCodeBillingCodesMapList -> Mono.just(dateCodeBillingCodesMapList.stream()
                                                    .flatMap(e -> e.entrySet().stream())
                                                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (v1, v2) -> v1)))));
                                });
                    })
                    .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())))
                    .collectList()
                    .block();
            if (!CollectionUtils.isEmpty(result)) {
                Set<Long> allocationSubmissionIds = new HashSet<>();
                for (Tuple5<WeeklyProcess, Rule, List<CostCodeAssociation>, Project, Map<LocalDate, Map<String, BillingCode>>> tuple : result) {
                    WeeklyProcess weeklyProcess = tuple.getT1();
                    Long projectId = weeklyProcess.getProjectId();
                    Rule rule = tuple.getT2();
                    Map<String, CostCodeAssociation> codeAssociationMap = tuple.getT3().stream()
                            .collect(Collectors.toMap(CostCodeAssociation::getCostCode, Function.identity(),
                                    (e1, e2) -> e1.getCreatedAt().isBefore(e2.getCreatedAt()) ? e2 : e1));
                    Project project = tuple.getT4();
                    Map<LocalDate, Map<String, BillingCode>> dateCodeBillingCodesMap = tuple.getT5();

                    LocalDate weekEndDate = weeklyProcess.getWeekEndDate();
                    LocalDate weekEndDateToWrite = weekEndDate;
                    boolean isTimeLeftOff = weeklyProcess.getType() == WeeklyProcessType.TLO;

                    // TM-327 set allocation_time exported = true
                    allocationSubmissionIds.addAll(weeklyProcess.getAllocationTimes()
                            .stream()
                            .map(AllocationTime::getAllocationSubmission)
                            .map(AllocationSubmission::getId)
                            .collect(Collectors.toSet()));

                    Set<AllocationTime> allocationTimes = weeklyProcess.getAllocationTimes().stream()
                            .filter(t -> !(t.getStHour() == null || t.getStHour().compareTo(BigDecimal.ZERO) == 0)
                                    || !(t.getOtHour() == null || t.getOtHour().compareTo(BigDecimal.ZERO) == 0)
                                    || !(t.getAllocatedHour() == null || t.getAllocatedHour().compareTo(BigDecimal.ZERO) == 0)
                                    || t.isHasPerDiem()
                                    || t.isHasMob()
                                    || !(t.getRigPay() == null || t.getRigPay().compareTo(BigDecimal.ZERO) == 0))
                            .collect(Collectors.toSet());
                    allocationTimes = allocationTimes.stream()
                            .filter(t -> {
                                Boolean billingPurposeOnly = t.getAllocationSubmission().getBillingPurposeOnly();
                                return billingPurposeOnly == null || !billingPurposeOnly;
                            }).collect(Collectors.toSet());
                    if (CollectionUtils.isEmpty(allocationTimes)) continue;
                    hasAny = true;

                    LocalDate weekStartDate = weekEndDate.minusDays(6);
                    List<String> empIds = allocationTimes.stream()
                            .sorted(AllocationTime.getAllocationTimeComparatorByName())
                            .map(AllocationTime::getEmpId)
                            .distinct()
                            .collect(Collectors.toList());

                    Map<LocalDate, Map<String, List<AllocationTime>>> dateEmpIdAllocationTimeMap = allocationTimes.stream()
                            .collect(Collectors
                                    .groupingBy(at -> at.getAllocationSubmission().getDateOfService(),
                                            Collectors.groupingBy(AllocationTime::getEmpId, TreeMap::new,
                                                    Collectors.mapping(Function.identity(),
                                                            Collectors.collectingAndThen(Collectors.toList(), e -> e.stream()
                                                                    .sorted(Comparator.comparing(t -> t.getAllocationSubmission().getDateOfService()))
                                                                    .collect(Collectors.toList()))))));

                    weeklyProcess.setReportGeneratedAt(LocalDateTime.now());
                    weeklyProcess.setReportUserId(BaseController.getUserId());

                    String projectName = "Corporate";
                    if (null != projectId) {
                        projectName = String.format("%s  %s", project.getJobSubJob(), project.getDescription());
                    } else {
                        projectName = projectName + String.format("(%s)", weeklyProcess.getTeamName());
                    }
                    if (isTimeLeftOff) projectName = projectName.concat(" (TLO)");
                    int index = 1;
                    // Avoid duplicate sheet names
                    while (sheetNames.contains(projectName)) {
                        projectName = projectName + index;
                        index++;
                    }
                    sheetNames.add(projectName);

                    Sheet sheet = workbook.createSheet(String.format("%s", projectName));
                    Row header = sheet.createRow(0);

                    fillReportHeader(header, headerStyle);

                    int startLine = 1;

                    List<LocalDate> dateList = new DateRange(weekStartDate, weekEndDate, false).toList();
                    TreeSet<LocalDate> dates = new TreeSet<>();
                    dates.addAll(dateList);
                    dates.addAll(dateEmpIdAllocationTimeMap.keySet());

                    for (LocalDate dateOfService : dates) {
                        Map<String, List<AllocationTime>> curEmpIdAllocationTimeMap = dateEmpIdAllocationTimeMap.getOrDefault(dateOfService, new HashMap<>());
                        for (String empId : empIds) {
                            List<AllocationTime> allocationTimeList = curEmpIdAllocationTimeMap.getOrDefault(empId, new ArrayList<>());

                            if (!CollectionUtils.isEmpty(allocationTimeList)) {
                                for (AllocationTime allocationTime : allocationTimeList) {

                                    Employee employee = allocationTime.getEmployee();

                                    if (isTimeLeftOff) {
                                        weekEndDateToWrite = rule.getEndOfWeek(dateOfService);
                                    }
                                    if (allocationTime.isHasPerDiem()) {
                                        String adjType = weeklyProcess.isTaxablePerDiem() ? "TX" : "NT";
                                        int adjustmentNo = weeklyProcess.isTaxablePerDiem() ? 400 : 401;

                                        String perDiemCostCode = allocationTime.getPerDiemCostCode();

                                        startLine = fillReportRow(sheet, startLine, style, numberStyle, dateStyle, dateFormatter,
                                                dateOfService, weekEndDateToWrite, isTimeLeftOff, project, employee, allocationTime.getEmpId(),
                                                getParsedCostCode(perDiemCostCode, codeAssociationMap),
                                                null, null, null, null,
                                                adjType, adjustmentNo, allocationTime.getPerDiemAmount(), "P",
                                                craftCodeEmplClassMap, craftCodeEmplTypeMap, rule.getWeekEndDay());
                                    }
                                    if (StringUtils.hasText(allocationTime.getMobCostCode()) && allocationTime.isHasMob()) {
                                        String adjType = "NT";
                                        int adjustmentNo = 450;

                                        startLine = fillReportRow(sheet, startLine, style, numberStyle, dateStyle, dateFormatter,
                                                dateOfService, weekEndDateToWrite, isTimeLeftOff, project, employee, allocationTime.getEmpId(),
                                                getParsedCostCode(allocationTime.getMobCostCode(), codeAssociationMap),
                                                null, null, null, null,
                                                adjType, adjustmentNo, allocationTime.getMobAmount(), "D",
                                                craftCodeEmplClassMap, craftCodeEmplTypeMap, rule.getWeekEndDay());
                                    }
                                    if (projectId != null) {
                                        for (CostCodePerc costCodePerc : allocationTime.getCostCodePercs()) {
                                            String ohCode = null;
                                            String costCode = costCodePerc.getCostCodeFull();
                                            BigDecimal othHour = null;
                                            if (StringUtils.hasText(costCode)) {
                                                BillingCode billingCode = dateCodeBillingCodesMap.getOrDefault(dateOfService, new HashMap<>()).getOrDefault(costCode, null);
                                                if (billingCode != null) {
                                                    ohCode = billingCode.getCodeName();
                                                }
                                                if (ohCode != null) {
                                                    othHour = costCodePerc.getStHour().add(costCodePerc.getOtHour())
                                                            .add(costCodePerc.getDtHour());
                                                }
                                            }
                                            startLine = fillReportRow(sheet, startLine, style, numberStyle, dateStyle, dateFormatter,
                                                    dateOfService, weekEndDateToWrite, isTimeLeftOff, project, employee, allocationTime.getEmpId(),
                                                    getParsedCostCode(costCode, codeAssociationMap), ohCode == null ? costCodePerc.getStHour() : null,
                                                    ohCode == null ? costCodePerc.getOtHour() : null,
                                                    ohCode, othHour, null, null, null, "L",
                                                    craftCodeEmplClassMap, craftCodeEmplTypeMap, rule.getWeekEndDay());
                                        }
                                    } else {
                                        BigDecimal stHour = allocationTime.getStHour();
                                        if (stHour.compareTo(BigDecimal.ZERO) != 0) {
                                            String stCostCode = rule.getStCostCode();
                                            startLine = fillReportRow(sheet, startLine, style, numberStyle, dateStyle, dateFormatter,
                                                    dateOfService, weekEndDateToWrite, isTimeLeftOff, project, employee, allocationTime.getEmpId(),
                                                    getParsedCostCode(stCostCode, codeAssociationMap),
                                                    stHour, null, null, null,
                                                    null, null, null, "L",
                                                    craftCodeEmplClassMap, craftCodeEmplTypeMap, rule.getWeekEndDay());
                                        }
                                        if (allocationTime.getOtHour().compareTo(BigDecimal.ZERO) != 0) {
                                            if (allocationTime.getExtraTimeType() == AllocationTimeHourType.OT) {
                                                BigDecimal otHour = allocationTime.getOtHour();
                                                startLine = fillReportRow(sheet, startLine, style, numberStyle, dateStyle, dateFormatter,
                                                        dateOfService, weekEndDateToWrite, isTimeLeftOff, project, employee, allocationTime.getEmpId(),
                                                        getParsedCostCode(rule.getOtCostCode(), codeAssociationMap),
                                                        null, otHour, null, null,
                                                        null, null, null, "L",
                                                        craftCodeEmplClassMap, craftCodeEmplTypeMap, rule.getWeekEndDay());
                                            }
                                            String ohCode = null;
                                            if (allocationTime.getExtraTimeType() == AllocationTimeHourType.HOLIDAY) {
                                                ohCode = "HL";
                                            }
                                            if (allocationTime.getExtraTimeType() == AllocationTimeHourType.SICK) {
                                                ohCode = "SL";
                                            }
                                            if (allocationTime.getExtraTimeType() == AllocationTimeHourType.VACATION) {
                                                ohCode = "VA";
                                            }
                                            if (StringUtils.hasText(ohCode)) {
                                                fillReportRow(sheet, startLine, style, numberStyle, dateStyle, dateFormatter,
                                                        dateOfService, weekEndDateToWrite, isTimeLeftOff, null, employee, allocationTime.getEmpId(),
                                                        getParsedCostCode(rule.getOtCostCode(), codeAssociationMap),
                                                        null, null, ohCode, allocationTime.getOtHour(),
                                                        null, null, null, "L",
                                                        craftCodeEmplClassMap, craftCodeEmplTypeMap, rule.getWeekEndDay());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                providerAPI.updateAllocationSubmissionsExport(allocationSubmissionIds, true).block();
            }

            if (hasAny) {
                return excelService.downloadExcel(String.format("Payroll %s", weeklyPayrollDto.getWeekEndDate().toString()), workbook);
            } else {
                workbook.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!hasAny) {
            return excelService.downloadEmpty();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }

    private int fillReportRow(Sheet sheet, int startLine, CellStyle style, CellStyle numberStyle, CellStyle dateStyle,
                              DateTimeFormatter dateFormatter, LocalDate dateOfService, LocalDate weekEndDate, boolean isTimeLeftOff,
                              Project project, Employee employee, String empId, String costCode, BigDecimal stHour, BigDecimal otHour,
                              String ohCode, BigDecimal othHour, String adjType, Integer adjustmentNo, BigDecimal adjAmount, String costType,
                              Map<String, String> craftCodeEmplClassMap, Map<String, String> craftCodeEmplTypeMap, DayOfWeek lastDayOfWeek) {
        if ((stHour == null || stHour.compareTo(BigDecimal.ZERO) == 0)
                && (otHour == null || otHour.compareTo(BigDecimal.ZERO) == 0)
                && (othHour == null || othHour.compareTo(BigDecimal.ZERO) == 0)
                && (adjAmount == null || adjAmount.compareTo(BigDecimal.ZERO) == 0)) return startLine;

        Row row = sheet.createRow(startLine);

        int cellIndex = 0;

        Cell cell = row.createCell(cellIndex++);
        cell.setCellValue("A");
        cell.setCellStyle(style);

        cell = row.createCell(cellIndex++);
        cell.setCellValue(70);
        cell.setCellStyle(numberStyle);

        cell = row.createCell(cellIndex++);
        cell.setCellValue(0);
        cell.setCellStyle(numberStyle);

        cell = row.createCell(cellIndex++);
        cell.setCellValue(70);
        cell.setCellStyle(numberStyle);

        cell = row.createCell(cellIndex++);
        cell.setCellValue(0);
        cell.setCellStyle(numberStyle);

        cell = row.createCell(cellIndex++);
        cell.setCellValue(70);
        cell.setCellStyle(numberStyle);

        cell = row.createCell(cellIndex++);
        cell.setCellValue(0);
        cell.setCellStyle(numberStyle);

        cell = row.createCell(cellIndex++);
        cell.setCellValue(isTimeLeftOff ? "R3" : "R");
        cell.setCellStyle(style);

        Integer empIdInt = null;
        try {
            empIdInt = Integer.parseInt(empId);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }

        cell = row.createCell(cellIndex++);
        if (empIdInt == null) {
            cell.setBlank();
        } else {
            cell.setCellValue(empIdInt);
        }
        cell.setCellStyle(numberStyle);
        DayOfWeek firstDayOfWeek = lastDayOfWeek.plus(1);
        WeekFields weekFields = WeekFields.of(firstDayOfWeek, 1);
        LocalDate endOfWeek = dateOfService.with(lastDayOfWeek);
        if (endOfWeek.isBefore(dateOfService)) endOfWeek = endOfWeek.plusWeeks(1);
        int weekNumber = endOfWeek.get(weekFields.weekOfMonth());

        cell = row.createCell(cellIndex++);
        cell.setCellValue(weekNumber);
        cell.setCellStyle(numberStyle);

        String jobNumber = project == null ? employee.getJobNumber() : project.getJobNumber();
        jobNumber = jobNumber == null ? "" : jobNumber.trim();
        cell = row.createCell(cellIndex++);
        cell.setCellValue(jobNumber);
        cell.setCellStyle(style);

        String subJob = project == null ? employee.getJobNumber() : project.getSubJob();
        subJob = subJob == null ? "" : subJob.trim();
        cell = row.createCell(cellIndex++);
        cell.setCellValue(subJob);
        cell.setCellStyle(style);

        cell = row.createCell(cellIndex++);
//            cell.setCellValue(costCodePerc.getCostCodeFull() == null ? "" : costCodePerc.getCostCodeFull());
        cell.setCellValue(costCode == null ? "" : costCode);
        cell.setCellStyle(style);

        cell = row.createCell(cellIndex++);
        cell.setCellValue(costType);
        cell.setCellStyle(style);

        String craftCode = employee.getCraft() == null ? "" : employee.getCraft();
        String empClass = craftCodeEmplClassMap.get(craftCode.toUpperCase());
        if (!StringUtils.hasText(empClass)) {
            empClass = craftCodeEmplClassMap.getOrDefault(craftCode.replace("-", "").toUpperCase(), "");
        }

        cell = row.createCell(cellIndex++);
        cell.setCellValue(empClass);
        cell.setCellStyle(style);

        String empType = craftCodeEmplTypeMap.get(craftCode.toUpperCase());
        if (!StringUtils.hasText(empType)) {
            empType = craftCodeEmplTypeMap.getOrDefault(craftCode.replace("-", "").toUpperCase(), "");
        }

        cell = row.createCell(cellIndex++);
        cell.setCellValue(empType);
        cell.setCellStyle(style);

        cell = row.createCell(cellIndex++);
        cell.setCellValue(dateOfService.getDayOfWeek().getValue());
        cell.setCellStyle(numberStyle);

        cell = row.createCell(cellIndex++);
//            cell.setCellValue(costCodePerc.getStHour().toPlainString());
        if (stHour == null) {
            cell.setBlank();
        } else {
            cell.setCellValue(stHour.doubleValue());
        }
        cell.setCellStyle(numberStyle);

        cell = row.createCell(cellIndex++);
//            cell.setCellValue(costCodePerc.getOtHour().toPlainString());
        if (otHour == null) {
            cell.setBlank();
        } else {
            cell.setCellValue(otHour.doubleValue());
        }
        cell.setCellStyle(numberStyle);

        cell = row.createCell(cellIndex++);
        if (othHour == null) {
            cell.setBlank();
        } else {
            cell.setCellValue(othHour.doubleValue());
        }
        cell.setCellStyle(numberStyle);

        cell = row.createCell(cellIndex++);
        if (StringUtils.hasText(ohCode)) {
            cell.setCellValue(ohCode);
        } else {
            cell.setBlank();
        }
        cell.setCellStyle(style);

        cell = row.createCell(cellIndex++);
        cell.setCellValue("H");
        cell.setCellStyle(style);

        cell = row.createCell(cellIndex++);
        cell.setCellValue(dateFormatter.format(weekEndDate));
        cell.setCellStyle(dateStyle);

        Integer batchNo = null;
        try {
            batchNo = Integer.parseInt(jobNumber.concat(subJob));
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        }

        cell = row.createCell(cellIndex++);
        if (batchNo == null) {
            cell.setBlank();
        } else {
            cell.setCellValue(batchNo);
        }
        cell.setCellStyle(numberStyle);

        cell = row.createCell(cellIndex++);
        cell.setCellValue("R");
        cell.setCellStyle(style);

        cell = row.createCell(cellIndex++);
        if (StringUtils.hasText(adjType)) {
            cell.setCellValue(adjType);
        } else {
            cell.setBlank();
        }
        cell.setCellStyle(style);

        cell = row.createCell(cellIndex++);
        if (adjustmentNo != null) {
            cell.setCellValue(adjustmentNo);
        } else {
            cell.setBlank();
        }
        cell.setCellStyle(numberStyle);

        cell = row.createCell(cellIndex);
        if (adjAmount == null) {
            cell.setBlank();
        } else {
            cell.setCellValue(adjAmount.doubleValue());
        }
        cell.setCellStyle(numberStyle);
//        return startLine;
        return startLine + 1;
    }

    private void fillReportHeader(Row header, XSSFCellStyle headerStyle) {
        int headCellIndex = 0;
        Cell headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Record Code");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Src Com");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Src Div");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Dst Com");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Dst Div");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Emp Com");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Emp Div");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Check Type");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Empl No");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Week No");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Job No");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Sub Job");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Cost Code");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Cost Type");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Empl Class");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Empl Type");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Day Of Week");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Reg Hrs");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Ovt Hrs");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Oth Hrs");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Oth Hrs Type");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Pay Type H/S");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("W/Ending Date");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Batch No");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Check Code");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Adj Type");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex++);
        headerCell.setCellValue("Adjustment No");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(headCellIndex);
        headerCell.setCellValue("Adj Amount");
        headerCell.setCellStyle(headerStyle);
    }

    private String getParsedCostCode(String costCode, Map<String, CostCodeAssociation> codeAssociationMap) {
        if (StringUtils.hasText(costCode) && codeAssociationMap.containsKey(costCode)) {
            String parsedCostCode = codeAssociationMap.get(costCode).getCostCodeFull();
            return StringUtils.hasText(parsedCostCode) ? parsedCostCode : costCode;
        }
        return costCode;
    }

}

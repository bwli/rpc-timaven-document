package com.timaven.timekeeping.service.impl;

import be.quodlibet.boxable.*;
import be.quodlibet.boxable.image.Image;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.configuration.tenant.TenantProperties;
import com.timaven.timekeeping.configuration.web.ThreadTenantStorage;
import com.timaven.timekeeping.model.AllocationTime;
import com.timaven.timekeeping.model.Employee;
import com.timaven.timekeeping.model.EmployeeFilter;
import com.timaven.timekeeping.model.Project;
import com.timaven.timekeeping.model.dto.AllocationTimeFilter;
import com.timaven.timekeeping.model.dto.TenantDto;
import com.timaven.timekeeping.model.enums.AllocationSubmissionStatus;
import com.timaven.timekeeping.service.ReportOtService;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuple3;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.List;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.timaven.timekeeping.util.PdfUtility.hex2Rgb;

@Service
public class ReportOtServiceImpl extends PdfBaseService implements ReportOtService {

    public ReportOtServiceImpl(ProviderAPI providerAPI, TenantProperties tenantProperties) {
        super(providerAPI, tenantProperties);
    }

    @Override
    public ResponseEntity<InputStreamResource> generateOtReport1(Long projectId, LocalDate weekEndDate, LocalDate payrollDate) {
        Mono<Project> projectMono = providerAPI.findProjectById(projectId);
        AWSCredentials credentials = new BasicAWSCredentials(s3_access_key, s3_secret_key);
        AmazonS3 s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.US_EAST_2)
                .build();
        String s3LogoKey = tenantProperties.getTenants().getOrDefault(ThreadTenantStorage.getTenantId(), new TenantDto()).getS3Logo();

        Image image = null;
        if (StringUtils.hasText(s3LogoKey)) {
            try {
                image = new Image(ImageIO.read(s3client.getObject(buck_name, s3LogoKey).getObjectContent()));
                float imageWidth = 150f;
                image = image.scaleByWidth(imageWidth);
            } catch (Exception ignore) {
            }
        }

        LocalDate weekStartDate = weekEndDate.minusDays(6);
        EmployeeFilter filter = new EmployeeFilter(projectId, null, null, null, null, weekStartDate, weekEndDate);
        Mono<List<Employee>> employeesMono = providerAPI.getEmployees(filter);
        Tuple2<Project, Tuple3<List<Employee>, List<AllocationTime>, List<AllocationTime>>> tuple =
                Mono.zip(projectMono, employeesMono
                        .flatMap(employees -> {
                            if (employees.isEmpty()) return Mono.empty();
                            employees = employees.stream()
                                    .collect(Collectors.toMap(Employee::getEmpId, Function.identity(),
                                    BinaryOperator.maxBy(Comparator.comparing(Employee::getCreatedAt,
                                                    Comparator.nullsFirst(Comparator.naturalOrder())))))
                                    .values().stream().sorted().collect(Collectors.toList());
                            Set<String> empIds = employees.stream().map(Employee::getEmpId).collect(Collectors.toSet());
                            AllocationTimeFilter allocationTimeFilter1 = new AllocationTimeFilter.AllocationTimeFilterBuilder()
                                    .startDate(weekStartDate)
                                    .endDate(weekEndDate)
                                    .payrollStartDate(payrollDate)
                                    .payrollEndDate(payrollDate)
                                    .projectId(projectId)
                                    .status(AllocationSubmissionStatus.APPROVED)
                                    .empIds(empIds)
                                    .build();
                            AllocationTimeFilter allocationTimeFilter2 = new AllocationTimeFilter.AllocationTimeFilterBuilder()
                                    .startDate(weekStartDate)
                                    .endDate(weekEndDate)
                                    .payrollStartDate(payrollDate)
                                    .payrollEndDate(payrollDate)
                                    .isAllProject(true)
                                    .status(AllocationSubmissionStatus.APPROVED)
                                    .empIds(empIds)
                                    .build();
                            List<Employee> finalEmployees = employees;
                            return Mono.deferContextual(Mono::just)
                                    .flatMap(ctx -> {
                                        ProviderAPI api = ctx.get(ProviderAPI.class);
                                        return Mono.zip(Mono.just(finalEmployees), api.getAllocationTimes(allocationTimeFilter1), api.getAllocationTimes(allocationTimeFilter2));
                                    });
                        }))
                        .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())))
                        .block();

        if (tuple == null) return ResponseEntity.status(HttpServletResponse.SC_NO_CONTENT).build();

        Project project = tuple.getT1();
        Tuple3<List<Employee>, List<AllocationTime>, List<AllocationTime>> tuple3 = tuple.getT2();
        List<Employee> employees = tuple3.getT1();
        List<AllocationTime> projectAllocationTimes = tuple3.getT2();
        List<AllocationTime> allProjectAllocationTimes = tuple3.getT3();

        Map<String, BigDecimal> empTotalTime = allProjectAllocationTimes.stream()
                .collect(Collectors
                        .groupingBy(AllocationTime::getEmpId, Collectors
                                .mapping(AllocationTime::getAllocatedHour, Collectors
                                        .reducing(BigDecimal.ZERO, BigDecimal::add))));

        if (empTotalTime.values().stream().noneMatch(t -> t.compareTo(BigDecimal.valueOf(40)) > 0))
            return ResponseEntity.status(HttpServletResponse.SC_NO_CONTENT).build();

        Map<String, BigDecimal> empProjectTime = projectAllocationTimes.stream()
                .collect(Collectors
                        .groupingBy(AllocationTime::getEmpId, Collectors
                                .mapping(AllocationTime::getAllocatedHour, Collectors
                                        .reducing(BigDecimal.ZERO, BigDecimal::add))));

        PDDocument document = new PDDocument();
        PDPage page = new PDPage(PDRectangle.LETTER);
        document.addPage(page);

        float height = page.getMediaBox().getHeight();
        float width = page.getMediaBox().getWidth();
        float topMargin = 10f;
        float bottomMargin = 50f;
        float leftMargin = 36f;
        float rightMargin = 16f;

        // starting y position is whole page height subtracted by top and bottom margin
        float yStartNewPage = height - (topMargin + bottomMargin);
        // we want table across whole page width (subtracted by left and right margin of course)
        float tableWidth = width - (rightMargin + leftMargin);

        // y position is your coordinate of top left corner of the table
        float yPosition = height - topMargin;

        // Add header
        try {
            PDPageContentStream contentStream = new PDPageContentStream(document, page,
                    PDPageContentStream.AppendMode.APPEND, true);
            BaseTable table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, leftMargin,
                    document, page, false, true);

            // set default line spacing for entire table
            table.setLineSpacing(0.6f);

            Row<PDPage> row = table.createRow(54f);

            Cell<PDPage> cell = row.createCell(50f, "OT Report", HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM);
            cell.setFontSize(25f);
            cell.setFont(PDType1Font.HELVETICA_BOLD);
            cell.setTextColor(hex2Rgb("#c55910"));
            if (image != null) {
                row.createImageCell(50f,
                        image, HorizontalAlignment.RIGHT, VerticalAlignment.MIDDLE);
            } else {
                row.createCell(50f, "", HorizontalAlignment.CENTER, VerticalAlignment.TOP);
            }

            yPosition = table.draw();

            yPosition -= 10;

            contentStream.moveTo(36f, yPosition);
            contentStream.lineTo(width - 34f, yPosition);
            contentStream.setLineWidth(4f);
            contentStream.stroke();

            yPosition -= 10;

            table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, leftMargin,
                    document, page, true, true);

            row = table.createRow(20f);

            cell = row.createCell(23f, "Job - Sub Job Number", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
            cell.setFontSize(10f);
            cell.setFont(PDType1Font.HELVETICA_BOLD);
            cell.setTextColor(Color.white);
            cell.setFillColor(hex2Rgb("#767070"));

            cell = row.createCell(48f, Optional.ofNullable(project.getJobSubJob()).orElse(""), HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
            cell.setFontSize(10f);
            cell.setFont(PDType1Font.HELVETICA);

            yPosition = table.draw();

            yPosition -= 40;

            String weekday = weekEndDate.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.US);

            contentStream.beginText();
            contentStream.newLineAtOffset(36f, yPosition);
            contentStream.setNonStrokingColor(hex2Rgb("#c55910"));
            contentStream.setFont(PDType1Font.HELVETICA_BOLD, 20);
            contentStream.showText(weekday);
            contentStream.endText();

            contentStream.beginText();
            contentStream.newLineAtOffset(166f, yPosition);
            contentStream.setNonStrokingColor(hex2Rgb("#595959"));
            contentStream.setFont(PDType1Font.HELVETICA_BOLD, 20);
            contentStream.showText(weekEndDate.format(DateTimeFormatter.ofPattern("MMM dd, yyyy")));
            contentStream.endText();

            yPosition -= 15;

            table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, leftMargin,
                    document, page, true, true);
            table.setLineSpacing(1.5f);

            row = table.createRow(20f);
            cell = row.createCell(25f, "Emp. ID",
                    HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
            cell.setFillColor(hex2Rgb("#767070"));
            cell.setTextColor(Color.white);
            cell = row.createCell(25f, "Name",
                    HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
            cell.setFillColor(hex2Rgb("#767070"));
            cell.setTextColor(Color.white);
            cell = row.createCell(25f, "Hours on this project",
                    HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
            cell.setFillColor(hex2Rgb("#767070"));
            cell.setTextColor(Color.white);
            cell = row.createCell(25f, "Total Hours",
                    HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
            cell.setFillColor(hex2Rgb("#767070"));
            cell.setTextColor(Color.white);

            table.addHeaderRow(row);

            for (Employee employee : employees) {
                if (!StringUtils.hasText(employee.getEmpId())) employee.setEmpId("");
                if (empTotalTime.getOrDefault(employee.getEmpId(), BigDecimal.ZERO).compareTo(BigDecimal.valueOf(40)) < 0)
                    continue;

                Color bgColor = Color.white;

                BigDecimal projectTime = empProjectTime.getOrDefault(employee.getEmpId(), BigDecimal.ZERO);
                BigDecimal totalTime = empTotalTime.getOrDefault(employee.getEmpId(), BigDecimal.ZERO);

                row = table.createRow(20f);
                cell = row.createCell(25f, employee.getEmpId(),
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                cell.setFillColor(bgColor);
                cell = row.createCell(25f, employee.getFullName(),
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                cell.setFillColor(bgColor);
                cell = row.createCell(25f, projectTime.stripTrailingZeros().toPlainString(),
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                cell.setFillColor(bgColor);
                cell = row.createCell(25f, totalTime.stripTrailingZeros().toPlainString(),
                        HorizontalAlignment.CENTER, VerticalAlignment.BOTTOM);
                cell.setFillColor(bgColor);
            }
            yPosition = table.draw();

            yPosition -= 25;

            if (table.getCurrentPage() != page) {
                contentStream.close();
                page = table.getCurrentPage();
                contentStream = new PDPageContentStream(document, page,
                        PDPageContentStream.AppendMode.APPEND, true);
            }

            table = new BaseTable(yPosition, yPosition, 0f, 330f, leftMargin,
                    document, page, true, true);

            row = table.createRow(20f);

            cell = row.createCell(33f, "Authorization (if required)", HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);
            cell.setFontSize(8f);
            cell.setFont(PDType1Font.HELVETICA_BOLD);
            cell.setTextColor(Color.white);
            cell.setFillColor(hex2Rgb("#767070"));
            cell.setLeftPadding(0f);
            cell.setRightPadding(0f);

            row.createCell(66f, "", HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);

            table.draw();

            table = new BaseTable(yPosition, yPosition, 0f, 180f, 416f,
                    document, page, true, true);

            row = table.createRow(20f);

            cell = row.createCell(40f, "Date", HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE);
            cell.setFontSize(8f);
            cell.setFont(PDType1Font.HELVETICA_BOLD);
            cell.setTextColor(Color.white);
            cell.setFillColor(hex2Rgb("#767070"));

            row.createCell(60f, "", HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE);

            table.draw();

            contentStream.close();

            addFooter(document);

        } catch (IOException e) {
            e.printStackTrace();
        }


        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            document.save(outputStream);
            document.close();

            InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
            headers.add("Content-Type", "application/force-download");
            headers.add("Content-Disposition", String.format("attachment; filename=%s_ot_report.pdf", weekEndDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))));
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_PDF)
                    .body(new InputStreamResource(inputStream));
        } catch (IOException ex) {
            ex.printStackTrace();
            return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
        }
    }
}

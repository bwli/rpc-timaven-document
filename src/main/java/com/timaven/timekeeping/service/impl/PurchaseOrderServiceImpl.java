package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.PurchaseOrder;
import com.timaven.timekeeping.model.PurchaseOrderSubmission;
import com.timaven.timekeeping.model.PurchaseVendorRoster;
import com.timaven.timekeeping.model.dto.ReceivedPurchaseOrderDto;
import com.timaven.timekeeping.service.PurchaseOrderService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@Transactional
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

    private final ProviderAPI providerAPI;

    @Autowired
    public PurchaseOrderServiceImpl(ProviderAPI providerAPI) {
        this.providerAPI = providerAPI;
    }

    @Override
    public void savePurchaseOrders(List<PurchaseOrder> purchaseOrders, String submittedBy) {
        PurchaseOrderSubmission purchaseOrderSubmission = new PurchaseOrderSubmission();
        purchaseOrderSubmission.setSubmittedBy(submittedBy);
        purchaseOrderSubmission.setPurchaseOrders(new HashSet<>(purchaseOrders));
        Map<String, String> orderNumberMap = purchaseOrders.stream()
                .collect(Collectors.toMap(PurchaseOrder::getRequisitionNumber, PurchaseOrder::getPurchaseOrderNumber,
                        (e1, e2) -> e1));
        Set<String> purchaseOrderNumbers = purchaseOrders.stream()
                .map(PurchaseOrder::getPurchaseOrderNumber)
                .collect(Collectors.toSet());

        providerAPI.savePurchaseOrderSubmission(purchaseOrderSubmission).subscribe();

        providerAPI.findRequisitionsByPurchaseOrderNumberNull()
                .flatMap(requisition -> Mono.deferContextual(Mono::just)
                        .flatMap(ctx -> {
                            ProviderAPI api = ctx.get(ProviderAPI.class);
                            if (orderNumberMap.containsKey(requisition.getRequisitionNumber())) {
                                requisition.setPurchaseOrderNumber(orderNumberMap.get(requisition.getRequisitionNumber()));
                                return api.saveRequisition(requisition);
                            } else {
                                return Mono.empty();
                            }
                        }))
                .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())))
                .blockLast();

        providerAPI.deletePurchaseOrderGroupByPurchaseOrderNumbers(purchaseOrderNumbers).subscribe();

        providerAPI.findPurchaseOrderGroupByPurchaseOrderNumbers(purchaseOrderNumbers)
                .flatMap(purchaseOrderGroup -> Mono.deferContextual(Mono::just)
                        .flatMap(ctx -> {
                            ProviderAPI api = ctx.get(ProviderAPI.class);
                            purchaseOrderGroup.setFlag(null);
                            return api.savePurchaseOrderGroup(purchaseOrderGroup);
                        }))
                .contextWrite(ctx -> ctx.put(ProviderAPI.class, new ProviderAPI(providerAPI.getWebClient())))
                .blockLast();
    }

    @Override
    public void saveReceivedPurchases(List<ReceivedPurchaseOrderDto> receivedPurchaseOrderDtos, String submittedBy) {
        PurchaseOrderSubmission submission = getLatestPurchaseOrderSubmission();
        if (submission == null) return;
        Set<PurchaseOrder> purchaseOrders = submission.getPurchaseOrders();
        Set<PurchaseOrder> purchaseOrdersToBeUpdated = new HashSet<>();
        Map<ReceivedPoKey, PurchaseOrder> purchaseOrderMap = purchaseOrders.stream()
                .collect(Collectors.toMap(ReceivedPoKey::new, Function.identity()));
        for (ReceivedPurchaseOrderDto receivedPurchaseOrder : receivedPurchaseOrderDtos) {
            ReceivedPoKey receivedPoKey = new ReceivedPoKey(receivedPurchaseOrder.getPurchaseOrderNumber(),
                    receivedPurchaseOrder.getPoItem());
            if (!purchaseOrderMap.containsKey(receivedPoKey)) continue;
            PurchaseOrder purchaseOrder = purchaseOrderMap.get(receivedPoKey);
            if (purchaseOrder.getQuantityReceived() != null &&
                    !purchaseOrder.getQuantityReceived()
                            .equals(receivedPurchaseOrder.getQuantityReceived())) {
                if (receivedPurchaseOrder.getQuantityReceived().compareTo(BigDecimal.ZERO) < 0) {
                    purchaseOrder.setQuantityReceived(purchaseOrder.getQuantity().subtract(receivedPurchaseOrder.getQuantityReceived()));
                } else {
                    purchaseOrder.setQuantityReceived(receivedPurchaseOrder.getQuantityReceived());
                }
                purchaseOrdersToBeUpdated.add(purchaseOrder);
            }
        }
        if (!CollectionUtils.isEmpty(purchaseOrdersToBeUpdated)) {
            providerAPI.savePurchaseOrders(purchaseOrdersToBeUpdated);
        }
    }

    @Override
    public PurchaseOrderSubmission getLatestPurchaseOrderSubmission() {
        return providerAPI.getLatestPurchaseOrderSubmission().block();
    }

    @Override
    public void savePurchaseVendorRoster(List<PurchaseVendorRoster> purchaseVendorRosterList) {
        providerAPI.savePurchaseVendorRoster(purchaseVendorRosterList).block();
    }
}

@Data
class ReceivedPoKey {
    private String purchaseOrderNumber;
    private Integer lineNumber;
    public ReceivedPoKey(PurchaseOrder purchaseOrder) {
        this.purchaseOrderNumber = purchaseOrder.getPurchaseOrderNumber();
        this.lineNumber = purchaseOrder.getLineNumber();
    }
    public ReceivedPoKey(String purchaseOrderNumber, Integer lineNumber) {
        this.purchaseOrderNumber = purchaseOrderNumber;
        this.lineNumber = lineNumber;
    }
}

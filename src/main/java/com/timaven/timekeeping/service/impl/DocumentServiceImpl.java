package com.timaven.timekeeping.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.timaven.timekeeping.service.DocumentService;
import com.timaven.timekeeping.util.S3Utils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * aws s3 oss operating
 */
@Service
public class DocumentServiceImpl implements DocumentService {

    private final S3Utils s3Utils;

    public DocumentServiceImpl(S3Utils s3Utils) {
        this.s3Utils = s3Utils;
    }

    @Override
    public List<S3ObjectSummary> getAwsS3ListInfo(boolean isTemplate) {
        AmazonS3 s3client = s3Utils.getS3client();
        String buckName = isTemplate ? s3Utils.getTemplateBuckName() : s3Utils.getTenantBuckName();
        if (!StringUtils.hasText(buckName)) return new ArrayList<>();
        ListObjectsV2Result result = s3client.listObjectsV2(buckName);
        return result.getObjectSummaries();
    }

    @Override
    public ResponseEntity<InputStreamResource> downloadFile(String fileName, boolean isTemplate) {
        AmazonS3 s3client = s3Utils.getS3client();
        String buckName = isTemplate ? s3Utils.getTemplateBuckName() : s3Utils.getTenantBuckName();
        if (!StringUtils.hasText(buckName)) return ResponseEntity.status(HttpServletResponse.SC_NO_CONTENT).build();
        S3Object object = s3client.getObject(buckName, fileName);
        S3ObjectInputStream objectContent = object.getObjectContent();
        try {
            InputStream inputStream = new ByteArrayInputStream(objectContent.readAllBytes());
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
            headers.add("Content-Type", "application/force-download");
            headers.add("Content-Disposition", String.format("%s%s", "attachment; filename=", fileName));
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_PDF)
                    .body(new InputStreamResource(inputStream));
        } catch (final IOException ex) {
            ex.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }

    @Override
    public ResponseEntity<Object> uploadCorporateFileByTenantId(MultipartFile multipartFile) {
        AmazonS3 s3client = s3Utils.getS3client();
        String buckName = s3Utils.getTenantBuckName();
        if (!StringUtils.hasText(buckName)) {
            return ResponseEntity.status(HttpServletResponse.SC_NOT_ACCEPTABLE).contentType(MediaType.TEXT_PLAIN).body("s3 bucket not set");
        }
        ListObjectsV2Result result = s3client.listObjectsV2(buckName);

        long sumSize = result.getObjectSummaries().stream()
                .mapToLong(S3ObjectSummary::getSize)
                .sum();
        // TM-287 get tenant max upload size
        Long maxUploadSize = s3Utils.getMaxUploadSize();

        if (sumSize + multipartFile.getSize() <= maxUploadSize) {
            try {
                ObjectMetadata metadata = new ObjectMetadata();
                metadata.setContentType(multipartFile.getContentType());
                metadata.setContentLength(multipartFile.getSize());

                PutObjectRequest request = new PutObjectRequest(buckName, multipartFile.getOriginalFilename(), multipartFile.getInputStream(), metadata);
                s3client.putObject(request);
                return ResponseEntity.ok().build();
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).contentType(MediaType.TEXT_PLAIN).body(e.getMessage());
            }
        } else {
            return ResponseEntity.status(HttpServletResponse.SC_NOT_ACCEPTABLE).contentType(MediaType.TEXT_PLAIN).body("Exceeded the space limit, please contact the administrator to upgrade");
        }
    }

    @Override
    public ResponseEntity<Object> removeCompanyDocumentByKey(String fileName) {
        AmazonS3 s3client = s3Utils.getS3client();
        String buckName = s3Utils.getTenantBuckName();
        try {
            s3client.deleteObject(new DeleteObjectRequest(buckName, fileName));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).contentType(MediaType.TEXT_PLAIN).body(e.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    @Async("asyncExecutor")
    @Override
    public void dumpDocuments(MultipartHttpServletRequest request) {
        AmazonS3 s3client = s3Utils.getS3client();
        String buckName = s3Utils.getDumpBuckName();
        if (!StringUtils.hasText(buckName)) {
            return;
        }
        String dateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd_HHmmss"));
        request.getFileMap().values().forEach(multipartFile -> {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(multipartFile.getContentType());
            metadata.setContentLength(multipartFile.getSize());
            String fullFileName = multipartFile.getOriginalFilename();
            if (!StringUtils.hasText(fullFileName)) return;
            int dotIndex = fullFileName.lastIndexOf(".");
            String fileName = fullFileName.substring(0, dotIndex);
            String extension = fullFileName.substring(dotIndex);
            PutObjectRequest request1 = null;
            try {
                request1 = new PutObjectRequest(buckName, String.format("%s_%s%s",fileName, dateTime, extension), multipartFile.getInputStream(), metadata);
            } catch (IOException e) {
                e.printStackTrace();
            }
            s3client.putObject(request1);
        });
    }
}

package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.EmployeeView;
import com.timaven.timekeeping.service.ExcelService;
import com.timaven.timekeeping.service.ReportEmployeeService;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class ReporEmployeeServiceImpl implements ReportEmployeeService {

    private final ProviderAPI providerAPI;
    private final ExcelService excelService;

    @Autowired
    public ReporEmployeeServiceImpl(ProviderAPI providerAPI, ExcelService excelService) {
        this.providerAPI = providerAPI;
        this.excelService = excelService;
    }

    @Override
    public ResponseEntity<InputStreamResource> reportEmployee(Long projectId, Boolean showAll) {
        List<EmployeeView> employeeList = providerAPI.findEmployeeByProjectIdAndShowAll(projectId, showAll).block();
        try{
            XSSFWorkbook workbook = new XSSFWorkbook();
            Sheet sheetRoster = workbook.createSheet("Roster");
            Row headerRoster = sheetRoster.createRow(0);

            XSSFFont bodyFont = workbook.createFont();
            bodyFont.setFontName("Arial");
            bodyFont.setFontHeightInPoints((short) 10);

            XSSFCellStyle cellStyleMDYYYY = workbook.createCellStyle();
            XSSFDataFormat formatMDYYYY = workbook.createDataFormat();
            cellStyleMDYYYY.setDataFormat(formatMDYYYY.getFormat("m/d/yyyy"));
            cellStyleMDYYYY.setFont(bodyFont);

            XSSFCellStyle cellStyleHMM = workbook.createCellStyle();
            XSSFDataFormat formatHMM = workbook.createDataFormat();
            cellStyleHMM.setDataFormat(formatHMM.getFormat("h:mm"));
            cellStyleHMM.setFont(bodyFont);

            CellStyle style = workbook.createCellStyle();
            style.setFont(bodyFont);

            XSSFCellStyle headerStyle = workbook.createCellStyle();
            XSSFFont font = workbook.createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            font.setBold(true);
            headerStyle.setFont(font);

            headerStyle.setBorderTop(BorderStyle.MEDIUM);
            headerStyle.setBorderBottom(BorderStyle.MEDIUM);
            headerStyle.setBorderLeft(BorderStyle.MEDIUM);
            headerStyle.setBorderRight(BorderStyle.MEDIUM);

            int headCellIndexRoster = 0;
            Cell headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("BADGE #");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("EMPLOYEE");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("EMPLOYEE ID");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Team");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Crew");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Client Employee ID");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("COMPANY");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Craft Code");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Base ST");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Base OT");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Base DT");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Client Craft Code");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Per Diem");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Rig Pay");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("HIRE DATE");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("TERM DATE");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Shift");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Schedule Start");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Schedule End");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Lunch Start");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Lunch End");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("HL");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("SL");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("TV");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("VA");
            headerCellRoster.setCellStyle(headerStyle);

            headerCellRoster = headerRoster.createCell(headCellIndexRoster++);
            headerCellRoster.setCellValue("Sign In Sheet");
            headerCellRoster.setCellStyle(headerStyle);

            CellStyle numberStyle = workbook.createCellStyle();
            numberStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.##"));

            int j = 0;
            for (EmployeeView employeeView : employeeList) {

                Row row = sheetRoster.createRow(++j);

                int cellIndex = 0;

                Cell cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getBadge());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getFullName());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getEmpId());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                if (StringUtils.hasText(employeeView.getTeamName())) {
                    cell.setCellValue(employeeView.getTeamName());
                } else {
                    cell.setBlank();
                }
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                if (StringUtils.hasText(employeeView.getCrew())) {
                    cell.setCellValue(employeeView.getCrew());
                } else {
                    cell.setBlank();
                }
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getClientEmpId());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getCompany());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getCraft());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                if (employeeView.getBaseST() != null) {
                    cell.setCellValue(employeeView.getBaseST().doubleValue());
                } else {
                    cell.setBlank();
                }
                cell.setCellStyle(numberStyle);

                cell = row.createCell(cellIndex++);
                if (employeeView.getBaseOT() != null) {
                    cell.setCellValue(employeeView.getBaseOT().doubleValue());
                } else {
                    cell.setBlank();
                }
                cell.setCellStyle(numberStyle);

                cell = row.createCell(cellIndex++);
                if (employeeView.getBaseDT() != null) {
                    cell.setCellValue(employeeView.getBaseDT().doubleValue());
                } else {
                    cell.setBlank();
                }
                cell.setCellStyle(numberStyle);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getClientCraft());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getHasPerDiem() ? "T" : "");
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getHasRigPay() ? "T" : "");
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getHiredAt() == null ? "" :
                        employeeView.getHiredAt().format(DateTimeFormatter.ofPattern("M/d/yyyy")));
                cell.setCellStyle(cellStyleMDYYYY);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getTerminatedAt());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getShift());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getScheduleStart() == null ? "" :
                        employeeView.getScheduleStart().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
                cell.setCellStyle(cellStyleHMM);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getScheduleEnd() == null ? "" :
                        employeeView.getScheduleEnd().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
                cell.setCellStyle(cellStyleHMM);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getLunchStart() == null ? "" :
                        employeeView.getLunchStart().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
                cell.setCellStyle(cellStyleHMM);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(employeeView.getLunchEnd() == null ? "" :
                        employeeView.getLunchEnd().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
                cell.setCellStyle(cellStyleHMM);

                cell = row.createCell(cellIndex++);
                if (employeeView.getHolidayRate() != null) {
                    cell.setCellValue(employeeView.getHolidayRate().doubleValue());
                } else {
                    cell.setBlank();
                }
                cell.setCellStyle(numberStyle);

                cell = row.createCell(cellIndex++);
                if (employeeView.getSickLeaveRate() != null) {
                    cell.setCellValue(employeeView.getSickLeaveRate().doubleValue());
                } else {
                    cell.setBlank();
                }
                cell.setCellStyle(numberStyle);

                cell = row.createCell(cellIndex++);
                if (employeeView.getTravelRate() != null) {
                    cell.setCellValue(employeeView.getTravelRate().doubleValue());
                } else {
                    cell.setBlank();
                }
                cell.setCellStyle(numberStyle);

                cell = row.createCell(cellIndex++);
                if (employeeView.getVacationRate() != null) {
                    cell.setCellValue(employeeView.getVacationRate().doubleValue());
                } else {
                    cell.setBlank();
                }
                cell.setCellStyle(numberStyle);

                cell = row.createCell(cellIndex++);
                if (StringUtils.hasText(employeeView.getSignInSheet())) {
                    cell.setCellValue(employeeView.getSignInSheet());
                } else {
                    cell.setBlank();
                }
                cell.setCellStyle(style);
            }

            return excelService.downloadExcel( "Employee", workbook);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }
}

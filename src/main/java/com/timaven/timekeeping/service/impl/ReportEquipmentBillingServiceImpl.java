package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.enums.EquipmentAllocationSubmissionStatus;
import com.timaven.timekeeping.service.ExcelService;
import com.timaven.timekeeping.service.ReportEquipmentBillingService;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ReportEquipmentBillingServiceImpl implements ReportEquipmentBillingService {

    private final ProviderAPI providerAPI;
    private final ExcelService excelService;

    @Autowired
    public ReportEquipmentBillingServiceImpl(ProviderAPI providerAPI, ExcelService excelService) {
        this.providerAPI = providerAPI;
        this.excelService = excelService;
    }

    @Override
    public ResponseEntity<InputStreamResource> reportEquipemtBilling(Long projectId, LocalDate startDate, LocalDate endDate) {
        List<EquipmentAllocationTime> equipmentAllocationTimes = providerAPI
                .findEquipmentAllocationTimesByDateRange(projectId, startDate, endDate,
                        Set.of(EquipmentAllocationSubmissionStatus.APPROVED)).block();
        if (CollectionUtils.isEmpty(equipmentAllocationTimes)) {
            return excelService.downloadEmpty();
        }
        try{
            equipmentAllocationTimes = equipmentAllocationTimes.stream()
                    .sorted(Comparator.comparing((EquipmentAllocationTime t) -> t.getEquipmentAllocationSubmission().getDateOfService())
                            .thenComparing(EquipmentAllocationTime::getEquipmentId))
                    .collect(Collectors.toList());

            XSSFWorkbook workbook = new XSSFWorkbook();


            Sheet sheet = workbook.createSheet("data");

            int currentRowIndex = -1;
            int currentColumnIndex = -1;

            Row row = sheet.createRow(++currentRowIndex);

            XSSFCellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont headerFont = workbook.createFont();
            headerFont.setFontName("Arial");
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setBold(true);
            headerStyle.setFont(headerFont);

            Cell cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Work Date");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("NRC Badge Number");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Employee Number");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Employee Name");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Work Classification");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Work Authorization Number");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Activity Number");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("ST Hours");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("OT Hours");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Total Hours");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("ST Billing");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("OT Billing");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Total Billing");
            cell.setCellStyle(headerStyle);

            CellStyle numberStyle = workbook.createCellStyle();
            numberStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.##"));

            CellStyle dateStyle = workbook.createCellStyle();
            CreationHelper creationHelper = workbook.getCreationHelper();
            dateStyle.setDataFormat(creationHelper.createDataFormat().getFormat("m/dd/yyyy"));

            for (EquipmentAllocationTime allocationTime : equipmentAllocationTimes) {
                row = sheet.createRow(++currentRowIndex);
                currentColumnIndex = -1;
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(allocationTime.getEquipmentAllocationSubmission().getDateOfService());
                cell.setCellStyle(dateStyle);

                currentColumnIndex++;
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(allocationTime.getEquipmentId());

                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(allocationTime.getDescription());

                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(allocationTime.getEquipmentClass());

                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(allocationTime.getSerialNumber());

                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(String.valueOf(allocationTime.getId()));

                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(allocationTime.getTotalHour().doubleValue());
                cell.setCellStyle(numberStyle);

                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(BigDecimal.ZERO.doubleValue());
                cell.setCellStyle(numberStyle);

                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(allocationTime.getTotalHour().doubleValue());
                cell.setCellStyle(numberStyle);

                BigDecimal stBilling = allocationTime.getTotalCharge();

                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(stBilling.doubleValue());
                cell.setCellStyle(numberStyle);

                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(BigDecimal.ZERO.doubleValue());
                cell.setCellStyle(numberStyle);

                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(stBilling.doubleValue());
                cell.setCellStyle(numberStyle);
            }
            return excelService.downloadExcel( "Equipment Billing", workbook);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }
}

package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.service.ExcelService;
import com.timaven.timekeeping.service.ReportPurchaseOrderService;
import com.timaven.timekeeping.util.NumberUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class ReportPurchaseOrderServiceImpl implements ReportPurchaseOrderService {

    private final ProviderAPI providerAPI;
    private final ExcelService excelService;

    @Autowired
    public ReportPurchaseOrderServiceImpl(ProviderAPI providerAPI, ExcelService excelService) {
        this.providerAPI = providerAPI;
        this.excelService = excelService;
    }

    @Override
    public ResponseEntity<InputStreamResource> reportPurchaseOrderBillings(Long id) {
        PurchaseOrderBilling purchaseOrderBilling = providerAPI.findPurchaseOrderBillingByIdFetchingDetails(id).block();
        if (null == purchaseOrderBilling) {
            return excelService.downloadEmpty();
        }
        try{
            XSSFWorkbook workbook = new XSSFWorkbook();

            XSSFCellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont headerFont = workbook.createFont();
            headerFont.setFontName("Arial");
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setBold(true);
            headerStyle.setFont(headerFont);

            Sheet sheet = workbook.createSheet("data");

            int colIndex = 0;
            int rowIndex = 0;

            Row row = sheet.createRow(rowIndex++);
            Cell cell = row.createCell(colIndex++);
            cell.setCellValue("Bill #");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(colIndex++);
            if (StringUtils.hasText(purchaseOrderBilling.getBillingNumber())) {
                cell.setCellValue(purchaseOrderBilling.getBillingNumber());
            } else {
                cell.setBlank();
            }

            colIndex = 0;
            row = sheet.createRow(rowIndex++);
            cell = row.createCell(colIndex++);
            cell.setCellValue("Vendor Invoice");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(colIndex++);
            if (StringUtils.hasText(purchaseOrderBilling.getInvoiceNumber())) {
                cell.setCellValue(purchaseOrderBilling.getInvoiceNumber());
            } else {
                cell.setBlank();
            }

            colIndex = 0;
            row = sheet.createRow(rowIndex++);
            cell = row.createCell(colIndex++);

            BigDecimal total = BigDecimal.ZERO;

            int lastColIndex = 1;
            BigDecimal totalTax = BigDecimal.ZERO;
            if (!CollectionUtils.isEmpty(purchaseOrderBilling.getPurchaseOrderBillingDetails())) {
                cell.setCellValue("Description");
                cell.setCellStyle(headerStyle);

                cell = row.createCell(colIndex++);
                cell.setCellValue("Quantity");
                cell.setCellStyle(headerStyle);

                cell = row.createCell(colIndex++);
                cell.setCellValue("Price");
                cell.setCellStyle(headerStyle);

                cell = row.createCell(colIndex++);
                cell.setCellValue("Amount");
                cell.setCellStyle(headerStyle);

                cell = row.createCell(colIndex++);
                cell.setCellValue("Tax");
                cell.setCellStyle(headerStyle);

                // TM - 277
                lastColIndex = colIndex;
                cell = row.createCell(colIndex++);
                cell.setCellValue("Total");
                cell.setCellStyle(headerStyle);

                for (PurchaseOrderBillingDetail detail : purchaseOrderBilling.getPurchaseOrderBillingDetails()) {
                    colIndex = 0;
                    row = sheet.createRow(rowIndex++);
                    cell = row.createCell(colIndex++);
                    if (StringUtils.hasText(detail.getPart())) {
                        cell.setCellValue(detail.getPart());
                    } else {
                        cell.setBlank();
                    }

                    cell = row.createCell(colIndex++);
                    if (detail.getBillingQuantity() != null) {
                        cell.setCellValue(detail.getBillingQuantity().doubleValue());
                    }

                    BigDecimal unitCost = detail.getUnitCost() == null ? BigDecimal.ZERO : detail.getUnitCost();
                    BigDecimal markupAmount = detail.getMarkupAmount() == null ? BigDecimal.ZERO : detail.getMarkupAmount();
                    BigDecimal price = unitCost.multiply(markupAmount.divide(BigDecimal.valueOf(100), 2, RoundingMode.DOWN).add(BigDecimal.ONE));
                    cell = row.createCell(colIndex++);
                    if (detail.getUnitCost() != null) {
                        cell.setCellValue(NumberUtil.currencyFormat(price));
                    }

                    cell = row.createCell(colIndex++);

                    BigDecimal markupQuantity = detail.getMarkupQuantity() == null ? BigDecimal.ZERO : detail.getMarkupQuantity();
                    BigDecimal billingQuantity = detail.getBillingQuantity() == null ? BigDecimal.ZERO : detail.getBillingQuantity();
                    BigDecimal taxRate = detail.getTax() == null ? BigDecimal.ZERO : detail.getTax();
                    BigDecimal amount = markupQuantity.multiply(price).add(price.multiply(billingQuantity.subtract(markupQuantity)));

                    cell.setCellValue(NumberUtil.currencyFormat(amount));

                    cell = row.createCell(colIndex++);
                    BigDecimal tax = amount.multiply(taxRate.divide(BigDecimal.valueOf(100), 2, RoundingMode.DOWN));
                    cell.setCellValue(NumberUtil.currencyFormat(tax));
                    total = total.add(amount).add(tax);
                    totalTax = totalTax.add(tax);

                    // TM - 277
                    cell = row.createCell(colIndex++);
                    BigDecimal totalDescription = amount.add(tax);
                    cell.setCellValue(NumberUtil.currencyFormat(totalDescription));
                }
            }

            colIndex = 0;
            row = sheet.createRow(rowIndex++);
            cell = row.createCell(colIndex++);
            cell.setCellValue("Tax");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(lastColIndex);
            cell.setCellValue(NumberUtil.currencyFormat(totalTax));

            colIndex = 0;
            row = sheet.createRow(rowIndex++);
            cell = row.createCell(colIndex++);
            cell.setCellValue("Freight");
            cell.setCellStyle(headerStyle);

            BigDecimal freightShipping = purchaseOrderBilling.getFreightShipping() == null ? BigDecimal.ZERO : purchaseOrderBilling.getFreightShipping();

            cell = row.createCell(lastColIndex);
            cell.setCellValue(NumberUtil.currencyFormat(freightShipping));

            total = total.add(freightShipping);

            if (StringUtils.hasText(purchaseOrderBilling.getOtherCharge())) {
                colIndex = 0;
                row = sheet.createRow(rowIndex++);
                cell = row.createCell(colIndex++);
                cell.setCellValue(purchaseOrderBilling.getOtherCharge());
                cell.setCellStyle(headerStyle);

                BigDecimal otherAmount = purchaseOrderBilling.getOtherChargeAmount() == null ? BigDecimal.ZERO : purchaseOrderBilling.getOtherChargeAmount();

                cell = row.createCell(lastColIndex);
                cell.setCellValue(NumberUtil.currencyFormat(otherAmount));
                total = total.add(otherAmount);
            }

            colIndex = 0;
            row = sheet.createRow(rowIndex++);
            cell = row.createCell(colIndex++);
            cell.setCellValue("Total");
            cell.setCellStyle(headerStyle);

            cell = row.createCell(lastColIndex);
            cell.setCellValue(NumberUtil.currencyFormat(total));
            return excelService.downloadExcel(String.format("PO Billing report for %s", purchaseOrderBilling.getBillingNumber()), workbook);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }
}

package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.model.dto.WorkbookInfo;
import com.timaven.timekeeping.service.ExcelService;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class ExcelServiceImpl implements ExcelService {

    @Override
    public ResponseEntity<InputStreamResource> downloadExcel(String fname, Workbook workbook) {
        return downloadExcel(fname, workbook, 0);
    }

    @Override
    public ResponseEntity<InputStreamResource> downloadExcel(String fname, Workbook workbook, int firstRowNum) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            autoSizeColumns(workbook, firstRowNum);
            workbook.write(outputStream);
            workbook.close();
            InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
            headers.add("Content-Disposition", String.format("attachment; filename=\"%s.xlsx\"", fname));
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(new InputStreamResource(inputStream));
        } catch (IOException ignore) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }

    @Override
    public ResponseEntity<InputStreamResource> downloadEmpty() {
        return ResponseEntity.status(HttpServletResponse.SC_NO_CONTENT).build();
    }

    @Override
    public ResponseEntity<InputStreamResource> downloadZip(String zipFileName, List<WorkbookInfo> workbookInfoList) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try (ZipOutputStream zippedOut = new ZipOutputStream(outputStream)) {
            for (WorkbookInfo workbookInfo : workbookInfoList) {
                Workbook workbook = workbookInfo.getWorkbook();
                autoSizeColumns(workbook, workbookInfo.getFirstRowNum());
                ZipEntry e = new ZipEntry(String.format("%s.xlsx", workbookInfo.getFileName()));
                zippedOut.putNextEntry(e);

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                workbook.write(bos);
                bos.writeTo(zippedOut);
                bos.close();
                workbook.close();
                zippedOut.closeEntry();
            }
            zippedOut.finish();
            InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);
            headers.add("Content-Disposition", String.format("attachment; filename=\"%s.xlsx\"", zipFileName));
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(new InputStreamResource(inputStream));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }

    private void autoSizeColumns(Workbook workbook, int rowNumber) {
        if (workbook == null) {
            return;
        }
        int numberOfSheets = workbook.getNumberOfSheets();
        for (int i = 0; i < numberOfSheets; i++) {
            Sheet sheet = workbook.getSheetAt(i);
            if (sheet.getPhysicalNumberOfRows() > 0) {
                Row row;
                if (rowNumber == 0) {
                    row = sheet.getRow(sheet.getFirstRowNum());
                } else {
                    row = sheet.getRow(rowNumber);
                }
                int maxColumnIndex = row.getLastCellNum();
                if (sheet.getNumMergedRegions() > 0 && sheet.getMergedRegion(0).getFirstRow() == 0) {
                    maxColumnIndex = Math.max(maxColumnIndex, sheet.getMergedRegion(0).getLastColumn());
                }
                for (int j = 0; j <= maxColumnIndex; j++) {
                    sheet.autoSizeColumn(j);
                }
            }
        }
    }
}

package com.timaven.timekeeping.service.impl;

import be.quodlibet.boxable.BaseTable;
import be.quodlibet.boxable.HorizontalAlignment;
import be.quodlibet.boxable.Row;
import be.quodlibet.boxable.VerticalAlignment;
import be.quodlibet.boxable.image.Image;
import be.quodlibet.boxable.line.LineStyle;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.configuration.tenant.TenantProperties;
import com.timaven.timekeeping.configuration.web.ThreadTenantStorage;
import com.timaven.timekeeping.model.dto.CellAttributes;
import com.timaven.timekeeping.model.dto.TenantDto;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static com.timaven.timekeeping.util.PdfUtility.generateCells;
import static com.timaven.timekeeping.util.PdfUtility.hex2Rgb;

public abstract class PdfBaseService {

    protected final ProviderAPI providerAPI;

    @Value("${aws.s3.access-key}")
    protected String s3_access_key;

    @Value("${aws.s3.secret-key}")
    protected String s3_secret_key;

    @Value("${aws.s3.buck-name}")
    protected String buck_name;

    protected final TenantProperties tenantProperties;

    protected PdfBaseService(ProviderAPI providerAPI, TenantProperties tenantProperties) {
        this.providerAPI = providerAPI;
        this.tenantProperties = tenantProperties;
    }

    protected float addSignature(float yPosition, float yStartNewPage, float tableWidth, float leftMargin, PDDocument document, PDPage page) {
        try {
            BaseTable table = new BaseTable(yPosition, yStartNewPage, 30f, tableWidth, leftMargin,
                    document, page, true, true);

            LineStyle noLine = new LineStyle(Color.white, 0);
            LineStyle blackLine = new LineStyle(Color.black, 1);
            Row<PDPage> row = table.createRow(30f);
            //{36, 146, 362, 416, 488, 596}
            generateCells(row, new int[]{36, 136, 286, 316, 416, 566, 596},
                    new CellAttributes[]{
                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                    .value("Contractor:")
                                    .textColor(Color.black)
                                    .fontSize(12f)
                                    .borderStyle(noLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                    .value("")
                                    .topBorderStyle(noLine)
                                    .leftBorderStyle(noLine)
                                    .rightBorderStyle(noLine)
                                    .bottomBorderStyle(blackLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                    .value("")
                                    .borderStyle(noLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                    .value("Client:")
                                    .fontSize(12f)
                                    .textColor(Color.black)
                                    .borderStyle(noLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                    .value("")
                                    .topBorderStyle(noLine)
                                    .leftBorderStyle(noLine)
                                    .rightBorderStyle(noLine)
                                    .bottomBorderStyle(blackLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                    .value("")
                                    .borderStyle(noLine)
                                    .build(),
                    });
            row = table.createRow(30f);
            generateCells(row, new int[]{36, 136, 286, 316, 416, 566, 596},
                    new CellAttributes[]{
                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                    .value("Signature:")
                                    .textColor(Color.black)
                                    .fontSize(12f)
                                    .borderStyle(noLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                    .value("")
                                    .topBorderStyle(noLine)
                                    .leftBorderStyle(noLine)
                                    .rightBorderStyle(noLine)
                                    .bottomBorderStyle(blackLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                    .value("")
                                    .borderStyle(noLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                    .value("Signature:")
                                    .textColor(Color.black)
                                    .fontSize(12f)
                                    .borderStyle(noLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                    .value("")
                                    .topBorderStyle(noLine)
                                    .leftBorderStyle(noLine)
                                    .rightBorderStyle(noLine)
                                    .bottomBorderStyle(blackLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                    .value("")
                                    .borderStyle(noLine)
                                    .build(),
                    });
            row = table.createRow(30f);
            generateCells(row, new int[]{36, 136, 286, 316, 416, 566, 596},
                    new CellAttributes[]{
                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                    .value("Date:")
                                    .textColor(Color.black)
                                    .borderStyle(noLine)
                                    .fontSize(12f)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                    .value("")
                                    .topBorderStyle(noLine)
                                    .leftBorderStyle(noLine)
                                    .rightBorderStyle(noLine)
                                    .bottomBorderStyle(blackLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                    .value("")
                                    .borderStyle(noLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                    .value("Date:")
                                    .textColor(Color.black)
                                    .borderStyle(noLine)
                                    .fontSize(12f)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                    .value("")
                                    .topBorderStyle(noLine)
                                    .leftBorderStyle(noLine)
                                    .rightBorderStyle(noLine)
                                    .bottomBorderStyle(blackLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.CENTER, VerticalAlignment.MIDDLE)
                                    .value("")
                                    .borderStyle(noLine)
                                    .build(),
                    });
            return table.draw();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return yPosition;
    }

    protected float drawHeader1(float yPosition, float tableWidth, float leftMargin, PDDocument document, PDPage page, String criteria, String title, String formDate, String toDate, String shift, String job) {
        AWSCredentials credentials = new BasicAWSCredentials(s3_access_key, s3_secret_key);
        AmazonS3 s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.US_EAST_2)
                .build();
        String s3LogoKey = tenantProperties.getTenants().getOrDefault(ThreadTenantStorage.getTenantId(), new TenantDto()).getS3Logo();

        be.quodlibet.boxable.image.Image image = null;
        if (StringUtils.hasText(s3LogoKey)) {
            try {
                image = new Image(ImageIO.read(s3client.getObject(buck_name, s3LogoKey).getObjectContent()));
                float imageWidth = 150f;
                image = image.scaleByWidth(imageWidth);
            } catch (Exception ignore) {
            }
        }
        try {
            BaseTable table = new BaseTable(yPosition, yPosition, 0f, tableWidth, leftMargin,
                    document, page, false, true);

            // set default line spacing for entire table
            table.setLineSpacing(1f);

            Row<PDPage> row = table.createRow(54f);

            generateCells(row, new int[]{36, 610, 756},
                    new CellAttributes[]{
                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE)
                                    .value(title)
                                    .textColor(hex2Rgb("#c55910"))
                                    .font(PDType1Font.HELVETICA_BOLD)
                                    .fontSize(25f)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.MIDDLE)
                                    .image(image)
                                    .build(),
                    });

            yPosition = table.draw();

            yPosition -= 10;

            table = new BaseTable(yPosition, yPosition, 0f, tableWidth, leftMargin,
                    document, page, true, true);

            row = table.createRow(20f);

            LineStyle greyLine = new LineStyle(hex2Rgb("#767070"), 0);

            generateCells(row, new int[]{20, 76, 168, 220, 308, 350, 436, 460, 490, 514, 590},
                    new CellAttributes[]{
                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.MIDDLE)
                                    .value("Report Criteria:")
                                    .fillColor(hex2Rgb("#767070"))
                                    .textColor(Color.white)
                                    .font(PDType1Font.HELVETICA_BOLD)
                                    .borderStyle(greyLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE)
                                    .value(StringUtils.hasText(criteria) ? criteria : "All")
                                    .fillColor(hex2Rgb("#767070"))
                                    .textColor(Color.white)
                                    .font(PDType1Font.HELVETICA_BOLD)
                                    .borderStyle(greyLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.MIDDLE)
                                    .value("From Date:")
                                    .fillColor(hex2Rgb("#767070"))
                                    .textColor(Color.white)
                                    .font(PDType1Font.HELVETICA_BOLD)
                                    .fontSize(8f)
                                    .leftPadding(0f)
                                    .rightPadding(0f)
                                    .borderStyle(greyLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE)
                                    .value(formDate)
                                    .fillColor(hex2Rgb("#767070"))
                                    .textColor(Color.white)
                                    .font(PDType1Font.HELVETICA_BOLD)
                                    .borderStyle(greyLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.MIDDLE)
                                    .value("To Date:")
                                    .fillColor(hex2Rgb("#767070"))
                                    .textColor(Color.white)
                                    .font(PDType1Font.HELVETICA_BOLD)
                                    .fontSize(8f)
                                    .leftPadding(0f)
                                    .rightPadding(0f)
                                    .borderStyle(greyLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE)
                                    .value(toDate)
                                    .fillColor(hex2Rgb("#767070"))
                                    .textColor(Color.white)
                                    .font(PDType1Font.HELVETICA_BOLD)
                                    .borderStyle(greyLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.MIDDLE)
                                    .value("Shift:")
                                    .fillColor(hex2Rgb("#767070"))
                                    .textColor(Color.white)
                                    .font(PDType1Font.HELVETICA_BOLD)
                                    .fontSize(8f)
                                    .leftPadding(0f)
                                    .rightPadding(0f)
                                    .borderStyle(greyLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE)
                                    .value(StringUtils.hasText(shift) ? shift : "All")
                                    .fillColor(hex2Rgb("#767070"))
                                    .textColor(Color.white)
                                    .font(PDType1Font.HELVETICA_BOLD)
                                    .borderStyle(greyLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.MIDDLE)
                                    .value("Job:")
                                    .fillColor(hex2Rgb("#767070"))
                                    .textColor(Color.white)
                                    .font(PDType1Font.HELVETICA_BOLD)
                                    .fontSize(8f)
                                    .leftPadding(0f)
                                    .rightPadding(0f)
                                    .borderStyle(greyLine)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.MIDDLE)
                                    .value(job)
                                    .fillColor(hex2Rgb("#767070"))
                                    .textColor(Color.white)
                                    .font(PDType1Font.HELVETICA_BOLD)
                                    .borderStyle(greyLine)
                                    .build(),
                    });
            return table.draw();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return yPosition;
    }

    protected void addFooter(PDDocument document) throws IOException {
        // get all number of pages.

        int numberOfPages = document.getNumberOfPages();
        if (numberOfPages <= 0) return;
        PDPage firstPage = document.getPage(0);
        float width = firstPage.getMediaBox().getWidth();
        float leftMargin = 36f;
        float rightMargin = 16f;
        // we want table across whole page width (subtracted by left and right margin of course)
        float tableWidth = width - (rightMargin + leftMargin);

        // y position is your coordinate of top left corner of the table
        float yPosition = 25f;
        Instant instant = Instant.now();
        ZonedDateTime zdt = instant.atZone(ZoneId.of("America/Chicago"));

        for (int i = 0; i < numberOfPages; i++) {
            PDPage fpage = document.getPage(i);

            BaseTable table = new BaseTable(yPosition, yPosition, 0f, tableWidth, leftMargin, document, fpage, false, true);

            Row<PDPage> row = table.createRow(12f);
            generateCells(row, new int[]{36, 394, 752},
                    new CellAttributes[]{
                            new CellAttributes.CellBuilder(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
                                    .value(DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm").format(zdt))
                                    .fontSize(10f)
                                    .textColor(Color.gray)
                                    .build(),
                            new CellAttributes.CellBuilder(HorizontalAlignment.RIGHT, VerticalAlignment.BOTTOM)
                                    .value(String.format("Page %d", (i + 1)))
                                    .fontSize(10f)
                                    .textColor(Color.gray)
                                    .build(),
                    });

            table.draw();
        }
    }
}

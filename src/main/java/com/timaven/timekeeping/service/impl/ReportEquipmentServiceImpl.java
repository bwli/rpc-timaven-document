package com.timaven.timekeeping.service.impl;

import com.timaven.timekeeping.api.provider.ProviderAPI;
import com.timaven.timekeeping.configuration.tenant.TenantProperties;
import com.timaven.timekeeping.configuration.web.ThreadTenantStorage;
import com.timaven.timekeeping.model.*;
import com.timaven.timekeeping.model.dto.TenantDto;
import com.timaven.timekeeping.model.enums.EquipmentAllocationSubmissionStatus;
import com.timaven.timekeeping.model.enums.EquipmentOwnershipType;
import com.timaven.timekeeping.service.ExcelService;
import com.timaven.timekeeping.service.ReportEquipmentService;
import com.timaven.timekeeping.util.LocalDateUtil;
import com.timaven.timekeeping.util.NumberUtil;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ReportEquipmentServiceImpl implements ReportEquipmentService {

    private final ProviderAPI providerAPI;
    private final ExcelService excelService;
    private final TenantProperties tenantProperties;

    @Autowired
    public ReportEquipmentServiceImpl(ProviderAPI providerAPI, ExcelService excelService, TenantProperties tenantProperties) {
        this.providerAPI = providerAPI;
        this.excelService = excelService;
        this.tenantProperties = tenantProperties;
    }

    @Override
    public ResponseEntity<InputStreamResource> reportEquipemtByEquipmentOwnershipType(Long projectId, LocalDate dateOfService, LocalDate payrollDate, Integer type) {
        EquipmentOwnershipType equipmentOwnershipType = EquipmentOwnershipType.values()[type == null ? 0 : type];
        List<EquipmentAllocationTime> equipmentAllocationTimes = providerAPI
                .findEquipmentAllocationTimesByDate(projectId, dateOfService, payrollDate,
                        EquipmentAllocationSubmissionStatus.APPROVED, equipmentOwnershipType).block();

        if (CollectionUtils.isEmpty(equipmentAllocationTimes)) {
            return excelService.downloadEmpty();
        }
        try{
            Set<String> costCodeSet = equipmentAllocationTimes.stream()
                    .map(EquipmentAllocationTime::getEquipmentCostCodePercs)
                    .flatMap(Collection::stream)
                    .map(EquipmentCostCodePerc::getCostCodeFull)
                    .collect(Collectors.toSet());

            Set<EquipmentAllocationSubmission> equipmentAllocationSubmissions = equipmentAllocationTimes.stream()
                    .map(EquipmentAllocationTime::getEquipmentAllocationSubmission)
                    .collect(Collectors.toSet());

            Set<CostCodeAssociation> associations = new HashSet<>();
            if (!CollectionUtils.isEmpty(costCodeSet)) {
                associations = providerAPI.findCostCodeAssociationsByProjectIdAndCodes(projectId == null ? 0L : projectId, dateOfService, costCodeSet).block();
            }

            Map<String, CostCodeAssociation> associationMap = associations.stream()
                    .collect(Collectors.toMap(CostCodeAssociation::getCostCode, Function.identity()));

            List<String> costCodes = costCodeSet.stream()
                    .sorted().collect(Collectors.toList());

            // 8 fix column and dynamic cost code
            int costCodeSize = costCodes.size();

            XSSFWorkbook workbook = new XSSFWorkbook();

            final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

            Sheet sheet = workbook.createSheet("data");
            sheet.setDisplayGridlines(false);

            int totalColumns = 8 + costCodeSize;
            int currentRowIndex = -1;
            int currentColumnIndex = -1;

            Row row = sheet.createRow(++currentRowIndex);

            XSSFCellStyle headerStyle = workbook.createCellStyle();
            headerStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont headerFont = workbook.createFont();
            headerFont.setFontName("Arial");
            headerFont.setFontHeightInPoints((short) 14);
            headerFont.setBold(true);
            headerStyle.setFont(headerFont);

            Cell cell = row.createCell(++currentColumnIndex);
            sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, totalColumns - 1));
            cell.setCellValue(tenantProperties.getTenants().getOrDefault(ThreadTenantStorage.getTenantId(), new TenantDto()).getCompanyName());
            cell.setCellStyle(headerStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            cell = row.createCell(++currentColumnIndex);
            sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, totalColumns - 1));

            XSSFCellStyle rightStyle = workbook.createCellStyle();
            rightStyle.setAlignment(HorizontalAlignment.RIGHT);
            XSSFCellStyle centerStyle = workbook.createCellStyle();
            centerStyle.setAlignment(HorizontalAlignment.CENTER);
            cell.setCellValue(tenantProperties.getTenants().getOrDefault(ThreadTenantStorage.getTenantId(), new TenantDto()).getCompanyAddress());
            cell.setCellStyle(centerStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            cell = row.createCell(++currentColumnIndex);
            sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, totalColumns - 1));
            XSSFCellStyle italicStyle = workbook.createCellStyle();
            italicStyle.setAlignment(HorizontalAlignment.CENTER);
            XSSFFont font = workbook.createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 12);
            font.setItalic(true);
            italicStyle.setFont(font);
            cell.setCellValue("Time Sheet");
            cell.setCellStyle(italicStyle);

            XSSFCellStyle boldStyle = workbook.createCellStyle();
            XSSFFont boldFont = workbook.createFont();
            boldFont.setBold(true);
            boldStyle.setFont(boldFont);

            XSSFCellStyle boldRightStyle = workbook.createCellStyle();
            boldRightStyle.setAlignment(HorizontalAlignment.RIGHT);
            boldRightStyle.setFont(boldFont);
            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Time Sheet Name");
            cell.setCellStyle(boldRightStyle);

            String timeSheetNumber = equipmentAllocationSubmissions.stream()
                    .map(EquipmentAllocationSubmission::getId)
                    .map(String::valueOf)
                    .collect(Collectors.joining(", "));
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(timeSheetNumber);

            sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, currentColumnIndex + 1 + costCodeSize));

            currentColumnIndex += (2 + costCodeSize);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Date - Shift");
            cell.setCellStyle(boldRightStyle);

            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(String.format("%s [%s] - ALL", dateFormatter.format(dateOfService),
                    LocalDateUtil.getDayOfWeekName(dateOfService)));

            Project project = providerAPI.findProjectById(projectId).block();

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Job");
            cell.setCellStyle(boldRightStyle);

            String jobNumber = project.getJobNumber() == null ? "" : project.getJobNumber();
            String subJob = project.getSubJob() == null ? "" : project.getSubJob();
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(String.format("%s - %s", jobNumber, subJob));

            sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, currentColumnIndex + 1 + costCodeSize));

            currentColumnIndex += (2 + costCodeSize);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Foreman");
            cell.setCellStyle(boldRightStyle);

            Set<Long> userIds = equipmentAllocationSubmissions.stream()
                    .map(EquipmentAllocationSubmission::getApproveUserId).collect(Collectors.toSet());
            List<User> users = providerAPI.findUsersById(userIds).block();
            String usernames = users.stream().map(User::getDisplayName)
                    .collect(Collectors.joining(", "));
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(usernames);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Notes");
            cell.setCellStyle(boldRightStyle);

            XSSFCellStyle codeTypeStyle = workbook.createCellStyle();
            codeTypeStyle.setAlignment(HorizontalAlignment.RIGHT);
            List<BillingCodeType> billingCodeTypes = providerAPI.findBillingCodeTypes(true).block();

            XSSFCellStyle blueBorderedStyle = workbook.createCellStyle();
            blueBorderedStyle.setFillForegroundColor(new XSSFColor(java.awt.Color.cyan, null));
            blueBorderedStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            blueBorderedStyle.setBorderBottom(BorderStyle.THIN);
            blueBorderedStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            blueBorderedStyle.setBorderLeft(BorderStyle.THIN);
            blueBorderedStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            blueBorderedStyle.setBorderRight(BorderStyle.THIN);
            blueBorderedStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            blueBorderedStyle.setBorderTop(BorderStyle.THIN);
            blueBorderedStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            XSSFCellStyle borderedRightStyle = workbook.createCellStyle();
            borderedRightStyle.setAlignment(HorizontalAlignment.RIGHT);
            borderedRightStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
            borderedRightStyle.setBorderBottom(BorderStyle.THIN);
            borderedRightStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            borderedRightStyle.setBorderLeft(BorderStyle.THIN);
            borderedRightStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            borderedRightStyle.setBorderRight(BorderStyle.THIN);
            borderedRightStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            borderedRightStyle.setBorderTop(BorderStyle.THIN);
            borderedRightStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            XSSFCellStyle borderedStyle = workbook.createCellStyle();
            borderedRightStyle.setBorderBottom(BorderStyle.THIN);
            borderedRightStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            borderedRightStyle.setBorderLeft(BorderStyle.THIN);
            borderedRightStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            borderedRightStyle.setBorderRight(BorderStyle.THIN);
            borderedRightStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            borderedRightStyle.setBorderTop(BorderStyle.THIN);
            borderedRightStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());

            for (int i = 0; i < billingCodeTypes.size(); i++) {
                row = sheet.createRow(++currentRowIndex);
                currentColumnIndex = 2;
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(billingCodeTypes.get(i).getCodeType());
                cell.setCellStyle(codeTypeStyle);

                boolean evenColumn = i % 2 == 0;

                for (String costCode : costCodes) {
                    CostCodeAssociation association = associationMap.getOrDefault(costCode, new CostCodeAssociation());
                    String levelX = association.getLevel(i + 1);
                    cell = row.createCell(++currentColumnIndex);
                    cell.setCellValue(levelX);
                    cell.setCellStyle(evenColumn ? blueBorderedStyle : borderedStyle);
                }
            }

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = 2;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Rep#");
            cell.setCellStyle(codeTypeStyle);

            boolean evenColumn = billingCodeTypes.size() % 2 == 0;

            for (String costCode : costCodes) {
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(costCode);
                cell.setCellStyle(evenColumn ? blueBorderedStyle : borderedStyle);
            }

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Number");
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Alias");

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Description");
            cell.setCellStyle(centerStyle);
            cell = row.createCell(totalColumns - 1);
            cell.setCellValue("Totals");
            cell.setCellStyle(rightStyle);

            Map<String, BigDecimal> costCodeStTotal = new HashMap<>();
            Map<String, BigDecimal> employeeStTotal = new HashMap<>();
            Map<String, BigDecimal> costCodeStBillingTotal = new HashMap<>();
            XSSFCellStyle pinkStyle = workbook.createCellStyle();
            pinkStyle.setFillForegroundColor(new XSSFColor(java.awt.Color.PINK, null));
            pinkStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            XSSFCellStyle greenBorderedRightStyle = workbook.createCellStyle();
            greenBorderedRightStyle.setAlignment(HorizontalAlignment.RIGHT);
            greenBorderedRightStyle.setFillForegroundColor(new XSSFColor(java.awt.Color.GREEN, null));
            greenBorderedRightStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            greenBorderedRightStyle.setBorderBottom(BorderStyle.THIN);
            greenBorderedRightStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            greenBorderedRightStyle.setBorderLeft(BorderStyle.THIN);
            greenBorderedRightStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            greenBorderedRightStyle.setBorderRight(BorderStyle.THIN);
            greenBorderedRightStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            greenBorderedRightStyle.setBorderTop(BorderStyle.THIN);
            greenBorderedRightStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            XSSFCellStyle yellowBorderedRightStyle = workbook.createCellStyle();
            yellowBorderedRightStyle.setAlignment(HorizontalAlignment.RIGHT);
            yellowBorderedRightStyle.setFillForegroundColor(new XSSFColor(java.awt.Color.YELLOW, null));
            yellowBorderedRightStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            yellowBorderedRightStyle.setBorderBottom(BorderStyle.THIN);
            yellowBorderedRightStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            yellowBorderedRightStyle.setBorderLeft(BorderStyle.THIN);
            yellowBorderedRightStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            yellowBorderedRightStyle.setBorderRight(BorderStyle.THIN);
            yellowBorderedRightStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
            yellowBorderedRightStyle.setBorderTop(BorderStyle.THIN);
            yellowBorderedRightStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
            for (EquipmentAllocationTime allocationTime : equipmentAllocationTimes) {
                row = sheet.createRow(++currentRowIndex);
                currentColumnIndex = -1;
                cell = row.createCell(++currentColumnIndex);
                sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, currentColumnIndex + 2));
                currentColumnIndex += 2;
                cell.setCellValue(allocationTime.getEquipmentId());
                cell.setCellStyle(pinkStyle);
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue("DT");
                cell.setCellStyle(greenBorderedRightStyle);
                Set<EquipmentCostCodePerc> costCodePercs = allocationTime.getEquipmentCostCodePercs();
                Map<String, EquipmentCostCodePerc> costCodePercMap = costCodePercs.stream()
                        .collect(Collectors.toMap(EquipmentCostCodePerc::getCostCodeFull, Function.identity()));

                for (int i = 0; i < costCodes.size(); i++) {
                    cell = row.createCell(++currentColumnIndex);
                    cell.setBlank();
                    cell.setCellStyle(greenBorderedRightStyle);
                }

                cell = row.createCell(++currentColumnIndex);
                sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, currentColumnIndex + 1));
                currentColumnIndex += 1;
                cell.setCellStyle(greenBorderedRightStyle);
                cell = row.createCell(++currentColumnIndex);
                cell.setCellStyle(greenBorderedRightStyle);
                cell = row.createCell(++currentColumnIndex);
                cell.setBlank();
                cell.setCellStyle(greenBorderedRightStyle);

                row = sheet.createRow(++currentRowIndex);
                currentColumnIndex = -1;
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(allocationTime.getDescription());

                currentColumnIndex += 2;
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue("OT");
                cell.setCellStyle(yellowBorderedRightStyle);

                for (int i = 0; i < costCodes.size(); i++) {
                    cell = row.createCell(++currentColumnIndex);
                    cell.setBlank();
                    cell.setCellStyle(greenBorderedRightStyle);
                }

                cell = row.createCell(++currentColumnIndex);
                sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, currentColumnIndex + 1));
                currentColumnIndex += 1;
                cell.setCellStyle(yellowBorderedRightStyle);
                cell = row.createCell(++currentColumnIndex);
                cell.setCellStyle(yellowBorderedRightStyle);
                cell = row.createCell(++currentColumnIndex);
                cell.setBlank();
                cell.setCellStyle(yellowBorderedRightStyle);

                row = sheet.createRow(++currentRowIndex);
                currentColumnIndex = -1;
                currentColumnIndex += 1;
                cell = row.createCell(++currentColumnIndex);
                String alias = allocationTime.getAlias();
                cell.setCellValue(alias == null ? "" : alias);

                currentColumnIndex += 1;
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue("ST");
                cell.setCellStyle(borderedRightStyle);

                BigDecimal stTotal = BigDecimal.ZERO;
                for (String costCode : costCodes) {
                    cell = row.createCell(++currentColumnIndex);

                    EquipmentCostCodePerc costCodePerc = costCodePercMap.getOrDefault(costCode, new EquipmentCostCodePerc());
                    BigDecimal hour = costCodePerc.getTotalHour();
                    stTotal = stTotal.add(hour);
                    cell.setCellValue(hour.compareTo(BigDecimal.ZERO) == 0 ? "" :
                            hour.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
                    cell.setCellStyle(borderedRightStyle);

                    BigDecimal codeTotal = costCodeStTotal.getOrDefault(costCode, BigDecimal.ZERO);
                    codeTotal = codeTotal.add(hour);
                    costCodeStTotal.put(costCode, codeTotal);

                    BigDecimal codeStBillingTotal = costCodeStBillingTotal.getOrDefault(costCode, BigDecimal.ZERO);
                    codeStBillingTotal = codeStBillingTotal.add(costCodePerc.getTotalCharge());
                    costCodeStBillingTotal.put(costCode, codeStBillingTotal);
                }
                cell = row.createCell(++currentColumnIndex);
                sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, currentColumnIndex + 1));
                currentColumnIndex += 1;
                cell.setCellStyle(borderedRightStyle);
                cell = row.createCell(++currentColumnIndex);
                cell.setCellStyle(borderedRightStyle);
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue(stTotal.compareTo(BigDecimal.ZERO) == 0 ? "" :
                        stTotal.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
                cell.setCellStyle(borderedRightStyle);

                BigDecimal tTotal = employeeStTotal.getOrDefault(allocationTime.getEquipmentId(), BigDecimal.ZERO);
                tTotal = tTotal.add(stTotal);
                employeeStTotal.put(allocationTime.getEquipmentId(), tTotal);
            }

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Hours Summary");
            cell.setCellStyle(boldRightStyle);
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Straightime:");
            cell.setCellStyle(rightStyle);
            for (String costCode : costCodes) {
                cell = row.createCell(++currentColumnIndex);

                BigDecimal codeTotal = costCodeStTotal.getOrDefault(costCode, BigDecimal.ZERO);

                cell.setCellValue(codeTotal.compareTo(BigDecimal.ZERO) == 0 ? "-0-"
                        : codeTotal.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
                cell.setCellStyle(rightStyle);
            }
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            BigDecimal totalStraightime = employeeStTotal.values().stream()
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            cell.setCellValue(totalStraightime.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
            cell.setCellStyle(rightStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 3;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Overtime:");
            cell.setCellStyle(rightStyle);
            for (int i = 0; i < costCodes.size(); i++) {
                cell.setBlank();
                cell.setCellStyle(rightStyle);
            }
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("0.00");
            cell.setCellStyle(rightStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 3;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Totals:");
            cell.setCellStyle(rightStyle);
            BigDecimal total = BigDecimal.ZERO;
            for (String costCode : costCodes) {
                cell = row.createCell(++currentColumnIndex);

                BigDecimal codeTotal = costCodeStTotal.getOrDefault(costCode, BigDecimal.ZERO);
                total = codeTotal.add(total);

                cell.setCellValue(codeTotal.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
                cell.setCellStyle(rightStyle);
            }
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("0.00");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("0.00");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(total.setScale(2, RoundingMode.HALF_DOWN).stripTrailingZeros().toPlainString());
            cell.setCellStyle(boldRightStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Billing Summary");
            cell.setCellStyle(boldRightStyle);
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Straightime:");
            cell.setCellStyle(rightStyle);
            for (String costCode : costCodes) {
                cell = row.createCell(++currentColumnIndex);
                BigDecimal codeTotal = costCodeStBillingTotal.getOrDefault(costCode, BigDecimal.ZERO);

                cell.setCellValue(codeTotal.compareTo(BigDecimal.ZERO) == 0 ? "-0-"
                        : NumberUtil.currencyFormat(codeTotal.setScale(2, RoundingMode.HALF_DOWN)));
                cell.setCellStyle(rightStyle);
            }
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            totalStraightime = costCodeStBillingTotal.values().stream()
                    .reduce(BigDecimal.ZERO, BigDecimal::add);
            cell.setCellValue(NumberUtil.currencyFormat(totalStraightime.setScale(2, RoundingMode.HALF_DOWN)));
            cell.setCellStyle(rightStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 3;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Overtime:");
            cell.setCellStyle(rightStyle);
            for (int i = 0; i < costCodes.size(); i++) {
                cell = row.createCell(++currentColumnIndex);
                cell.setCellValue("-0-");
                cell.setCellStyle(rightStyle);
            }
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("-0-");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(NumberUtil.currencyFormat(BigDecimal.ZERO));
            cell.setCellStyle(rightStyle);

            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 3;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Totals:");
            cell.setCellStyle(rightStyle);
            total = BigDecimal.ZERO;
            for (String costCode : costCodes) {
                cell = row.createCell(++currentColumnIndex);

                BigDecimal codeTotal = costCodeStBillingTotal.getOrDefault(costCode, BigDecimal.ZERO);
                total = codeTotal.add(total);

                cell.setCellValue(NumberUtil.currencyFormat(codeTotal.setScale(2, RoundingMode.HALF_DOWN)));
                cell.setCellStyle(rightStyle);
            }
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("0.00");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("0.00");
            cell.setCellStyle(rightStyle);
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue(NumberUtil.currencyFormat(total.setScale(2, RoundingMode.HALF_DOWN)));
            cell.setCellStyle(rightStyle);

            currentRowIndex += 1;
            row = sheet.createRow(++currentRowIndex);
            currentColumnIndex = -1;
            currentColumnIndex += 1;
            cell = row.createCell(++currentColumnIndex);

            cell.setCellValue("Contractor:");
            cell.setCellStyle(boldRightStyle);

            cell = row.createCell(++currentColumnIndex);
            sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, totalColumns / 2 - 1));
            cell.setCellValue("____________________________________________________");

            currentColumnIndex = totalColumns / 2 - 1;
            currentColumnIndex += 2;
            cell = row.createCell(++currentColumnIndex);
            cell.setCellValue("Client:");
            cell.setCellStyle(boldRightStyle);

            cell = row.createCell(++currentColumnIndex);
            sheet.addMergedRegion(new CellRangeAddress(currentRowIndex, currentRowIndex, currentColumnIndex, totalColumns - 1 ));
            cell.setCellValue("____________________________________________________");

            String fileName = equipmentOwnershipType == EquipmentOwnershipType.COMPANY_OWNED ? "Equipment Time Sheet" : "3rd Party Time Sheet";

            return excelService.downloadExcel(fileName, workbook);
        }catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }

    @Override
    public ResponseEntity<InputStreamResource> reportEquipemt(Boolean all) {
        List<Equipment> equipmentList = providerAPI.findEquipmentAllOrByActiveTrue(all).block();
        if (CollectionUtils.isEmpty(equipmentList)) {
            return excelService.downloadEmpty();
        }
        try{
            XSSFWorkbook workbook = new XSSFWorkbook();
            Sheet sheet = workbook.createSheet("Equipment");
            Row header = sheet.createRow(0);
            XSSFCellStyle headerStyle = workbook.createCellStyle();

            XSSFFont font = workbook.createFont();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            font.setBold(true);
            headerStyle.setFont(font);

            headerStyle.setBorderTop(BorderStyle.MEDIUM);
            headerStyle.setBorderBottom(BorderStyle.MEDIUM);
            headerStyle.setBorderLeft(BorderStyle.MEDIUM);
            headerStyle.setBorderRight(BorderStyle.MEDIUM);

            int headCellIndex = 0;
            Cell headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Equipment ID");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Serial Number (VIN)");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Description");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Alias");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Class");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex++);
            headerCell.setCellValue("Department");
            headerCell.setCellStyle(headerStyle);

            headerCell = header.createCell(headCellIndex);
            headerCell.setCellValue("Active");
            headerCell.setCellStyle(headerStyle);

            CellStyle style = workbook.createCellStyle();

            for (int i = 0; i < equipmentList.size(); i++) {
                Equipment equipment = equipmentList.get(i);

                Row row = sheet.createRow(i + 1);

                int cellIndex = 0;

                Cell cell = row.createCell(cellIndex++);
                cell.setCellValue(equipment.getEquipmentId());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(equipment.getSerialNumber());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(equipment.getDescription());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(equipment.getAlias());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(equipment.getEquipmentClass());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex++);
                cell.setCellValue(equipment.getDepartment());
                cell.setCellStyle(style);

                cell = row.createCell(cellIndex);
                cell.setCellValue(equipment.getActive() ? "True" : "False");
                cell.setCellStyle(style);
            }
            excelService.downloadExcel( "Equipment", workbook);
        } catch (Exception e){
            e.printStackTrace();
        }
        return ResponseEntity.status(HttpServletResponse.SC_BAD_REQUEST).build();
    }
}

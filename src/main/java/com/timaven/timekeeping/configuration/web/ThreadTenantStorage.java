package com.timaven.timekeeping.configuration.web;

import org.springframework.util.StringUtils;

public class ThreadTenantStorage {

    private static ThreadLocal<String> currentTenant = new ThreadLocal<>();

    public static void setTenantId(String tenantId) {
        currentTenant.set(tenantId);
    }

    public static String getTenantId() {
        String tenantId = currentTenant.get();
        return StringUtils.hasText(tenantId) ? tenantId : "tenant1";
    }

    public static void clear(){
        currentTenant.remove();
    }
}

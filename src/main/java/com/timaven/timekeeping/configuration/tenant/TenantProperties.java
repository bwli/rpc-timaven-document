package com.timaven.timekeeping.configuration.tenant;

import com.timaven.timekeeping.model.dto.TenantDto;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@ConfigurationProperties(prefix = "multi-tenants")
public class TenantProperties {

    private final Map<String, TenantDto> tenants = new LinkedHashMap<>();

    private Map<String, String> hosts = new HashMap<>();

    public void setTenants(Map<String, Map<String, String>> tenants) {
        tenants.forEach((key, value) -> this.tenants.put(key, parseTenant(key, value)));
        hosts = this.tenants.values().stream()
                .collect(Collectors.toMap(TenantDto::getHost, TenantDto::getName));
    }

    private TenantDto parseTenant(String key, Map<String, String> source) {
        TenantDto tenantDto = new TenantDto();
        tenantDto.setName(key);
        tenantDto.setHost(source.get("host"));
        tenantDto.setS3Logo(source.get("s3-logo"));
        tenantDto.setCompanyName(source.get("company-name"));
        tenantDto.setCompanyAddress(source.get("company-address"));
        tenantDto.setBuckName(source.get("buck-name"));
        tenantDto.setDumpBuckName(source.get("dump-buck-name"));
        long maxUploadedSize = 0;
        try {
            maxUploadedSize = Long.parseLong(source.get("max-upload-size"));
        } catch (NumberFormatException ignore) {
        }
        tenantDto.setMaxUploadSize(maxUploadedSize);
        return tenantDto;
    }

    public Map<String, String> getHosts() {
        return hosts;
    }

    public Map<String, TenantDto> getTenants() {
        return tenants;
    }
}

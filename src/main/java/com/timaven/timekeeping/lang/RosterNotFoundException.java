package com.timaven.timekeeping.lang;

public class RosterNotFoundException extends Exception {
    public RosterNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}

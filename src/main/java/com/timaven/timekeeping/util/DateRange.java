package com.timaven.timekeeping.util;

import java.time.LocalDate;
import java.util.*;

public class DateRange implements Iterable<LocalDate> {

    private final LocalDate startDate;
    private final LocalDate endDate;
    private final boolean excludeWeekEnd;

    public DateRange(LocalDate startDate, LocalDate endDate, boolean excludeWeekEnd) {
        //check that range is valid (null, start < end)
        this.startDate = startDate;
        this.endDate = endDate;
        this.excludeWeekEnd = excludeWeekEnd;
    }

    @Override
    public Iterator<LocalDate> iterator() {
        return toList().iterator();
    }

    public List<LocalDate> toList() { //could also be built from the stream() method
        List<LocalDate> dates = new ArrayList<>();
        for (LocalDate d = startDate; !d.isAfter(endDate); d = d.plusDays(1)) {
            if (!excludeWeekEnd || !LocalDateUtil.isWeekend(d)) {
                dates.add(d);
            }
        }
        return dates;
    }

    public Set<LocalDate> toSet() { //could also be built from the stream() method
        return new TreeSet<>(toList());
    }
}

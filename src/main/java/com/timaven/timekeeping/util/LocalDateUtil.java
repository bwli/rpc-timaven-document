package com.timaven.timekeeping.util;

import org.springframework.util.CollectionUtils;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class LocalDateUtil {
    public static boolean isWeekend(LocalDate localDate) {
        DayOfWeek dayOfWeek = localDate.getDayOfWeek();
        return dayOfWeek == DayOfWeek.SATURDAY || dayOfWeek == DayOfWeek.SUNDAY;
    }

    public static LocalDate getFirstValidDate(LocalDate starDate, LocalDate endDate, Collection<LocalDate> dateCollection) {
        if (null == starDate || null == endDate || CollectionUtils.isEmpty(dateCollection)) return null;
        List<LocalDate> dates = dateCollection.stream().sorted().collect(Collectors.toList());
        LocalDate date = dates.get(0);
        while (date.isBefore(endDate) || date.isEqual(endDate)) {
            if (!dates.contains(date)) return date;
            date = date.plusDays(1);
        }
        date = dates.get(0);
        while (date.isAfter(starDate) || date.isEqual(starDate)) {
            if (!dates.contains(date)) return date;
            date = date.minusDays(1);
        }
        return null;
    }

    public static String getDayOfWeekName(LocalDate date) {
        if (null == date) return "";
        return switch (date.getDayOfWeek()) {
            case MONDAY -> "Mon";
            case TUESDAY -> "Tue";
            case WEDNESDAY -> "Wed";
            case THURSDAY -> "Thu";
            case FRIDAY -> "Fri";
            case SATURDAY -> "Sat";
            case SUNDAY -> "Sun";
        };
    }
}

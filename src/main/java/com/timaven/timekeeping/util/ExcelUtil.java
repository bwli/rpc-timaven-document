package com.timaven.timekeeping.util;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

public class ExcelUtil {
    public static boolean isRowEmpty(Row row) {
        for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
            Cell cell = row.getCell(c);
            if (null == cell) {
                continue;
            }
            CellType cellType = cell.getCellType();
            if (cellType != CellType.BLANK) {
                if (cellType == CellType.FORMULA) {
                    switch (cell.getCachedFormulaResultType()) {
                        case NUMERIC:
                            return false;
                        case STRING:
                            String value =
                                    cell.getRichStringCellValue().toString();
                            if (StringUtils.hasText(value)) {
                                return false;
                            }
                            break;
                    }
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    public static String getStringValue(Cell cell) {
        if (cell == null) return "";
        if (CellType.NUMERIC == cell.getCellType()) {
            double value = cell.getNumericCellValue();
            if (value % 1 == 0) {
                // integer value
                return String.valueOf((int) cell.getNumericCellValue()).trim();
            }
            return String.valueOf(cell.getNumericCellValue()).trim();
        } else if (CellType.BOOLEAN == cell.getCellType()) {
            return String.valueOf(cell.getBooleanCellValue()).trim();
        }
        return cell.getStringCellValue().trim();
    }

    public static Double getDoubleValue(Cell cell) {
        if (cell == null) return null;
        if (CellType.STRING == cell.getCellType()) {
            String value = cell.getStringCellValue();
            if (!StringUtils.isEmpty(value)) {
                value = value.replace("$", "").trim();
                return Double.valueOf(value);
            }
        } else if (CellType.NUMERIC == cell.getCellType()) {
            return cell.getNumericCellValue();
        }
        return null;
    }

    public static Long getLongValue(Cell cell) {
        if (cell == null) return null;
        Integer integer = getIntValue(cell);
        return null == integer ? null : Long.valueOf(integer);
    }

    public static Integer getIntValue(Cell cell) {
        if (cell == null) return null;
        if (CellType.STRING == cell.getCellType()) {
            String value = cell.getStringCellValue();
            if (!StringUtils.isEmpty(value)) {
                return Double.valueOf(value).intValue();
            }
        } else if (CellType.NUMERIC == cell.getCellType()) {
            return (int) cell.getNumericCellValue();
        }
        return null;
    }

    public static Boolean getBooleanValue(Cell cell) {
        if (cell == null) return null;
        if (CellType.STRING == cell.getCellType()) {
            String value = cell.getStringCellValue();
            if ("Y".equalsIgnoreCase(value)) {
                return true;
            } else if ("N".equalsIgnoreCase(value)) {
                return false;
            } else {
                return null;
            }
        }
        if (CellType.BLANK == cell.getCellType()) {
            return null;
        }
        return cell.getBooleanCellValue();
    }

    public static LocalDate getLocalDate(Cell cell) {
        if (cell == null) return null;
        if (CellType.STRING == cell.getCellType()) {
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
            String strValue = cell.getStringCellValue();
            try {
                if (StringUtils.isEmpty(strValue)) return null;
                return LocalDate.parse(strValue, dateFormatter);
            } catch (DateTimeParseException e1) {
                dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                try {
                    return LocalDate.parse(strValue, dateFormatter);
                } catch (DateTimeParseException e2) {
                    dateFormatter = DateTimeFormatter.ofPattern("M/d/yyyy");
                    try {
                        return LocalDate.parse(strValue, dateFormatter);
                    } catch (DateTimeParseException e3) {
                        return null;
                    }
                }
            }
        } else if (CellType.NUMERIC == cell.getCellType()) {
            return convertToLocalDateViaInstant(cell.getDateCellValue());
        }
        return null;
    }

    private static LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        if (null == dateToConvert) {
            return null;
        }
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    public static LocalTime getLocalTime(Cell cell, String pattern) {
        if (cell == null) return null;
        if (CellType.STRING == cell.getCellType()) {
            DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(pattern);
            try {
                String strValue = cell.getStringCellValue();
                if (StringUtils.isEmpty(strValue)) return null;
                return LocalTime.parse(strValue, timeFormatter);
            } catch (DateTimeParseException e1) {
                e1.printStackTrace();
                return null;
            }
        } else if (CellType.NUMERIC == cell.getCellType()) {
            return convertToLocalTimeViaInstant(cell.getDateCellValue());
        }
        return null;
    }

    public static LocalTime getLocalTime(Cell cell) {
        if (cell == null) return null;
        if (CellType.STRING == cell.getCellType()) {
            DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
            try {
                String strValue = cell.getStringCellValue();
                if (StringUtils.isEmpty(strValue)) return null;
                return LocalTime.parse(strValue, timeFormatter);
            } catch (DateTimeParseException e1) {
                e1.printStackTrace();
                return null;
            }
        } else if (CellType.NUMERIC == cell.getCellType()) {
            return convertToLocalTimeViaInstant(cell.getDateCellValue());
        }
        return null;
    }

    private static LocalTime convertToLocalTimeViaInstant(Date dateToConvert) {
        if (null == dateToConvert) {
            return null;
        }
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalTime();
    }

    public static LocalDateTime getLocalDateTime(Cell cell, LocalDate date) {
        if (cell == null) return null;
        LocalTime time = getLocalTime(cell);
        if (null == time) return null;
        return LocalDateTime.of(date, time);
    }

    public static LocalDateTime getLocalDateTime(Cell cell) {
        if (cell == null) return null;
        return getLocalDateTime(cell, "MM/dd/yyyy HH:mm");
    }

    public static LocalDateTime getLocalDateTime(Cell cell, String pattern) {
        if (cell == null) return null;
        if (CellType.STRING == cell.getCellType()) {
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
            String strValue = cell.getStringCellValue();
            try {
                if (StringUtils.isEmpty(strValue)) return null;
                return LocalDateTime.parse(strValue, dateFormatter);
            } catch (DateTimeParseException e1) {
                return null;
            }
        } else if (CellType.NUMERIC == cell.getCellType()) {
            return convertToLocalDateTimeViaInstant(cell.getDateCellValue());
        }
        return null;
    }

    private static LocalDateTime convertToLocalDateTimeViaInstant(Date dateToConvert) {
        if (null == dateToConvert) {
            return null;
        }
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }
}

package com.timaven.timekeeping.util;


import be.quodlibet.boxable.Cell;
import be.quodlibet.boxable.Row;
import com.timaven.timekeeping.model.dto.CellAttributes;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;

import java.awt.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

public class PdfUtility {

    public static void fillInEditableFields(PDDocument document, Map<String, String> fieldValues,
                                            Map<String, String> fieldValues2) throws IOException {
        PDDocumentCatalog documentCatalog = document.getDocumentCatalog();
        PDAcroForm acroForm = documentCatalog.getAcroForm();
        acroForm.setNeedAppearances(true);
        List<PDField> fields = acroForm.getFields();
        for (PDField field : fields) {
            String name = field.getPartialName();
            if (!fieldValues.containsKey(name) && !fieldValues2.containsKey(name)) continue;
            if (fieldValues.containsKey(name)) {
                field.setValue(fieldValues.get(name));
            }
            if (fieldValues2.containsKey(name)) {
                field.setValue(fieldValues2.get(name));
            }
        }
    }

    private static boolean booleanValueOf(String value) {
        return "true".equalsIgnoreCase(value) || "t".equalsIgnoreCase(value);
    }

    public static Color hex2Rgb(String colorStr) {
        return new Color(
                Integer.valueOf(colorStr.substring(1, 3), 16),
                Integer.valueOf(colorStr.substring(3, 5), 16),
                Integer.valueOf(colorStr.substring(5, 7), 16));
    }

    public static String getCurrencyString(BigDecimal decimal) {
        if (decimal == null) decimal = BigDecimal.ZERO;
        return DecimalFormat.getCurrencyInstance(Locale.US).
                format(decimal);
    }

    public static String removeControlHT(String value) {
        if (value == null) return null;
        return value.replaceAll("\t", " ").replace("\n", "<p></p>");
    }

    public static String getOrDefault(String str) {
        if (str == null) return "";
        else return str;
    }

    public static void generateCells(Row<PDPage> row, int[] points, CellAttributes[] attributes) {
        if (null == points || null == attributes) return;
        if (points.length != attributes.length + 1)
            throw new IllegalArgumentException("Point sizes mismatch attribute sizes.");
        BigDecimal totalLen = BigDecimal.valueOf(points[points.length - 1] - points[0]);
        Cell<PDPage> cell;
        for (int i = 0; i < attributes.length; i++) {
            float width = BigDecimal.valueOf(points[i + 1] - points[i]).multiply(BigDecimal.valueOf(100)).divide(totalLen, 2, RoundingMode.HALF_UP).floatValue();
            CellAttributes attribute = attributes[i];
            if (attribute.getImage() != null) {
                cell = row.createImageCell(width, attribute.getImage(), attribute.getHorizontalAlignment(),
                        attribute.getVerticalAlignment());
            } else {
                cell = row.createCell(width, Optional.ofNullable(attribute.getValue()).orElse(""),
                        attribute.getHorizontalAlignment(), attribute.getVerticalAlignment());
            }
            if (attribute.getFont() != null) {
                cell.setFont(attribute.getFont());
            }
            if (attribute.getFillColor() != null) {
                cell.setFillColor(attribute.getFillColor());
            }
            if (attribute.getTextColor() != null) {
                cell.setTextColor(attribute.getTextColor());
            }
            if (attribute.getFontSize() != null) {
                cell.setFontSize(attribute.getFontSize());
            }
            if (attribute.getLeftPadding() != null) {
                cell.setLeftPadding(attribute.getLeftPadding());
            }
            if (attribute.getRightPadding() != null) {
                cell.setRightPadding(attribute.getRightPadding());
            }
            if (attribute.getTopPadding() != null) {
                cell.setTopPadding(attribute.getTopPadding());
            }
            if (attribute.getBottomPadding() != null) {
                cell.setBottomPadding(attribute.getBottomPadding());
            }
            if (attribute.getBorderStyle() != null) {
                cell.setBorderStyle(attribute.getBorderStyle());
            }
            if (attribute.getLeftBorderStyle() != null) {
                cell.setLeftBorderStyle(attribute.getLeftBorderStyle());
            }
            if (attribute.getRightBorderStyle() != null) {
                cell.setRightBorderStyle(attribute.getRightBorderStyle());
            }
            if (attribute.getTopBorderStyle() != null) {
                cell.setTopBorderStyle(attribute.getTopBorderStyle());
            }
            if (attribute.getBottomBorderStyle() != null) {
                cell.setBottomBorderStyle(attribute.getBottomBorderStyle());
            }
        }
    }
}

<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.timaven</groupId>
    <artifactId>timaven-document</artifactId>
    <name>timaven-document</name>
    <packaging>jar</packaging>
    <version>0.0.1</version>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.4.2</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <properties>
        <start-class>com.timaven.timekeeping.DocumentApplication</start-class>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>15</java.version>
        <spring-cloud-version>2020.0.1</spring-cloud-version>
        <oauth2-autoconfigure-version>2.4.2</oauth2-autoconfigure-version>
        <spring-security-jwt-version>1.1.1.RELEASE</spring-security-jwt-version>
        <commons-lang3-version>3.11</commons-lang3-version>
        <kie-spring-version>7.48.0.Final</kie-spring-version>
        <apache-poi-version>5.0.0</apache-poi-version>
        <json-version>20201115</json-version>
        <project-lombok-version>1.18.20</project-lombok-version>
        <pdfbox-version>2.0.24</pdfbox-version>
        <boxable-version>1.6</boxable-version>
        <s3-version>1.12.16</s3-version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-dependencies</artifactId>
                <version>${spring-cloud-version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <!-- oauth2 -->
        <dependency>
            <groupId>org.springframework.security.oauth.boot</groupId>
            <artifactId>spring-security-oauth2-autoconfigure</artifactId>
            <version>${oauth2-autoconfigure-version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.security</groupId>
            <artifactId>spring-security-jwt</artifactId>
            <version>${spring-security-jwt-version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>${commons-lang3-version}</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.kie/kie-spring -->
        <dependency>
            <groupId>org.kie</groupId>
            <artifactId>kie-spring</artifactId>
            <version>${kie-spring-version}</version>
        </dependency>

        <!-- Spring Boot 2.3 no longer include this by default any more    -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-validation</artifactId>
        </dependency>

        <dependency>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi-ooxml</artifactId>
            <version>${apache-poi-version}</version>
        </dependency>
        <dependency>
            <groupId>org.json</groupId>
            <artifactId>json</artifactId>
            <version>${json-version}</version>
        </dependency>
        <!--  Test  -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${project-lombok-version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-config</artifactId>
        </dependency>
        <!-- Tools -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-webflux</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-aop</artifactId>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.apache.pdfbox/pdfbox -->
        <dependency>
            <groupId>org.apache.pdfbox</groupId>
            <artifactId>pdfbox</artifactId>
            <version>${pdfbox-version}</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/com.github.dhorions/boxable -->
        <dependency>
            <groupId>com.github.dhorions</groupId>
            <artifactId>boxable</artifactId>
            <version>${boxable-version}</version>
        </dependency>

        <!-- aws-java-sdk-s3 -->
        <dependency>
            <groupId>com.amazonaws</groupId>
            <artifactId>aws-java-sdk-s3</artifactId>
            <version>${s3-version}</version>
        </dependency>
        <!-- hot swapping, disable cache for template, enable live reload -->
        <!-- Open the Settings Build-Execution-Deployment, Compiler and enable the Make Project Automatically. -->
        <!-- Then press ctrl+shift+A and search for the registry. In the registry, make the following configuration enabled. -->
        <!-- compiler.automake.allow.when.app.running -->
        <!-- Restart the IDE -->
        <!-- devtool has Conflict with Drools rule -->
        <!--        <dependency>-->
        <!--            <groupId>org.springframework.boot</groupId>-->
        <!--            <artifactId>spring-boot-devtools</artifactId>-->
        <!--            <optional>true</optional>-->
        <!--        </dependency>-->
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <configuration>
                    <excludeDevtools>true</excludeDevtools>
                </configuration>
            </plugin>
        </plugins>
        <finalName>document</finalName>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
    </build>
    <profiles>
        <profile>
            <id>rpc</id>
            <properties>
                <activatedProperties>rpc</activatedProperties>
            </properties>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
        </profile>
        <profile>
            <id>test</id>
            <properties>
                <activatedProperties>test</activatedProperties>
            </properties>
            <build>
                <directory>test-target</directory>
            </build>
        </profile>
    </profiles>
</project>
